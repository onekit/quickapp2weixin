export default class OneKit {
  static current () {
    const pages = getCurrentPages()
    return pages[pages.length - 1]
  }

  static currentUrl () {
    return this.current().uri
  }
}
