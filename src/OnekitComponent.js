/* eslint-disable camelcase */
import { STRING } from 'oneutil'

export default function OnekitComponent (quick_object) {
  const properties = {}
  const wx_object = {
    behaviors: [],
    data: {
    },
    created () {
      if (this.props) {
        for (const k of Object.keys(this.props)) {
          const v = this.props[k]
          properties[k] = v
          wx_object.data[k] = v
        }
      }
    },
    attached () {
      // ///
      let onInit
      if (quick_object.onInit) {
        onInit = quick_object.onInit
      } else {
        onInit = function () { }
      }
      onInit.call(this)
      // //////
      let onReady
      if (quick_object.onReady) {
        onReady = quick_object.onReady
      } else {
        onReady = function () { }
      }
      onReady.call(this)
    },
    didUnmount () {
      let onDestroy
      if (quick_object.onDestroy) {
        onDestroy = quick_object.onDestroy
      } else {
        onDestroy = function () { }
      }
      onDestroy.call(this)
    },
    methods: {
      get properties () {
        return properties
      },

    }
  }
  if (quick_object) {
    if (!quick_object.methods) { quick_object.methods = {} }
    quick_object.methods.triggerEvent = function (name, data) {
      const funcName = `on${STRING.firstUpper(name)}`
      if (this.props[funcName]) {
        this.props[funcName](data)
      }
    }
    quick_object.methods.createSelectorQuery = wx.createSelectorQuery
  }
  for (const key of Object.keys(quick_object)) {
    const value = quick_object[key]
    if (!value) {
      continue
    }
    switch (key) {
      case 'properties':
        wx_object.props = {}
        for (const k of Object.keys(value)) {
          const p = value[k]
          const v = (p && p.value ? p.value : null)
          wx_object.props[k] = v
          properties[k] = v
        }
        break
      case 'data':
        for (const k of Object.keys(value)) {
          wx_object.data[k] = value[k]
        }
        break

      default:
        if (typeof (value) === 'function') {
          wx_object.methods[key] = value
        } else {
          wx_object[key] = value
        }
        break
    }
  }
  if (quick_object.data) {
    for (const k of Object.keys(quick_object.data)) {
      const v = quick_object.data[k]
      wx_object.data[k] = v
    }
  }
  return Component(wx_object)
}
