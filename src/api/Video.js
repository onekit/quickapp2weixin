/* eslint-disable class-methods-use-this */
/* eslint-disable no-console */
/* eslint-disable camelcase */
import PROMISE from '../../node_modules/oneutil/PROMISE'

export default class Video {
  constructor(object) {
    this.object = object
  }

  compressVideo (quick_object) {
    const quick_success = quick_object.success
    const quick_fail = quick_object.fail
    const quick_complete = quick_object.complete
    quick_object = null
    PROMISE((SUCCESS) => {
      wx.chooseVideo({
        sourceType: ['album'],
        compressed: true,
        success: wx_res => {
          const quick_res = {
            uri: wx_res.tempFilePath,
            size: wx_res.size,
            duration: wx_res.duration,
            height: wx_res.height,
            width: wx_res.width,
          }
          SUCCESS(quick_res)
        }
      })
    }, quick_success, quick_fail, quick_complete)
  }

  getVideoInfo () {
    return console.warn('getVideoInfo is not support')
  }
}
