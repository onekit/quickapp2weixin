// onekit/ui/tabs/tabs.js
Component({
  options: {
  },
  properties: {
    onekitClass: {
      type: String,
      value: ''
    },
    onekitStyle: {
      type: String,
      value: ''
    },
    index: {
      type: Number,
      value: 0
    }
  },
  lifetimes: {
    attached () {

    }
  },
  pageLifetimes: {
    show () {
    }
  },
  relations: {
    '../tab-content/tab-content': {
      type: 'child', // 关联的目标节点应为子节点
      linked () {
      }
    }
  },
  observers: {
    index (index) {
      this.changeIndex(index)
    }
  },
  methods: {
    tab_change (e) {
      const index = e.detail.index
      this.changeIndex(index)
    },
    changeIndex (index) {
      const tabContent = this.tabContent
      tabContent.changeIndex(index)
    }
  }
})
