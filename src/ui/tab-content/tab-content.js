// onekit/ui/tab-content/tab-content.js
Component({
  data: {
    index: 0
  },
  options: {
    virtualHost: true,
    multipleSlots: true
  },
  properties: {
    onekitClass: {
      type: String,
      value: ''
    },
    onekitStyle: {
      type: String,
      value: ''
    },
    scrollable: {
      type: Boolean,
      value: true
    },
    count: Number
  },
  relations: {
    '../tabs/tabs': {
      type: 'parent',
      linked (tabs) {
        tabs.tabContent = this
        const index = tabs.data.index
        this.changeIndex(index)
      },
    }
  },
  methods: {
    changeIndex (index) {
      this.setData({ index })
    }
  }
})
