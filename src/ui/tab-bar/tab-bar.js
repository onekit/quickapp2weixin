// onekit/ui/tab-bar/tab-bar.js
Component({
  options: {
    multipleSlots: true
  },
  properties: {
    onekitClass: {
      type: String,
      value: ''
    },
    onekitStyle: {
      type: String,
      value: ''
    },
    mode: {
      type: String,
      value: 'fixed'
    }
  },
  methods: {
    tab_tap (e) {
      const index = e.currentTarget.dataset.index
      this.triggerEvent('change', { index }, { bubbles: true, composed: true })
    }
  }
})
