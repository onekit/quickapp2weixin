/* eslint-disable camelcase */
/* eslint-disable no-console */
function prop (THIS, p) {
  Object.defineProperty(THIS, p, {
    configurable: true,
    enumerable: true,
    get () {
      return THIS.data[p]
    },
    set (value) {
      // console.log('setData', p, value)
      const json = {}
      json[p] = value
      THIS.setData(json)
    }
  })
}
export default function OnekitPage (quick_object) {
  const wx_object = {
    data: {},
    $set (key, value) {
      const json = {}
      json[key] = value
      this.setData(json)
    },
    onLoad () {
      this.$page = {
        name: this.route,
        path: this.__route__,
        component: '',
        setTitleBar (quick_object) {
          const quick_text = quick_object.text || ''
          quick_object = null
          //
          const wx_title = quick_text
          wx.setNavigationBarTitle({ title: wx_title })
        }
      }
      const data = {}
      if (quick_object.private) {
        for (const ky of Object.keys(quick_object.private)) {
          prop(this, ky)
          data[ky] = quick_object.private[ky]
        }
      }
      this.setData(data)
      if (quick_object.onInit) {
        quick_object.onInit.apply(this)
      }
    }
  }
  for (const key of Object.keys(quick_object)) {
    const value = quick_object[key]
    switch (key) {
      case 'private':
        break
      case 'onInit':
        break
      default:
        wx_object[key] = value
    }
  }
  return Page(wx_object)
}
