/* eslint-disable no-console */
/* eslint-disable camelcase */
/* eslint-disable object-curly-spacing */
import { STRING } from 'oneutil'

export default function OnekitApp (quick_object) {
  // const wx_object = quick_object
  // return App(wx_object)

  const wx_object = {
    onekit: {},
    onLaunch () {
      // GUID
      const GUID = STRING.GUID()
      // 取 GUID
      wx.getStorage({
        key: 'GUID',
        success (res) {
          if (res.data === '') {
            // 存 GUID
            wx.setStorage({
              key: 'GUID',
              data: GUID,
            })
          }
        }
      })

      // getApp().on_launch_time = new Date().getTime()
      if (quick_object.onLaunch) {
        quick_object.onLaunch()
      }
    },
    onShow () {
      // getApp().show_time_consuming = new Date().getTime() - getApp().on_launch_time
      if (quick_object.onShow) {
        quick_object.onShow()
      }
    },
    onHide () {
      if (quick_object.onHide) {
        quick_object.onHide()
      }
    },
    onError () {
      if (quick_object.onError) {
        quick_object.onError()
      }
    },
    onPageNotFound () {
      if (quick_object.onPageNotFound) {
        quick_object.onPageNotFound()
      }
    }
  }
  for (const key of Object.keys(quick_object)) {
    const member = quick_object[key]
    switch (key) {
      case 'onLaunch':
      case 'onShow':
      case 'onHide':
      case 'onError':
      case 'onPageNotFound':
        break
      default:
        wx_object[key] = member
        break
    }
  }
  return App(wx_object)
}
