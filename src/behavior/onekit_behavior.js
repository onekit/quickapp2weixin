export default {
  properties: {
    onekitV: {
      type: String,
      value: ''
    },
    onekitClass: {
      type: String,
      value: ''
    },
    onekitStyle: {
      type: String,
      value: ''
    },
    onekitId: {
      type: String,
      value: ''
    }
  }
}
