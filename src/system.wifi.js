/* eslint-disable consistent-return */
/* eslint-disable no-console */
/* eslint-disable camelcase */
// import PROMISE from '../node_modules/oneutil/PROMISE'

export default {
  /** wifi.connect */
  connect (quick_object) {
    // return console.warn('connect is not support')
    return wx.connectWifi(quick_object)
  },
  /**
   * wifi.scan
   */
  scan (quick_object) {
    // return console.warn('scan is not support')
    return wx.getWifiList(quick_object)
  },
  /**
   * wifi.getConnectedWifi */

  getConnectedWifi (quick_object) {
    // return console.warn('getConnectedWifi is not support')
    return wx.getConnectedWifi(quick_object)
  },
  /**
   * wifi.onscanned */
  onscanned (quick_object) {
    // return console.warn('onscanned is not support')
    return wx.onGetWifiList(quick_object)
  },
  /** wifi.onstatechanged */
  onstatechanged (quick_object) {
    // return console.warn('onstatechanged is not support')
    return wx.onWifiConnected(quick_object)
  }
}
