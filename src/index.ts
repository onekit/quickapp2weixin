/* eslint-disable camelcase */
import App from './OnekitApp'
import Behavior from './OnekitBehavior'
import Component from './OnekitComponent'
import Page from './OnekitPage'

import service_alipay from './service.alipay'
import service_share from './service.share'
import service_wxaccount from './service.wxaccount'
import service_wxpay from './service.wxpay'
import service_account from './service.account'
import system_audio from './system.audio'
import system_barcode from './system.barcode'
import system_battery from './system.battery'
import system_bluetooth from './system.bluetooth'
import system_brightness from './system.brightness'
import system_calendar from './system.calendar'
import system_clipboard from './system.clipboard'
import system_contact from './system.contact'
import system_device from './system.device'
import system_dhtml from './system.dhtml'
import system_fetch from './system.fetch'
import system_file from './system.file'
import system_geolocation from './system.geolocation'
import system_image from './system.image'
import system_media from './system.media'
import system_network from './system.network'
import system_prompt from './system.prompt'
import system_record from './system.record'
import system_request from './system.request'
import system_router from './system.router'
import system_sensor from './system.sensor'
import system_share from './system.share'
import system_storage from './system.storage'
import system_vibrator from './system.vibrator'
import system_websocketfactory from './system.websocketfactory'
import system_webview from './system.webview'
import system_wifi from './system.wifi'
import system_zip from './system.zip'

export {
  App,
  Behavior,
  Component,
  Page,
  service_alipay,
  service_share,
  service_wxaccount,
  service_wxpay,
  service_account,
  system_audio,
  system_barcode,
  system_battery,
  system_bluetooth,
  system_brightness,
  system_calendar,
  system_clipboard,
  system_contact,
  system_device,
  system_dhtml,
  system_fetch,
  system_file,
  system_geolocation,
  system_image,
  system_media,
  system_network,
  system_prompt,
  system_record,
  system_request,
  system_router,
  system_sensor,
  system_share,
  system_storage,
  system_vibrator,
  system_webview,
  system_websocketfactory,
  system_wifi,
  system_zip
}
