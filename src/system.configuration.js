/* eslint-disable no-console */
/* eslint-disable camelcase */
// import PROMISE from '../node_modules/oneutil/PROMISE'

export default {
  getLocale () {
    const wx_res = wx.getSystemInfoSync()
    const quick_res = {
      language: wx_res.language,
      countryOrRegion: 'CN'
    }
    return quick_res
  },
  setLocale () {
    return console.warn('setLocale is not support')
  },
  getThemeMode () {
    return console.warn('getThemeMode is not support')
  }

}
