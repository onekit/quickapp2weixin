module.exports = (function() {
var __MODS__ = {};
var __DEFINE__ = function(modId, func, req) { var m = { exports: {}, _tempexports: {} }; __MODS__[modId] = { status: 0, func: func, req: req, m: m }; };
var __REQUIRE__ = function(modId, source) { if(!__MODS__[modId]) return require(source); if(!__MODS__[modId].status) { var m = __MODS__[modId].m; m._exports = m._tempexports; var desp = Object.getOwnPropertyDescriptor(m, "exports"); if (desp && desp.configurable) Object.defineProperty(m, "exports", { set: function (val) { if(typeof val === "object" && val !== m._exports) { m._exports.__proto__ = val.__proto__; Object.keys(val).forEach(function (k) { m._exports[k] = val[k]; }); } m._tempexports = val }, get: function () { return m._tempexports; } }); __MODS__[modId].status = 1; __MODS__[modId].func(__MODS__[modId].req, m, m.exports); } return __MODS__[modId].m.exports; };
var __REQUIRE_WILDCARD__ = function(obj) { if(obj && obj.__esModule) { return obj; } else { var newObj = {}; if(obj != null) { for(var k in obj) { if (Object.prototype.hasOwnProperty.call(obj, k)) newObj[k] = obj[k]; } } newObj.default = obj; return newObj; } };
var __REQUIRE_DEFAULT__ = function(obj) { return obj && obj.__esModule ? obj.default : obj; };
__DEFINE__(1644817541944, function(require, module, exports) {
var BASE64 = require('./BASE64')
var BLOB = require('./BLOB')
var BOOLEAN = require('./BOOLEAN')
var COLOR = require('./COLOR')
var DATE = require('./DATE')
var DEVICE = require('./DEVICE')
var HTML = require('./HTML')
var OBJECT = require('./OBJECT')
var PATH = require('./PATH')
var PROMISE = require('./PROMISE')
var STRING = require('./STRING')
var TASK = require('./TASK')
var URL = require('./URL')

module.exports = {
  BASE64: BASE64,
  BLOB: BLOB,
  BOOLEAN: BOOLEAN,
  COLOR: COLOR,
  DATE: DATE,
  DEVICE: DEVICE,
  HTML: HTML,
  OBJECT: OBJECT,
  PATH: PATH,
  PROMISE: PROMISE,
  STRING: STRING,
  TASK: TASK,
  URL: URL
}

}, function(modId) {var map = {"./BASE64":1644817541945,"./BLOB":1644817541946,"./BOOLEAN":1644817541947,"./COLOR":1644817541948,"./DATE":1644817541949,"./DEVICE":1644817541950,"./HTML":1644817541951,"./OBJECT":1644817541952,"./PATH":1644817541953,"./PROMISE":1644817541954,"./STRING":1644817541955,"./TASK":1644817541956,"./URL":1644817541957}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541945, function(require, module, exports) {
module.exports = {

  base64decode: function (e) {
    var base64DecodeChars = new Array((-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), (-1), 62, (-1), (-1), (-1), 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, (-1), (-1), (-1), (-1), (-1), (-1), (-1), 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, (-1), (-1), (-1), (-1), (-1), (-1), 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, (-1), (-1), (-1), (-1), (-1));
    var r, a, c, h, o, t, d;
    for (t = e.length, o = 0, d = ''; o < t;) {
      do r = base64DecodeChars[255 & e.charCodeAt(o++)]
      while (o < t && r == -1);
      if (r == -1) break;
      do a = base64DecodeChars[255 & e.charCodeAt(o++)]
      while (o < t && a == -1);
      if (a == -1) break;
      d += String.fromCharCode(r << 2 | (48 & a) >> 4)
      do {
        if (c = 255 & e.charCodeAt(o++), 61 == c) return d
        c = base64DecodeChars[c]
      } while (o < t && c == -1)
      if (c == -1) break;
      d += String.fromCharCode((15 & a) << 4 | (60 & c) >> 2)
      do {
        if (h = 255 & e.charCodeAt(o++), 61 == h) return d;
        h = base64DecodeChars[h]
      } while (o < t && h == -1)
      if (h == -1) break;
      d += String.fromCharCode((3 & c) << 6 | h)
    }
    return d
  }
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541946, function(require, module, exports) {
var BASE64 = require('./BASE64')
module.exports = {
  blob2Base64: function (blob, callback) {
    // eslint-disable-next-line no-undef
    var a = new FileReader();
    a.onload = function (e) {
      callback(e.target.result)
    }
    a.readAsDataURL(blob)
  },

  blob2string: function (blob, callbak, encoding) {
    // eslint-disable-next-line no-undef
    var reader = new FileReader()
    reader.onload = function () {
      callbak(reader.result)
    }
    reader.readAsText(blob, encoding)
  },

  blob2ascii: function (blob, callbak) {
    // eslint-disable-next-line no-undef
    var reader = new FileReader()
    reader.onload = function () {
      callbak(reader.result)
    }
    reader.readAsText(blob, 'ascii')
  },

  blob2binary: function (blob, callback) {
    // eslint-disable-next-line no-undef
    var reader = new FileReader()
    reader.onload = function () {
      callback(reader.result)
    }
    reader.readAsBinaryString(blob)
  },

  blob2hex: function (blob, callback) {
    this.blobToBase64(blob, function (res) {
      var bin = BASE64.base64decode(res.replace(/[ \r\n]+$/, ""))
      var hex = []
      for (var i = 0; i < bin.length; ++i) {
        var tmp = bin.charCodeAt(i).toString(16);
        if (tmp.length === 1) tmp = "0" + tmp;
        hex[hex.length] = tmp;
      }
      callback(hex.join(''))
    })
  },

  blob2arrbuffer: function (blob, callback) {
    // eslint-disable-next-line no-undef
    var reader = new FileReader()
    reader.onload = function () {
      callback(reader.result)
    }
    reader.readAsArrayBuffer(blob)
  }
}

}, function(modId) { var map = {"./BASE64":1644817541945}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541947, function(require, module, exports) {

module.exports = {
  fix: function (obj) {
    if (typeof obj === 'string') {
      return obj === 'checked'
    } else if (typeof obj === 'boolean') {
      return obj
    } else if (typeof obj === 'number') {
      return obj === 0
    } else {
      return obj != null
    }
  }
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541948, function(require, module, exports) {

module.exports = {
  COLORS: function () {
    return {
      aliceblue: 'F0F8FFFF',
      antiquewhite: 'FAEBD7FF',
      aqua: '00FFFFFF',
      aquamarine: '7FFFD4FF',
      azure: 'F0FFFFFF',
      beige: 'F5F5DCFF',
      bisque: 'FFE4C4FF',
      black: '000000FF',
      blanchedalmond: 'FFEBCDFF',
      blue: '0000FFFF',
      blueviolet: '8A2BE2FF',
      brown: 'A52A2AFF',
      burlywood: 'DEB887FF',
      cadetblue: '5F9EA0FF',
      chartreuse: '7FFF00FF',
      chocolate: 'D2691EFF',
      coral: 'FF7F50FF',
      cornflowerblue: '6495EDFF',
      cornsilk: 'FFF8DCFF',
      crimson: 'DC143CFF',
      cyan: '00FFFFFF',
      darkblue: '00008BFF',
      darkcyan: '008B8BFF',
      darkgoldenRod: 'B8860BFF',
      darkgray: 'A9A9A9FF',
      darkgreen: '006400FF',
      darkkhaki: 'BDB76BFF',
      darkmagenta: '8B008BFF',
      darkolivegreen: '556B2FFF',
      darkorange: 'FF8C00FF',
      darkorchid: '9932CCFF',
      darkred: '8B0000FF',
      darksalmon: 'E9967AFF',
      darkseaGreen: '8FBC8FFF',
      darkslateBlue: '483D8BFF',
      darkslateGray: '2F4F4FFF',
      darkturquoise: '00CED1FF',
      darkviolet: '9400D3FF',
      deeppink: 'FF1493FF',
      deepskyblue: '00BFFFFF',
      dimgray: '696969FF',
      dodgerblue: '1E90FFFF',
      feldspar: 'D19275FF',
      firebrick: 'B22222FF',
      floralwhite: 'FFFAF0FF',
      forestgreen: '228B22FF',
      fuchsia: 'FF00FFFF',
      gainsboro: 'DCDCDCFF',
      ghostwhite: 'F8F8FFFF',
      gold: 'FFD700FF',
      goldenRod: 'DAA520FF',
      gray: '808080FF',
      green: '008000FF',
      greenyellow: 'ADFF2FFF',
      honeydew: 'F0FFF0FF',
      hotpink: 'FF69B4FF',
      indianred: 'CD5C5CFF',
      indigo: '4B0082FF',
      ivory: 'FFFFF0FF',
      khaki: 'F0E68CFF',
      lavender: 'E6E6FAFF',
      lavendernlush: 'FFF0F5FF',
      lawngreen: '7CFC00FF',
      lemonchiffon: 'FFFACDFF',
      lightblue: 'ADD8E6FF',
      lightcoral: 'F08080FF',
      lightcyan: 'E0FFFFFF',
      lightgoldenRodYellow: 'FAFAD2FF',
      lightgrey: 'D3D3D3FF',
      lightgreen: '90EE90FF',
      lightpink: 'FFB6C1FF',
      lightsalmon: 'FFA07AFF',
      lightseaGreen: '20B2AAFF',
      lightskyBlue: '87CEFAFF',
      lightslateBlue: '8470FFFF',
      lightslateGray: '778899FF',
      lightsteelBlue: 'B0C4DEFF',
      lightyellow: 'FFFFE0FF',
      lime: '00FF00FF',
      limegreen: '32CD32FF',
      linen: 'FAF0E6FF',
      magenta: 'FF00FFFF',
      maroon: '800000FF',
      mediumaquaMarine: '66CDAAFF',
      mediumblue: '0000CDFF',
      mediumorchid: 'BA55D3FF',
      mediumpurple: '9370D8FF',
      mediumseagreen: '3CB371FF',
      mediumslateblue: '7B68EEFF',
      mediumspringgreen: '00FA9AFF',
      mediumturquoise: '48D1CCFF',
      mediumvioletred: 'C71585FF',
      midnightblue: '191970FF',
      mintcream: 'F5FFFAFF',
      mistyrose: 'FFE4E1FF',
      moccasin: 'FFE4B5FF',
      navajowhite: 'FFDEADFF',
      navy: '000080FF',
      oldlace: 'FDF5E6FF',
      olive: '808000FF',
      olivedrab: '6B8E23FF',
      orange: 'FFA500FF',
      Orangered: 'FF4500FF',
      Orchid: 'DA70D6FF',
      palegoldenrod: 'EEE8AAFF',
      palegreen: '98FB98FF',
      paleturquoise: 'AFEEEEFF',
      palevioletred: 'D87093FF',
      papayawhip: 'FFEFD5FF',
      peachpuff: 'FFDAB9FF',
      peru: 'CD853FFF',
      pink: 'FFC0CBFF',
      plum: 'DDA0DDFF',
      powderblue: 'B0E0E6FF',
      purple: '800080FF',
      red: 'FF0000FF',
      rosybrown: 'BC8F8FFF',
      royalblue: '4169E1FF',
      saddlebrown: '8B4513FF',
      salmon: 'FA8072FF',
      sandybrown: 'F4A460FF',
      seagreen: '2E8B57FF',
      seashell: 'FFF5EEFF',
      sienna: 'A0522DFF',
      silver: 'C0C0C0FF',
      skyblue: '87CEEBFF',
      slateblue: '6A5ACDFF',
      slategray: '708090FF',
      snow: 'FFFAFAFF',
      springgreen: '00FF7FFF',
      steelblue: '4682B4FF',
      tan: 'D2B48CFF',
      teal: '008080FF',
      thistle: 'D8BFD8FF',
      tomato: 'FF6347FF',
      turquoise: '40E0D0FF',
      violet: 'EE82EEFF',
      violetred: 'D02090FF',
      wheat: 'F5DEB3FF',
      white: 'FFFFFFFF',
      whitesmoke: 'F5F5F5FF',
      yellow: 'FFFF00FF',
      yellowgreen: '9ACD32FF'
    }
  },
  rgba2str: function (r, g, b, a) { // a=0~255
    function componentToHex (c) {
      c = c.trim()
      var hex = parseInt(c, 10).toString(16)
      return hex.length === 1 ? '0' + hex : hex
    }
    return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b) + (a ? componentToHex(a) : "")
  },

  //十六进制转换为rgb或rgba
  str2rgb: function (sHex, alpha) {
    alpha = alpha || 1;
    // 十六进制颜色值的正则表达式
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/
    /* 16进制颜色转为RGB格式 */
    var sColor = sHex.toLowerCase()
    if (sColor && reg.test(sColor)) {
      if (sColor.length === 4) {
        var sColorNew = '#'
        for (var i = 1; i < 4; i += 1) {
          sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1))
        }
        sColor = sColorNew
      }
      //  处理六位的颜色值
      var sColorChange = []
      for (i = 1; i < 7; i += 2) {
        sColorChange.push(parseInt('0x' + sColor.slice(i, i + 2)))
      }
      return 'rgba(' + sColorChange.join(',') + ',' + alpha + ')'
    } else {
      return sColor
    }
  },

  color: function (string, isLength6) {
    if (!string) {
      return null
    }

    function fix (result) {
      result = result.toUpperCase()
      if (isLength6) {
        return result.substr(0, 7)
      } else {
        return result
      }
    }
    var str = new String(string.trim())
    var result = this.COLORS[str.toLowerCase()]
    if (result) {
      return fix("#" + result)
    }
    if ((str.startsWith('rgb(') || str.startsWith('rgba(')) && str.endsWith(')')) {
      var array = str.substring(str.indexOf('(') + 1, str.indexOf(')')).split(',')
      if (array.length === 3) {
        array.push(1)
      }
      result = this.rgba2str(array[0], array[1], array[2], array[3] * 255 + '')
      return fix(result)
    }
    var r, g, b, a;
    switch (str.length) {
      case 4: {
        r = str[1] + ""
        g = str[2] + ""
        b = str[3] + ""
        result = '#' + r + r + g + g + b + b + 'FF';
        return fix(result)
      }
      case 5: {
        r = str[1] + ""
        g = str[2] + ""
        b = str[3] + ""
        a = str[4] + ""
        result = '#' + r + r + g + g + b + b + a + a;
        return fix(result)
      }
      case 7:
        return fix(str + 'FF')
      case 9:
        return fix(str)
      default:
        throw new Error(str + " " + str.length)
    }
  },
  rgba: function (string) {
    var str = this.color(string)
    var array = this.str2array(str)
    return 'rgba(' + array[0] + ',' + array[1] + ',' + array[2] + ',' + (array[3] / 255.0).toFixed(2) + ')'
  },
  rgb: function (string) {
    var str = this.color(string, true)
    var array = this.str2array(str)
    return 'rgb(' + array[0] + ',' + array[1] + ',' + array[2] + ')'
  },
  str2array: function (str) {
    var string
    switch (str.length) {
      case 4:
        string =
          "#".concat(str[1]).concat(str[1]).concat(str[2]).concat(str[2]).concat(str[3]).concat(str[3], "FF");
        break
      case 5:
        string =
          "#".concat(str[1]).concat(str[1]).concat(str[2]).concat(str[2]).concat(str[3]).concat(str[3]).concat(str[4]).concat(str[4]);
        break
      case 7:
        string = "#".concat(str, "FF")
        break
      case 9:
        string = str
        break
      default:
        throw new Error(str + " " + str.length)
    }
    return [parseInt(string.substr(1, 2), 16),
    parseInt(string.substr(3, 2), 16),
    parseInt(string.substr(5, 2), 16),
    parseInt(string.substr(7, 2), 16)
    ]
  },

  array2str: function (array) {
    function f (v) {
      var s = v.toString(16)
      if (s.length === 1) {
        s = '0' + s
      }
      return s
    }
    var str = '#' + f(array[0]) + f(array[1]) + f(array[2]) + f(array[3])
    return str
  }
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541949, function(require, module, exports) {

module.exports = {
  monthDays: function (year, month) {
    if ([1, 3, 5, 7, 8, 10, 12].indexOf(month) >= 0) {
      return 31
    } else if (month === 2) {
      if ((year % 4 === 0 && year % 100 !== 0) || (year % 100 === 0 && year % 400 === 0)) {
        return 29
      } else {
        return 28
      }
    } else {
      return 30
    }
  }
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541950, function(require, module, exports) {

module.exports = {
  isMobile: function () {
    // eslint-disable-next-line no-undef
    var ua = navigator.userAgent;

    var ipad = ua.match(/(iPad).*OS\s([\d_]+)/),

      isIphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/),

      isAndroid = ua.match(/(Android)\s+([\d.]+)/);
    return isIphone || isAndroid
  }
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541951, function(require, module, exports) {
var HTML = require("./HTML")
module.exports = {
  html2nodes: function (html) {
    function _html2node (xParent) {
      var nodes = []
      for (var i = 0; i < xParent.childNodes.length; i++) {
        var xNode = xParent.childNodes[i]
        var node
        switch (xNode.nodeType) {
          case 3:
            node = {
              type: 'text',
              text: xNode.nodeValue
            }
            break
          case 1:
            node = {
              name: xNode.tagName.toLowerCase(),
              children: _html2node(xNode),
              attrs: {}
            }
            if (xNode.attributes.class) {
              node.attrs.class = xNode.attributes.class.value
            }
            if (xNode.attributes.style) {
              node.attrs.style = xNode.attributes.style.value
            }
            break
          default:
            console.warn(xNode.nodeType)
            node = null
            break
        }
        if (node) { nodes.push(node) }
      }
      return nodes
    }
    // eslint-disable-next-line no-undef
    var document = new DOMParser().parseFromString(html, 'text/html')
    return _html2node(document.querySelector('body'))
  },

  nodes2html: function (nodes) {
    function _node2html (node) {
      var selfCloses = ['br', 'img']
      switch (node.type) {
        case 'text': return node.text
        default: {
          var attributs = ''
          if (node.attrs) {
            for (var key in node.attrs) {
              var value = node.attrs[key]
              attributs += key + '="' + value + '"'
            }
          }
          //
          var children = node.children ? HTML.nodes2html(node.children) : ''
          if (selfCloses.indexOf(node.name) >= 0) {
            return '<' + node.name + attributs + '/>'
          } else {
            return '<' + node.name + attributs + '>' + children + '</' + node.name + '>'
          }
        }
      }
    }
    var html = ''
    for (var n = 0; n < nodes.length; n++) {
      var node = nodes[n]
      html += _node2html(node)
    }
    return html
  }
}

}, function(modId) { var map = {"./HTML":1644817541951}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541952, function(require, module, exports) {

var OBJECT = require("./OBJECT")
module.exports = {
  assign: function (object1, object2) {
    if (object2 == null) {
      return object1
    }
    var keys = Object.keys(object2);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i]
      var value1 = object1[key]
      var value2 = object2[key]
      if (value2 == null) {
        continue;
      }
      if (value1 != null) {
        object1[key] = OBJECT.assign(value1, value2)
      } else {
        object1[key] = value2
      }
    }
    return object1;
  },
  clone: function (object) {
    var string = JSON.stringify(object)
    return JSON.parse(string)
  }
}

}, function(modId) { var map = {"./OBJECT":1644817541952}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541953, function(require, module, exports) {

var PATH = require("./PATH")
module.exports = {
  rel2abs: function (currentUrl, url) {
    if (url.startsWith('/')) {
      return url.substring(1)
    }
    // //////////////////
    var folder
    if (currentUrl.indexOf('/') >= 0) {
      folder = currentUrl.substring(0, currentUrl.lastIndexOf('/') + 1)
      if (folder.startsWith('/')) {
        folder = folder.substring(1)
      }
    } else {
      folder = ''
    }
    url = url.trim()
    if (url.startsWith('./')) {
      url = url.substring('./'.length)
    }
    while (url.startsWith('../')) {
      folder = folder.substring(0, folder.length - 1)
      folder = folder.substring(0, folder.lastIndexOf('/') + 1)
      url = url.substring('../'.length)
    }

    return folder + url
  },

  abs2rel: function (currentUrl, url) {
    url = PATH.rel2abs(currentUrl, url)
    if (currentUrl.startsWith('/')) {
      currentUrl = currentUrl.substring(1)
    }
    var array = currentUrl.split('/')
    var result = ''
    if (array.length > 1) {
      for (var i = 0; i < array.length - 1; i++) {
        result += '../'
      }
    } else {
      result += './'
    }
    result += url
    return result.toString()
  }
}

}, function(modId) { var map = {"./PATH":1644817541953}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541954, function(require, module, exports) {

module.exports = function (body, success, fail, complete) {
  try {
    return body(function (res) {
      if (success) {
        success(res)
      }
      if (complete) {
        complete(res)
      }
    }, function (res) {
      if (fail) {
        fail(res)
      }
      if (complete) {
        complete(res)
      }
    })
  } catch (e) {
    var res = {
      errMsg: e.message
    }
    if (fail) {
      fail(res)
    }
    if (complete) {
      complete(res)
    }
    return null
  }
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541955, function(require, module, exports) {

module.exports = {
  GUID: function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  },

  firstUpper: function (str) {
    return str.substr(0, 1).toUpperCase() + str.substr(1)
  },

  firstLower: function (str) {
    return str.substr(0, 1).toLowerCase() + str.substr(1)
  },
  replaceAll: function (str, str1, str2) {
    return str.replace(new RegExp(str1, "gm"), str2);
  },
  replace: function (string, str1, str2) {
    var result = ''
    var inTag = false
    for (var i = 0; i < string.length; i++) {
      var chr = string.substr(i, 1)
      switch (chr) {
        case '<':
          inTag = true
          result += chr
          break
        case '>':
          inTag = false
          result += chr
          break
        case str1:
          result += inTag ? chr : str2
          break
        default:
          result += chr
          break
      }
    }
    return result
  },

  base64ToArrayBuffer: function (base64) {
    base64 = base64.replace(/\s/g, '+')
    var commonContent = Buffer.import(base64, 'base64')
    return commonContent
  },

  arrayBufferToBase64: function (arrayBuffer) {
    var base64Content = Buffer.import(arrayBuffer).toString('base64')
    return base64Content
  }
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541956, function(require, module, exports) {

module.exports = function (items, func, success) {
  var result = []
  var i = 0
  var itemCallback = null
  itemCallback = function (res) {
    result.push(res)
    if (i >= items.length) {
      success(result)
      return
    }
    func(items[i++], itemCallback)
  }
  func(items[i++], itemCallback)
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1644817541957, function(require, module, exports) {

module.exports = {
  parse: function (url) {
    const result = {};
    var p1 = url.indexOf(':')
    if (p1 > 0) {
      var temp = url.substring(p1 + 1)
      var flag = temp.startsWith('//') ? 3 : 1
      //
      result.scheme = url.substring(0, p1)
      var p2 = url.indexOf('/', p1 + flag)
      if (p2 > 0) {
        result.host = url.substring(p1 + flag, p2)
        var p3 = url.indexOf('?')
        if (p2 < url.length) {
          if (p3 > 0) {
            result.path = url.substring(p2, p3)
          } else {
            result.path = url.substring(p2)
          }
        }
      } else {
        result.host = url.substring(p1 + flag)
      }
    }
    //
    result.params = {}
    var p4 = url.indexOf('?')
    if (p4 > 0) {
      result.querystring = url.substring(p4 + 1)
      var querys = result.querystring.split('&')
      for (var q = 0; q < querys.length; q++) {
        var query = querys[q]
        var pair = query.split('=')
        result.params[pair[0]] = decodeURIComponent(pair[1])
      }
    }
    return result
  }
}

}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
return __REQUIRE__(1644817541944);
})()
//miniprogram-npm-outsideDeps=[]
//# sourceMappingURL=index.js.map