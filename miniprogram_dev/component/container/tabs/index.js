import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'tabs',
        "time":'',
        "loadMore":true,
        "scrollPage":false,
        "scrollable":true,
        "listData":[
          {
              "name":'A'
},
          {
              "name":'B'
},
          {
              "name":'C'
},
          {
              "name":'D'
}
        ]
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Tabs'
});
      this.setData({time:((new Date().getHours() + ':')) + new Date().getMinutes()});
    },
    "changeTabactive"(e){
      console.info('切换tab: ' + e.index);
    },
    "setTime"(e){
      this.setData({time:((e.hour + ':')) + e.minute});
    },
    "scrollbottom"(){
      const self = this;
      setTimeout(function(){
      self.listData = self.listData.concat([
      {
          "name":'A'
},
      {
          "name":'B'
},
      {
          "name":'C'
},
      {
          "name":'D'
}
    ]);
    },1000);
    },
    "goIndex"(){
      this.$element('list').scrollTo({
        "index":0,
        "smooth":true
});
    },
    "switchScrollMode"(){
      this.goIndex();
      this.setData({scrollPage:!this.data.scrollPage});
    },
    "switchScrollable"(e){
      this.setData({scrollable:e.checked});
    }
});

