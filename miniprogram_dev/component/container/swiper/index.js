import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'swiper',
        "autoPlay":true,
        "indicator":true,
        "loopPlay":true,
        "sliderValue":1000,
        "durationValue":1000,
        "isVertical":false,
        "marginValue":'0px',
        "topValue":'210px'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Swiper'
});
    },
    "startAutoPlay"(e){
      this.setData({autoPlay:e.checked});
    },
    "showIndicator"(e){
      this.setData({indicator:e.checked});
    },
    "switchLoopPlay"(e){
      this.setData({loopPlay:e.checked});
    },
    "switchDirection"(e){
      this.setData({isVertical:e.checked});
      if(e.checked)
      {
        this.setData({topValue:68 + 'px'});
      }
else
      {
        this.setData({topValue:195 + 'px'});
      };
    },
    "setSliderValue"(e){
      this.setData({sliderValue:e.progress});
    },
    "setDurationValue"(e){
      this.setData({durationValue:e.progress});
    },
    "setMarginValue"(e){
      this.setData({marginValue:e.progress + 'px'});
    },
    "setTopValue"(e){
      this.setData({topValue:e.progress + 'px'});
    }
});

