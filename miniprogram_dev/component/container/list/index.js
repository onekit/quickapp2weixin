import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'list',
        "loadMore":true,
        "listAdd":[
          'A',
          'B',
          'C',
          'D',
          'E',
          'F',
          'G',
          'H',
          'I',
          'J',
          'K',
          'L',
          'M',
          'N',
          'O',
          'P',
          'Q',
          'R',
          'S',
          'T',
          'U',
          'V',
          'W',
          'X',
          'Y',
          'Z'
        ],
        "listData":[
        ],
        "scrollPage":false,
        "showListBtn":true,
        "listClass":'selected',
        "arrayClass":'',
        "columnsNum":3,
        "columnsNumNew":0
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'List'
});
      this.setData({listData:[
    ].concat(this.data.listAdd)});
      this.$page.setTitleBar({
        "text":'List'
});
    },
    "showListMethod"(){
      if(!this.data.showListBtn)
      {
        this.setData({showListBtn:true});
        this.setData({listClass:'selected'});
        this.setData({arrayClass:''});
      };
    },
    "showArrayMethod"(){
      if(this.data.showListBtn)
      {
        this.setData({showListBtn:false});
        this.setData({listClass:''});
        this.setData({arrayClass:'selected'});
      };
    },
    "scrollbottom"(){
      const self = this;
      const renderData = [
    ].concat(self.listData,self.listAdd);
      setTimeout(function(){
      self.listData = renderData;
    },1000);
    },
    "handleScroll"(param){
      if(param.scrollState == 0)
      {
        console.info(`### handleScroll ### scrollState: ${param.scrollState}, scrollX: ${param.scrollX}, scrollY: ${param.scrollY}`);
      };
    },
    "writeCloumnsNum"(e){
      this.setData({columnsNumNew:(e.text) || (0)});
    },
    "setCloumnsNum"(){
      if((this.data.columnsNumNew) && this.data.columnsNumNew != this.data.columnsNum)
      {
        this.goIndex();
        this.setData({columnsNum:this.data.columnsNumNew});
      };
      this.$element('columns').focus({
        "focus":false
});
    },
    "switchScrollMode"(){
      this.goIndex();
      this.setData({scrollPage:!this.data.scrollPage});
    },
    "goIndex"(){
      this.$element('list').scrollTo({
        "index":0,
        "smooth":true
});
    },
    "clickPush"(){
          const listData=this.data.listData;listData.push('push');this.setData({[`listData`]:listData});
      prompt.showToast({
        "message":'向数组的末尾添加一个"push"元素'
});
    },
    "clickPop"(){
      prompt.showToast({
        "message":'删除数组的最后一个元素' + JSON.stringify(this.data.listData.pop())
});
    },
    "clickShift"(){
      prompt.showToast({
        "message":'删除数组的第一个元素' + JSON.stringify(this.data.listData.shift())
});
    },
    "clickUnshift"(){
      this.data.listData.unshift('unshift');
      prompt.showToast({
        "message":'向数组的开头添加一个"unshift"元素'
});
    },
    "clickSplice"(){
      prompt.showToast({
        "message":(('删除第一个元素' + JSON.stringify(this.data.listData.splice(0,1,'splice')[0]))) + '，并添加一个"splice元素"'
});
    },
    "clickSort"(){
      this.data.listData.sort();
      prompt.showToast({
        "message":'sort'
});
    },
    "clickReverse"(){
      this.data.listData.reverse();
      prompt.showToast({
        "message":'reverse'
});
    },
    "scrolltouchup"(){
      prompt.showToast({
        "message":'touch up'
});
    },
    "scrollend"(){
      prompt.showToast({
        "message":'scroll end'
});
    }
});

