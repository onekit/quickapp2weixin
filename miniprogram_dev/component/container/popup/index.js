import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'popup',
        "posList":[
          'top',
          'left',
          'bottom',
          'right',
          'topLeft',
          'topRight',
          'bottomLeft',
          'bottomRight'
        ],
        "currentDire":'bottom'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'popup'
});
    },
    "show"(){
      this.$element('picker').show();
    },
    "selectDire"(e){
      this.setData({currentDire:e.newValue});
    }
});

