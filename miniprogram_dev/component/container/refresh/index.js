import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'refresh',
        "isRefreshing":false,
        "refreshType":'auto'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Refresh'
});
    },
    "changeType"(){
      this.setData({refreshType:(this.data.refreshType == 'auto')?'pulldown':'auto'});
    },
    "refresh"(e){
      const self = this;
      self.isRefreshing = e.refreshing;
      setTimeout(function(){
      self.isRefreshing = false;
      prompt.showToast({
        "message":'刷新完成'
});
    },3000);
    },
    "stopRefresh"(){
      this.setData({isRefreshing:false});
    }
});

