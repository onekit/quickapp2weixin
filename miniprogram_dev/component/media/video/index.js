import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_network} from '../../../quickapp2weixin/index';const network = system_network;
Page({
onekit_startVideo(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.startvideo0);this.startVideo.apply(this,args);},
    "private":{
        "componentName":'video',
        "showmask":true,
        "showicon":true,
        "ignorenet":false,
        "showControls":true,
        "muted":false,
        "objectFitValues":[
          "cover",
          "contain",
          "fill",
          "none",
          "scale-down"
        ],
        "objectFitClass":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Video'
});
    },
    "startVideo"(e){
      if(e.usenet)
      {
        this.setData({ignorenet:true});
      };
      this.networkstatus('video');
    },
    "networkstatus"(){
      network.subscribe({
        "callback":function(data){
          if((data.type == 'wifi') || (this.data.ignorenet))
          {
            this.$element('video').start();
            this.setData({showmask:false});
          }
else
          {
            this.setData({showmask:true});
            this.$element('video').pause();
            this.setData({showicon:false});
          };
        }.bind(this)
});
    },
    "switchShowControls"(e){
      this.setData({showControls:e.checked});
    },
    "onMutedSwitched"(e){
      this.setData({muted:e.checked});
    },
    "onMulchSwitched"(e){
      this.$element('stackDemo01').requestFullscreen({
        "screenOrientation":"landscape"
});
    },
    "changeOption"(e){
      this.setData({objectFitClass:e.newValue});
      prompt.showToast({
        "message":`object-fit切换为 ${e.newValue}`
});
    }
});

