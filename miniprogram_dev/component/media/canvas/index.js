import {Page,Component} from '../../../quickapp2weixin/index.js';
import {drawClock,drawLogo,drawGradientLogo,grayFilter,drawRectByGloComOperation} from './canvas';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'canvas',
        "globalCompositeOperation":'source-over',
        "imageUrl":'',
        "blur":'0',
        "color":'black',
        "OffsetX":'0',
        "OffsetY":'0'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Canvas'
});
    },
    "onShow"(){
      this.drawCanvas1();
      this.drawCanvas2();
      this.drawCanvas3();
      this.drawCanvas4();
      this.drawCanvas5();
      this.drawCanvas6();
      this.drawCanvas7();
      this.drawCanvas8(this.data.blur,this.data.color,this.data.OffsetX,this.data.OffsetY);
    },
    "drawCanvas1"(){
      const conf = {
        "indicate":true,
        "indicateColor":'#222',
        "dial1Color":'#666600',
        "dial2Color":'#81812e',
        "dial3Color":'#9d9d5c',
        "timeAdd":1,
        "time24h":true,
        "dateAdd":3,
        "dateAddColor":'#999'
};
      const canvas = this.$element('canvas1');
      const ctx = canvas.getContext('2d');
      drawClock(400,ctx,conf);
    },
    "drawCanvas2"(){
      const canvas = this.$element('canvas2');
      const ctx = canvas.getContext('2d');
      drawLogo(ctx);
    },
    "drawCanvas3"(){
      const canvas = this.$element('canvas3');
      const ctx = canvas.getContext('2d');
      const img = new Image();
      img.src = 'https://www.quickapp.cn/assets/images/home/logo.png';
      img.onload = function(){
      ctx.drawImage(img,40,40,300,100);
    };
    },
    "drawCanvas4"(){
      const canvas = this.$element('canvas4');
      const ctx = canvas.getContext('2d');
      ctx.drawImage(this.$element('image'),0,0,300,300);
    },
    "drawCanvas5"(){
      const canvas = this.$element('canvas5');
      const ctx = canvas.getContext('2d');
      drawGradientLogo(ctx);
      grayFilter(ctx);
    },
    "drawCanvas6"(){
      const canvas = this.$element('canvas6');
      const ctx = canvas.getContext('2d');
      drawRectByGloComOperation(ctx,this.data.globalCompositeOperation);
    },
    "changeGlobalCompositeOperation"(){
      const globalCompositeOperationArr = [
      'source-over',
      'source-atop',
      'source-in',
      'source-out',
      'destination-over',
      'destination-atop',
      'destination-in',
      'destination-out',
      'lighter',
      'copy',
      'xor'
    ];
      const index = globalCompositeOperationArr.indexOf(this.data.globalCompositeOperation);
      if(index < ((globalCompositeOperationArr.length - 1)))
      {
        this.setData({globalCompositeOperation:globalCompositeOperationArr[index + 1]});
      }
else
      {
        this.setData({globalCompositeOperation:globalCompositeOperationArr[0]});
      };
      this.drawCanvas6();
    },
    "drawCanvas7"(){
      const canvas = this.$element('canvas7');
      const ctx = canvas.getContext('2d');
      let offset = 0;
      setInterval(()=>{
      offset++;
      if(offset > 16)
        {
          offset = 0;
        };
      ctx.clearRect(0,0,300,300);
      ctx.setLineDash([
        4,
        2
      ]);
      ctx.lineDashOffset = -offset;
      ctx.strokeRect(10,10,200,200);
    },20);
    },
    "savePic"(){
      this.$element('canvas1').toTempFilePath({
        "x":50,
        "y":50,
        "width":350,
        "height":350,
        "destWidth":200,
        "destHeight":200,
        "fileType":'png',
        "quality":0.5,
        "success":(res)=>{this.setData({imageUrl:res.uri})},
        "fail":(err,code)=>{prompt.showToast({
            "message":`错误原因：${err}, 错误代码：${code}`
})}
});
    },
    "drawCanvas8"(blur,color,OffsetX,OffsetY){
      const canvas = this.$element('canvas8');
      const ctx = canvas.getContext('2d');
      ctx.clearRect(0,0,300,300);
      ctx.beginPath();
      ctx.fillStyle = '#ccc';
      ctx.shadowBlur = blur;
      ctx.shadowColor = color;
      ctx.shadowOffsetX = OffsetX;
      ctx.shadowOffsetY = OffsetY;
      ctx.rect(0,0,300,300);
      ctx.fill();
      ctx.closePath();
    },
    "shadowChangeBlur"(e){
      this.setData({blur:e.value});
      this.drawCanvas8(this.data.blur,this.data.color,this.data.OffsetX,this.data.OffsetY);
    },
    "shadowChangeColor"(e){
      this.setData({color:e.value});
      this.drawCanvas8(this.data.blur,this.data.color,this.data.OffsetX,this.data.OffsetY);
    },
    "shadowChangeOffsetX"(e){
      this.setData({OffsetX:e.value});
      this.drawCanvas8(this.data.blur,this.data.color,this.data.OffsetX,this.data.OffsetY);
    },
    "shadowChangeOffsetY"(e){
      this.setData({OffsetY:e.value});
      this.drawCanvas8(this.data.blur,this.data.color,this.data.OffsetX,this.data.OffsetY);
    }
});

