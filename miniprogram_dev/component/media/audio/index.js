import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_audio} from '../../../quickapp2weixin/index';const audio = system_audio;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_resident} from '../../../quickapp2weixin/index';const resident = system_resident;
Page({
    "private":{
        "componentName":'Audio',
        "curIndex":0,
        "audioArr":[
          {
              "src":'/common/audio/music-1.mp3',
              "cover":'/common/audio/music-1.png',
              "artist":'作者1',
              "title":'歌曲1'
},
          {
              "src":'/common/audio/music-2.mp3',
              "cover":'/common/audio/music-2.png',
              "artist":'作者2',
              "title":'歌曲2'
},
          {
              "src":'/common/audio/music-3.mp3',
              "cover":'/common/audio/music-3.png',
              "artist":'作者3',
              "title":'歌曲3'
}
        ],
        "duration":100,
        "spendTime":'00:00',
        "totalTime":'00:00',
        "currentTime":0,
        "volume":0,
        "valumeStatus":0,
        "status":'stop',
        "playerStatus":true,
        "audioState":'请先播放音乐'
},
    "onReady"(){
      this.$page.setTitleBar({
        "text":'Audio'
});
      audio.src = this.data.audioArr[this.data.curIndex].src;
      audio.title = this.data.audioArr[this.data.curIndex].title;
      audio.artist = this.data.audioArr[this.data.curIndex].artist;
      audio.cover = this.data.audioArr[this.data.curIndex].cover;
      audio.autoplay = false;
      this.setData({volume:parseInt(audio.volume * 10)});
      this.setData({valumeStatus:audio.volume});
      this.audioInit();
    },
    "audioInit"(){
      const self = this;
      audio.onloadeddata = function(){
      audio.loop = false;
      self.duration = audio.duration;
      self.totalTime = self.formatTime(audio.duration);
    };
      audio.ontimeupdate = function(){
      self.updateAudio();
    };
      audio.onpause = function(){
      self.status = 'audio pause';
      console.info('pause');
    };
      audio.onended = function(){
      self.status = 'audio end';
      self.playerStatus = true;
    };
      audio.onerror = function(){
      self.status = 'audio error';
      console.info('error');
    };
      audio.ondurationchange = function(){
      console.info('change');
    };
      audio.onplay = function(){
      self.status = 'audio play';
      self.playerStatus = false;
    };
      audio.onstop = function(){
      self.currentTime = 0;
      self.spendTime = '00:00';
      self.status = 'audio stop';
      self.playerStatus = true;
    };
      audio.onprevious = function(){
      if(self.curIndex > 0)
      {
        self.curIndex--;
      }
else
      {
        self.curIndex = self.audioArr.length - 1;
      };
      audio.src = self.audioArr[self.curIndex].src;
      audio.title = self.audioArr[self.curIndex].title;
      audio.artist = self.audioArr[self.curIndex].artist;
      audio.cover = self.audioArr[self.curIndex].cover;
      audio.play();
    };
      audio.onnext = function(){
      if(self.curIndex < ((self.audioArr.length - 1)))
      {
        self.curIndex++;
      }
else
      {
        self.curIndex = 0;
      };
      audio.src = self.audioArr[self.curIndex].src;
      audio.title = self.audioArr[self.curIndex].title;
      audio.artist = self.audioArr[self.curIndex].artist;
      audio.cover = self.audioArr[self.curIndex].cover;
      audio.play();
    };
      resident.start({
        "desc":'快应用音乐后台运行'
});
      audio.onended = ()=>{
      if(this.data.curIndex < ((this.data.audioArr.length - 1)))
        {
                    let curIndex=this.data.curIndex;curIndex++;this.setData({[`curIndex`]:curIndex});
        }
else
        {
          this.setData({curIndex:0});
        };
      audio.src = this.data.audioArr[this.data.curIndex].src;
      audio.title = this.data.audioArr[this.data.curIndex].title;
      audio.artist = this.data.audioArr[this.data.curIndex].artist;
      audio.cover = this.data.audioArr[this.data.curIndex].cover;
      audio.play();
    };
    },
    "playerStart"(){
      if(this.data.playerStatus)
      {
        audio.play();
      }
else
      {
        audio.pause();
      };
      this.setData({playerStatus:!this.data.playerStatus});
    },
    "stopAudio"(){
      if(!this.data.playerStatus)
      {
        audio.stop();
      };
    },
    "updateAudio"(){
      const time = audio.currentTime;
      this.setData({currentTime:time});
      this.setData({spendTime:this.formatTime(time)});
    },
    "sliderChange"(e){
      const value = e.progress - audio.currentTime;
      if((value > 1) || value < -1)
      {
        audio.currentTime = e.progress;
        this.setData({playerStatus:false});
      };
    },
    "adjustVolume"(e){
      audio.volume = e.progress / 10;
      this.setData({valumeStatus:e.progress / 10});
    },
    "changeAudioNotificationVisibility"(e){
      audio.notificationVisible = e.checked;
    },
    "formatTime"(time){
      const s = parseInt(time);
      if(!s)
      {
        return '00:00';
      };
      const m = parseInt(s / 60);
      const se = s % 60;
      const min = (m >= 10)?m:('0' + m);
      const sec = (se >= 10)?se:('0' + se);
      return ((min + ':')) + sec;
    },
    "changeOption"(e){
      let streamType = e.newValue;
      audio.streamType = streamType;
      prompt.showToast({
        "message":`音频类型切换为 ${streamType}`
});
    },
    "getAudioState"(){
      audio.getPlayState({
        "success":(data)=>{
          const arr = [
            `state: ${data.state}`,
            `src: ${data.src}`,
            `currentTime: ${data.currentTime}`,
            `autoplay: ${data.autoplay}`,
            `loop: ${data.loop}`,
            `volume: ${data.volume}`,
            `muted: ${data.muted}`,
            `notificationVisible: ${data.notificationVisible}`
          ];
          this.setData({audioState:'\n' + arr.join('\n')});
        }
});
    }
});

