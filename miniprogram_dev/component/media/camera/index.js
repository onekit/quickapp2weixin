import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'camera',
        "pictureUrl":'',
        "photoQuality":'normal',
        "deviceposition":"back",
        "flash":"auto",
        "flashText":'自动'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Camera'
});
    },
    "takePhoto"(){
      let _this = this;
      this.$element('camera').takePhoto({
        "quality":_this.photoQuality,
        "success"(data){
          _this.pictureUrl = data.uri;
        },
        "fail"(data,code){
          console.log('take photos failed：code' + code.code);
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "switchQuality"(){
      const qualityArr = [
      'low',
      'normal',
      'high'
    ];
      const index = qualityArr.indexOf(this.data.photoQuality);
      if(index >= 2)
      {
        this.setData({photoQuality:qualityArr[0]});
      }
else
      {
        this.setData({photoQuality:qualityArr[index + 1]});
      };
    },
    "switchCamera"(e){
      this.setData({deviceposition:(this.data.deviceposition == 'back')?'front':'back'});
    },
    "switchFlash"(){
      const flashArr = [
      'auto',
      'on',
      'off',
      'torch'
    ];
      const textArr = [
      '自动',
      '开',
      '关',
      '常亮'
    ];
      const index = flashArr.indexOf(this.data.flash);
      if(index >= 3)
      {
        this.setData({flash:flashArr[0]});
      }
else
      {
        this.setData({flash:flashArr[index + 1]});
      };
      this.setData({flashText:textArr[flashArr.indexOf(this.data.flash)]});
    },
    "handlerError"(){
      console.log('用户不允许使用摄像头');
    }
});

