import {Page,Component} from '../../../../quickapp2weixin/index.js';
Component({
    "data":{
        "titleBar":{
            "type":'titleBar',
            "config":{
                "titleBarShow":false,
                "height":'100px',
                "backgroundColor":'#0faeff',
                "color":'#ffffff',
                "title":'',
                "textAlign":'center',
                "arrowLeftShow":true,
                "leftIconSrc":'./component/arrow-left.png',
                "arrowRightShow":false,
                "rightIconSrc":'./component/arrow-right.png'
}
}
},
    "onInit"(){
      for(const i in this.data.value.config){
        this.data.titleBar.config[i] = this.data.value.config[i];
      };
      this.$extend(this.data.value,this.data.titleBar);
    },
    "back"(){
      this.$dispatch('arrowLeft');
    },
    "share"(){
      this.$dispatch('arrowRight');
    }
});

