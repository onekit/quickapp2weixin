import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_router} from '../../../quickapp2weixin/index';const router = system_router;
Page({
onekit_gotoPay(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.gotopay0);this.gotoPay.apply(this,args);},
onekit_openWeb(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.openweb0);this.openWeb.apply(this,args);},
    "private":{
        "componentName":'web'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Web'
});
    },
    "openWeb"(allowthirdpartycookies){
      router.push({
        "uri":'/component/media/web/detail',
        "params":{
            "url":'https://www.quickapp.cn/',
            allowthirdpartycookies
}
});
    },
    "gotoPay"(type){
      router.push({
        "uri":'/component/media/web/detail',
        "params":{
            "url":(type == 'weixin')?'https://www.jd.com':'https://www.taobao.com',
            "allowthirdpartycookies":false
}
});
    },
    "gotoMap"(){
      router.push({
        "uri":'/component/media/web/detail',
        "params":{
            "url":'https://m.amap.com/'
}
});
    }
});

