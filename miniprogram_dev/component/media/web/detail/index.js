import {Page,Component} from '../../../../quickapp2weixin/index.js';
import {system_router} from '../../../../quickapp2weixin/index';const router = system_router;
Page({
    "data":{
        "websrc":'',
        "titleBarParams":{
            "type":'titleBar',
            "config":{
                "titleBarShow":true
}
},
        "allow":false
},
    "onInit"(){
      this.setData({websrc:this.data.url});
      this.setData({allow:this.data.allowthirdpartycookies == 'true'});
      this.$page.setTitleBar({
        "text":`allowthirdpartycookies=${this.data.allow}`
});
      this.$on('arrowLeft',this.data.arrowLeftIcon);
      this.$on('arrowRight',this.data.arrowRightIcon);
    },
    "onPageStart"(e){
      console.info('### pagestart ###' + e.url);
    },
    "onTitleReceive"(e){
      this.data.titleBarParams.config.title = e.title;
    },
    "onError"(){
      console.info('### pageError ###');
    },
    "arrowLeftIcon"(){
      this.isCanBack();
    },
    "arrowRightIcon"(){
      this.isCanForward();
    },
    "isCanForward"(){
      this.$element('web').canForward({
        "callback":function(e){
          if(e)
          {
            this.$element('web').forward();
          };
        }.bind(this)
});
    },
    "isCanBack"(){
      this.$element('web').canBack({
        "callback":function(e){
          if(e)
          {
            this.$element('web').back();
          }
else
          {
            router.back();
          };
        }.bind(this)
});
    }
});

