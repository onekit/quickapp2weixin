import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
const MARKER_PATH = "./img/marker.png";
const DEFAULT_SCALE = 17;
const TIAN_TAN_PARK_WGS = {
    "latitude":39.8812273482,
    "longitude":116.4105388182
};
const POINT1 = {
    "latitude":39.9090371069,
    "longitude":116.3953853161
};
const POINT2 = {
    "latitude":39.9089550115,
    "longitude":116.3992842749
};
const POINT3 = {
    "latitude":39.9061293143,
    "longitude":116.3995796987
};
const POINT4 = {
    "latitude":39.906169422,
    "longitude":116.3953937341
};
const MARKER = {
    "iconPath":MARKER_PATH,
    "width":100,
    "callout":{
        "convertHtml":true,
        "content":"这里是<font color=\'#ff0000\'>天坛公园</font>",
        "padding":20,
        "borderRadius":20
}
};
const POLYLINE = {
    "points":[
      POINT1,
      POINT2,
      POINT3,
      POINT4,
      POINT1
    ],
    "dotted":false,
    "width":10,
    "arrowLine":true,
    "arrowIconPath":"./img/black_arrow.png"
};
const POLYGONS = {
    "points":[
      POINT1,
      POINT2,
      POINT3,
      POINT4,
      POINT1
    ],
    "strokeWidth":"1px",
    "strokeColor":"#0faeff",
    "fillColor":"#000000",
    "zIndex":10
};
const CIRCLE = Object.assign({
    "fillColor":"#666666",
    "borderColor":"#ffffff",
    "borderWidth":10,
    "radius":100
},TIAN_TAN_PARK_WGS);
let iconPathIndex = 0;
const iconPathList = [
  MARKER_PATH,
  'https://doc.quickapp.cn/assets/images/logo.png'
];
let markTemp = {
};
let locationTemp = {
};
let carTarget = {
    "carA":1,
    "carB":1
};
let latitudeTemp = TIAN_TAN_PARK_WGS.latitude;
let longitudeTemp = TIAN_TAN_PARK_WGS.longitude;
Page({
onekit_changeOffset(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.changeoffset0);this.changeOffset.apply(this,args);},
onekit_changeColor(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.changecolor0);this.changeColor.apply(this,args);},
onekit_changePolygons(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.changepolygons0);this.changePolygons.apply(this,args);},
    "private":{
        "showList":{
            "mapShow":true,
            "guestureShow":false,
            "methodShow":false,
            "viewportShow":false,
            "operateSingleMarker":false,
            "calloutShow":false,
            "labelShow":false,
            "moveShow":false,
            "polylineShow":false,
            "polygonsShow":false,
            "circleShow":false,
            "groundoverlayShow":false,
            "controlShow":false,
            "coordShow":false,
            "viewShow":false
},
        "width":"100%",
        "height":"100%",
        "rotate":0,
        "currentCoordType":'',
        "showmylocation":false,
        "scale":DEFAULT_SCALE,
        "latitude":null,
        "longitude":null,
        "polylines":null,
        "polygons":null,
        "circles":null,
        "markers":null,
        "groundoverlays":null,
        "controls":null,
        "includePoints":[
        ],
        "latitudeConvert":null,
        "longitudeConvert":null,
        "fillColor":'',
        "strokeColor":'',
        "compassSwitch":true,
        "lookingSwitch":true,
        "zoomSwitch":true,
        "dragSwitch":true,
        "rotateSwitch":true,
        "customMarkerattr":{
            "latitude":39.9090371069,
            "longitude":116.3953853161
},
        "isShowView":false
},
    "onReady"(){
      this.getCoord();
    },
    "changeOption"(e){
      const newValue = e.newValue;
      for(let key in this.data.showList){
        this.setData({[`showList.${key}`]:false});
      };
      this.setData({[`showList.${newValue}`]:true});
      this.setData({polylines:null});
      this.setData({polygons:null});
      this.setData({markers:null});
      this.setData({circles:null});
      this.setData({includePoints:[
    ]});
      this.setData({controls:null});
      this.setData({groundoverlays:null});
      this.setData({scale:null});
      this.setData({scale:DEFAULT_SCALE});
      this.setData({rotate:0});
      markTemp = {
        "markerAlpha":1,
        "markerX":0.5,
        "markerY":1
};
          switch(newValue){
case "viewportShow":
        const pointList = [
  POINT1,
  POINT2,
  POINT3,
  POINT4
];
        this.setData({width:"100%"});
        this.setData({height:"100%"});
        this.setData({markers:pointList.map((item)=>{Object.assign({
},item,{
    "iconPath":MARKER_PATH,
    "width":20
})})});
        this.setData({includePoints:pointList});
        break;
case "operateSingleMarker":
case "calloutShow":
        this.moveToCenter();
        this.setData({markers:[
  Object.assign({
      "iconPath":MARKER_PATH,
      "width":100,
      "id":1,
      "rotate":0,
      "callout":{
          "convertHtml":true,
          "content":"这里是<font color=\'#ff0000\'>天坛公园</font>",
          "padding":0,
          "borderRadius":0
}
},TIAN_TAN_PARK_WGS)
]});
        break;
case "labelShow":
        this.moveToCenter();
        this.setData({markers:[
  Object.assign({
      "iconPath":MARKER_PATH,
      "width":100,
      "id":1,
      "rotate":0,
      "label":{
          "content":'当前位置(label)',
          "color":'#ff981a',
          "fontSize":'25px',
          "anchorX":0,
          "anchorY":0,
          "borderRadius":'10px',
          "padding":'10px',
          "backgroundColor":'#ffffff',
          "textAlign":'center'
}
},TIAN_TAN_PARK_WGS)
]});
        break;
case "moveShow":
        this.moveToCenter();
        this.setData({markers:[
  {
      "id":1,
      "iconPath":"./img/carA.png"
},
  {
      "id":2,
      "iconPath":"./img/carB.png"
}
].map((item)=>{Object.assign(item,POINT1,{
    "width":100,
    "anchor":{
        "x":0.5,
        "y":0.5
}
})})});
case "polylineShow":
        this.moveToCenter();
        this.setData({polylines:[
  POLYLINE
]});
        break;
case "polygonsShow":
        this.moveToCenter();
        this.setData({polygons:[
  POLYGONS
]});
        break;
case "circleShow":
        this.moveToCenter();
        this.setData({circles:[
  CIRCLE
]});
        break;
case "groundoverlayShow":
        this.moveToCenter();
        this.setData({groundoverlays:[
  {
      "northEast":POINT2,
      "southWest":POINT4,
      "iconPath":"./../../../common/logo.png",
      "opacity":1,
      "visible":true
}
]});
        break;
case "controlShow":
        this.setData({controls:[
  {
      "id":1,
      "position":{
          "left":"50px",
          "bottom":"200px",
          "width":"70px"
},
      "iconPath":"./img/location.png"
},
  {
      "id":2,
      "position":{
          "right":"50px",
          "bottom":"200px",
          "width":"70px"
},
      "iconPath":"./img/minus.png"
},
  {
      "id":3,
      "position":{
          "right":"50px",
          "bottom":"300px",
          "width":"70px"
},
      "iconPath":"./img/plus.png"
}
]});
        break;
    };
    },
    "moveToCenter"(){
      this.setData({latitude:null});
      this.setData({longitude:null});
      this.setData({latitude:TIAN_TAN_PARK_WGS.latitude});
      this.setData({longitude:TIAN_TAN_PARK_WGS.longitude});
    },
    "tap"(){
      prompt.showToast({
        "message":"地图被点击"
});
    },
    "loaded"(){
      prompt.showToast({
        "message":"地图组件加载完毕"
});
    },
    "getCenter"(){
      this.$element('map').getCenterLocation({
        "success"(res){
          prompt.showToast({
            "message":(((((("中心点纬度：" + res.latitude)) + "\n")) + "中心点经度：")) + res.longitude
});
        },
        "fail"(reason){
          prompt.showToast({
            "message":`获取失败，reason = ${reason}`
});
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "getScale"(){
      this.$element('map').getScale({
        "success"(res){
          prompt.showToast({
            "message":"缩放级别：" + res.scale
});
        },
        "fail"(reason){
          prompt.showToast({
            "message":`获取失败，reason = ${reason}`
});
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "getRegion"(){
      this.$element('map').getRegion({
        "success"(res){
          prompt.showToast({
            "message":(((((("西南角：\n" + JSON.stringify(res.southwest))) + "\n")) + "东北角：\n")) + JSON.stringify(res.northeast)
});
        },
        "fail"(reason){
          prompt.showToast({
            "message":`获取失败，reason = ${reason}`
});
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "regionchange"(){
      prompt.showToast({
        "message":"地图视野发生变化"
});
    },
    "controlTap"(res){
      const that = this;
          switch(res.controlId){
case 1:
        this.setData({showmylocation:true});
        this.$element('map').moveToMyLocation();
        prompt.showToast({
    "message":"控件：移动到当前位置并显示定位UI"
});
        break;
case 2:
        this.setData({scale:this.data.scale - 1});
        this.$element('map').getScale({
    "success"(res){
      that.scale = res.scale;
    },
    "fail"(reason){
      prompt.showToast({
        "message":`获取失败，reason = ${reason}`
});
    },
    "complete"(){
      console.log('complete');
    }
});
        prompt.showToast({
    "message":"控件：缩小地图"
});
        break;
case 3:
        this.setData({scale:this.data.scale + 1});
        this.$element('map').getScale({
    "success"(res){
      that.scale = res.scale;
    },
    "fail"(reason){
      prompt.showToast({
        "message":`获取失败，reason = ${reason}`
});
    },
    "complete"(){
      console.log('complete');
    }
});
        prompt.showToast({
    "message":"控件：放大地图"
});
        break;
      default:
    };
    },
    "switchDotted"(){
      let polyline = this.data.polylines[0];
      polyline.dotted = polyline.dotted?false:true;
      this.setData({polylines:[
      polyline
    ]});
    },
    "switchArrow"(){
      let polyline = this.data.polylines[0];
      polyline.arrowLine = polyline.arrowLine?false:true;
      this.setData({polylines:[
      polyline
    ]});
    },
    "showMyLocation"(){
      this.setData({showmylocation:this.data.showmylocation?false:true});
    },
    "moveToMyLocation"(){
      this.$element('map').moveToMyLocation();
    },
    "getCoord"(){
      const self = this;
      this.$element('map').getCoordType({
        "success"(res){
          self.currentCoordType = res.coordType;
          prompt.showToast({
            "message":"当前坐标系：" + res.coordType
});
        },
        "fail"(reason){
          prompt.showToast({
            "message":`获取失败，reason = ${reason}`
});
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "getSupportedCoordTypes"(){
      this.$element('map').getSupportedCoordTypes({
        "success"(res){
          let msg = JSON.stringify(res);
          prompt.showToast({
            "message":`当前支持的坐标系：${msg}`
});
        },
        "fail"(reason){
          prompt.showToast({
            "message":`获取失败，reason = ${reason}`
});
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "limitViewPort"(){
      this.$element('map').includePoints({
        "points":[
          POINT1,
          POINT2,
          POINT3,
          POINT4
        ],
        "padding":"0px 530px 200px 300px",
        "success"(){
          console.log('success');
        },
        "fail"(reason){
          prompt.showToast({
            "message":`reason = ${reason}`
});
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "polylinePlus"(){
      let polyline = this.data.polylines[0];
      polyline.width++;
      this.setData({polylines:[
      polyline
    ]});
    },
    "polylineMinus"(){
      let polyline = this.data.polylines[0];
      polyline.width--;
      if(polyline.width <= 0)
      {
        polyline.width = 1;
      };
      this.setData({polylines:[
      polyline
    ]});
    },
    "circleLinePlus"(){
      let circle = this.data.circles[0];
      circle.borderWidth++;
      this.setData({circles:[
      circle
    ]});
    },
    "circleLineMinus"(){
      let circle = this.data.circles[0];
      circle.borderWidth--;
      if(circle.borderWidth <= 0)
      {
        circle.borderWidth = 1;
      };
      this.setData({circles:[
      circle
    ]});
    },
    "circleRadiusPlus"(){
      let circle = this.data.circles[0];
      circle.radius += 10;
      this.setData({circles:[
      circle
    ]});
    },
    "circleRadiusMinus"(){
      let circle = this.data.circles[0];
      circle.radius -= 10;
      if(circle.radius <= 0)
      {
        circle.radius = 10;
      };
      this.setData({circles:[
      circle
    ]});
    },
    "markerSizePlus"(){
      let mark = this.data.markers[0];
      mark.width += 10;
      this.setData({markers:[
      mark
    ]});
    },
    "markerSizeMinus"(){
      let mark = this.data.markers[0];
      mark.width -= 10;
      if(mark.width <= 0)
      {
        mark.width <= 10;
      };
      this.setData({markers:[
      mark
    ]});
    },
    "changeMarkerAlpha"(value){
      markTemp.markerAlpha = value.text;
    },
    "changeMarkerX"(value){
      markTemp.markerX = value.text;
    },
    "changeMarkerY"(value){
      markTemp.markerY = value.text;
    },
    "refreshMarker"(){
      let mark = this.data.markers[0];
      mark.opacity = markTemp.markerAlpha;
      mark.anchor = {
        "x":markTemp.markerX,
        "y":markTemp.markerY
};
      this.setData({markers:[
      mark
    ]});
    },
    "rotateMarkerPlus"(){
      let mark = this.data.markers[0];
      mark.rotate += 10;
      this.setData({markers:[
      mark
    ]});
    },
    "rotateMarkerMinus"(){
      let mark = this.data.markers[0];
      mark.rotate -= 10;
      this.setData({markers:[
      mark
    ]});
    },
    "calloutRadiusPlus"(){
      let mark = this.data.markers[0];
      mark.callout.borderRadius += 10;
      this.setData({markers:[
      mark
    ]});
    },
    "calloutRadiusMinus"(){
      let mark = this.data.markers[0];
      mark.callout.borderRadius -= 10;
      if(mark.callout.borderRadius <= 0)
      {
        mark.callout.borderRadius = 0;
      };
      this.setData({markers:[
      mark
    ]});
    },
    "calloutPaddingPlus"(){
      let mark = this.data.markers[0];
      mark.callout.padding += 10;
      this.setData({markers:[
      mark
    ]});
    },
    "calloutPaddingMinus"(){
      let mark = this.data.markers[0];
      mark.callout.padding -= 10;
      if(mark.callout.padding <= 0)
      {
        mark.callout.padding = 0;
      };
      this.setData({markers:[
      mark
    ]});
    },
    "displayChange"(e){
      let mark = this.data.markers[0];
      mark.callout.display = e.value;
      this.setData({markers:[
      mark
    ]});
    },
    "calloutContentChange"(e){
      let mark = this.data.markers[0];
      mark.callout.content = (e.text == "")?"这里是<font color=\'#ff0000\'>天坛公园</font>":e.text;
      this.setData({markers:[
      mark
    ]});
    },
    "textAlignChange"(e){
      let mark = this.data.markers[0];
      mark.callout.textAlign = e.value;
      this.setData({markers:[
      mark
    ]});
    },
    "moveA"(){
      let target;
          switch(carTarget.carA){
case 1:
        carTarget.carA = 2;
        target = POINT2;
        break;
case 2:
        carTarget.carA = 3;
        target = POINT3;
        break;
case 3:
        carTarget.carA = 4;
        target = POINT4;
        break;
case 4:
        carTarget.carA = 1;
        target = POINT1;
        break;
    };
      this.$element("map").translateMarker({
        "markerId":1,
        "destination":target,
        "autoRotate":true,
        "duration":3000,
        "animationEnd"(){
          prompt.showToast({
            "message":"小车A移动结束"
});
        },
        "success"(){
          console.log('success');
        },
        "fail"(reason){
          prompt.showToast({
            "message":`移动失败，reason = ${reason}`
});
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "moveB"(){
      let target;
          switch(carTarget.carB){
case 1:
        carTarget.carB = 4;
        target = POINT4;
        break;
case 2:
        carTarget.carB = 1;
        target = POINT1;
        break;
case 3:
        carTarget.carB = 2;
        target = POINT2;
        break;
case 4:
        carTarget.carB = 3;
        target = POINT3;
        break;
    };
      this.$element("map").translateMarker({
        "markerId":2,
        "destination":target,
        "autoRotate":true,
        "duration":3000,
        "animationEnd"(){
          prompt.showToast({
            "message":"小车B移动结束"
});
        },
        "success"(){
          console.log('success');
        },
        "fail"(reason){
          prompt.showToast({
            "message":`移动失败，reason = ${reason}`
});
        },
        "complete"(){
          console.log('complete');
        }
});
    },
    "resizeMap"(){
      this.setData({width:(this.data.width == "80%")?"100%":"80%"});
      this.setData({height:(this.data.height == "80%")?"100%":"80%"});
    },
    "rotateMapPlus"(){
      this.data.rotate += 10;
    },
    "rotateMapMinus"(){
      this.data.rotate -= 10;
    },
    "changeLatitude"(e){
      const text = e.text;
      if(text == "")
      {
        latitudeTemp = "";
      }
else
      {
        latitudeTemp = text;
      };
    },
    "changeLongitude"(e){
      const text = e.text;
      if(text == "")
      {
        longitudeTemp = "";
      }
else
      {
        longitudeTemp = text;
      };
    },
    "convertCoord"(){
      if((latitudeTemp) && (longitudeTemp))
      {
        const that = this;
        this.$element("map").convertCoord({
            "latitude":latitudeTemp,
            "longitude":longitudeTemp,
            "success"(res){
              that.latitudeConvert = res.latitude;
              that.longitudeConvert = res.longitude;
            },
            "fail"(reason){
              prompt.showToast({
                "message":`转换失败，reason = ${reason}`
});
            },
            "complete"(){
              console.log('complete');
            }
});
      };
    },
    "markerTap"(res){
      prompt.showToast({
        "message":"Marker被点击, id：" + res.markerId
});
    },
    "calloutTap"(res){
      prompt.showToast({
        "message":"Callout被点击, marker id：" + res.markerId
});
    },
    "goOpacityMinus"(){
      let groundoverlay = this.data.groundoverlays[0];
      groundoverlay.opacity -= 0.1;
      if(groundoverlay.opacity < 0)
      {
        groundoverlay.opacity = 0;
      };
      this.setData({groundoverlays:[
      groundoverlay
    ]});
    },
    "goOpacityPlus"(){
      let groundoverlay = this.data.groundoverlays[0];
      groundoverlay.opacity += 0.1;
      if(groundoverlay.opacity > 1)
      {
        groundoverlay.opacity = 1;
      };
      this.setData({groundoverlays:[
      groundoverlay
    ]});
    },
    "switchGoVisible"(){
      let groundoverlay = this.data.groundoverlays[0];
      groundoverlay.visible = groundoverlay.visible?false:true;
      this.setData({groundoverlays:[
      groundoverlay
    ]});
    },
    "toggleIconPath"(){
      let mark = this.data.markers[0];
      iconPathIndex++;
      mark.iconPath = iconPathList[iconPathIndex % iconPathList.length];
      this.setData({markers:[
      mark
    ]});
    },
    "changeOffset"(type,e){
      markTemp[`offset${type}`] = Number(e.value);
    },
    "refreshOffset"(){
      let mark = this.data.markers[0];
      mark[`offsetX`] = markTemp[`offsetX`];
      mark[`offsetY`] = markTemp[`offsetY`];
      this.setData({markers:[
      mark
    ]});
    },
    "changeColor"(type,e){
      locationTemp[`${type}Color`] = e.value;
    },
    "refreshLocationStyle"(){
      this.setData({fillColor:locationTemp.fillColor});
      this.setData({strokeColor:locationTemp.strokeColor});
      this.setData({showmylocation:true});
    },
    "switchCompass"(){
      this.setData({compassSwitch:!this.data.compassSwitch});
    },
    "switchLooking"(){
      this.setData({lookingSwitch:!this.data.lookingSwitch});
    },
    "switchZoom"(){
      this.setData({zoomSwitch:!this.data.zoomSwitch});
    },
    "switchDrag"(){
      this.setData({dragSwitch:!this.data.dragSwitch});
    },
    "switchRotate"(){
      this.setData({rotateSwitch:!this.data.rotateSwitch});
    },
    "poitap"(data){
      prompt.showToast({
        "message":(((((((((((("poi id：" + data.poiId)) + "   poiName ：")) + data.poiName)) + "   latitude ：")) + data.latitude)) + "   longitude ：")) + data.longitude
});
    },
    "changePolygons"(type,e){
      markTemp[`polygons${type}`] = e.value;
    },
    "refreshPolygonsStyle"(){
      let polygon = this.data.polygons[0];
      polygon.strokeWidth = markTemp[`polygonsStrokeWidth`] + 'px';
      polygon.strokeColor = markTemp[`polygonsStrokeColor`];
      polygon.fillColor = markTemp[`polygonsFillColor`];
      this.setData({polygons:[
      polygon
    ]});
    },
    "showView"(){
      this.setData({isShowView:true});
    },
    "hideView"(){
      this.setData({isShowView:false});
    }
});

