import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'picker',
        "list":[
          '中国',
          '美国',
          '日本',
          '韩国'
        ],
        "country":'中国',
        "time":'',
        "date":''
},
    "onInit"(){
      const date = new Date();
      const Y = date.getFullYear();
      const M = date.getMonth() + 1;
      const D = date.getDate();
      const H = date.getHours();
      const m = date.getMinutes();
      this.setData({date:((((((Y + '-')) + M)) + '-')) + D});
      this.setData({time:((H + ':')) + m});
      this.$page.setTitleBar({
        "text":'Picker'
});
    },
    "getCountry"(e){
      this.setData({country:e.newValue});
    },
    "getTime"(e){
      this.setData({time:((e.hour + ':')) + e.minute});
    },
    "getDate"(e){
      this.setData({date:((((((e.year + '-')) + ((e.month + 1)))) + '-')) + e.day});
    },
    "show"(){
      this.$element('picker').show();
    }
});

