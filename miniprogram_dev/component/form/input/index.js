import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
onekit_showClickPrompt(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.showclickprompt0);this.showClickPrompt.apply(this,args);},
    "private":{
        "componentName":'input',
        "inputValue":'',
        "inputType":'text'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Input'
});
    },
    "changeInputType"(){
      if(this.data.inputType == 'text')
      {
        this.setData({inputType:'password'});
      }
else
      {
        this.setData({inputType:'text'});
      };
    },
    "updateValue"(e){
      this.setData({inputValue:e.text});
    },
    "setValue"(){
      this.setData({inputValue:'Hello, world!'});
    },
    "resetValue"(){
      this.setData({inputValue:''});
    },
    "showChangePrompt"(e){
      prompt.showToast({
        "message":(((((e.name == undefined)?'':('---name: ' + e.name) + (e.value == undefined)?'':('---value: ' + e.value))) + (e.checked == undefined)?'':('---checked: ' + e.checked))) + (e.text == undefined)?'':('---text: ' + e.text)
});
    },
    "showClickPrompt"(msg){
      prompt.showToast({
        "message":msg
});
    },
    "selectAll"(){
      this.$element('input').select();
    },
    "selectPart"(){
      this.$element('input').setSelectionRange({
        "start":0,
        "end":3
});
    },
    "getSelect"(){
      this.$element('input').getSelectionRange({
        "callback"(start,end){
          prompt.showToast({
            "message":(((('当前选中区域为：' + start)) + '~')) + end
});
        }
});
    },
    "onSelectionChange"(){
      this.$element('input').getSelectionRange({
        "callback"(start,end){
          if(((end - start)) > 3)
          {
            prompt.showToast({
                "message":'当前选中文本字符数大于3个'
});
          };
        }
});
    }
});

