import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'textarea',
        "lineHeight":0,
        "lineCount":0
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Textarea'
});
    },
    "onSelectionChange"(){
      this.$element('textarea').getSelectionRange({
        "callback"(start,end){
          if(((end - start)) > 3)
          {
            prompt.showToast({
                "message":'当前选中文本字符数大于3个'
});
          };
        }
});
    },
    "onLineChange"(msg){
      this.setData({lineHeight:msg.height});
      this.setData({lineCount:msg.lineCount});
    }
});

