import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
onekit_getValue(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.getvalue0);this.getValue.apply(this,args);},
    "private":{
        "componentName":'slider',
        "step":10,
        "currentValue1":50,
        "currentValue2":50,
        "currentValue3":50,
        "currentColor":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Slider'
});
    },
    "getValue"(name,evt){
      this.setData({name:evt.progress});
    },
    "changeColor"(evt){
      this.setData({currentColor:`rgb(125, 125, ${evt.progress})`});
    }
});

