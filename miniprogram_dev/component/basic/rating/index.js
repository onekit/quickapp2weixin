import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'rating',
        "starNum":0
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Rating'
});
    },
    "onstarStatus"(e){
      this.setData({starNum:e.rating});
    }
});

