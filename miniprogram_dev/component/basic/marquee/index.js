import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
onekit_stopMarquee(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.stopmarquee0);this.stopMarquee.apply(this,args);},
onekit_runMarquee(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.runmarquee0);this.runMarquee.apply(this,args);},
    "private":{
        "componentName":'marquee',
        "scrollSpeed":6
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Marquee'
});
    },
    "onReady"(){
      this.$element('marquee3').start();
    },
    "speedDown"(){
      this.data.scrollSpeed -= 20;
    },
    "speedUp"(){
      this.data.scrollSpeed += 20;
    },
    "showPopStart"(){
      prompt.showToast({
        "message":'跑马灯开始跑起来了！'
});
    },
    "showPopEnd"(){
      prompt.showToast({
        "message":'跑马灯跑到结尾了！'
});
    },
    "showPopFinish"(){
      prompt.showToast({
        "message":'最后的这个文字跑马灯跑完了！'
});
    },
    "runMarquee"(i){
      this.$element(`marquee${i}`).start();
    },
    "stopMarquee"(i){
      this.$element(`marquee${i}`).stop();
    }
});

