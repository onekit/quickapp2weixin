import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'image',
        "remoteURL":'http://www.w3school.com.cn/svg/',
        "inputImageURL":'http://www.w3school.com.cn/svg/rect1.svg',
        "requestImageURL":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Image'
});
    },
    "onImageUrlChange"(data){
      this.setData({inputImageURL:data.value});
    },
    "onImageGetClick"(){
      this.setData({requestImageURL:this.data.inputImageURL});
      prompt.showToast({
        "message":(('加载' + this.data.requestImageURL)) + '图片'
});
    },
    "onImageComplete"(data){
      prompt.showToast({
        "message":(((((((((('图片获取成功\n' + '高度：')) + data.height)) + 'px  ')) + '宽度：')) + data.width)) + 'px'
});
    },
    "onImageError"(){
      prompt.showToast({
        "message":'图片获取失败'
});
    }
});

