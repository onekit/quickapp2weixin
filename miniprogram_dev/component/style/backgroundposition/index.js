import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'background-position',
        "position":"0px 0px"
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "edit"(e){
      this.setData({position:e.text});
    }
});

