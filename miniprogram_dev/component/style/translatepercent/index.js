import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'translate 百分比的动画',
        "t":'50%',
        "translate":'translate'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "changeSize"(){
      this.setData({t:(this.data.t == '50%')?'60%':'50%'});
    },
    "switch"(){
      this.setData({translate:(this.data.translate == 'translate')?'':'translate'});
    }
});

