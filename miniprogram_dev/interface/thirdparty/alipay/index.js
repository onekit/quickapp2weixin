import {Page,Component} from '../../../quickapp2weixin/index.js';
import {service_alipay} from '../../../quickapp2weixin/index';const alipay = service_alipay;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_fetch} from '../../../quickapp2weixin/index';const fetch = system_fetch;
Page({
    "private":{
        "componentName":'alipay'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Alipay'
});
    },
    "useAlipay"(){
      fetch.fetch({
        "url":'https://www.quickapp.cn/',
        "data":{
            "type":'ali_pay'
},
        "method":'POST',
        "success"(ret){
          const order = JSON.parse(ret.data).data.order_info;
          prompt.showToast({
            "message":'fetch order success  ' + order
});
          alipay.pay({
            "orderInfo":order,
            "callback"(ret){
              if(ret.resultStatus == '9000')
              {
                prompt.showToast({
                    "message":'支付成功：' + JSON.stringify(ret)
});
              }
else
              if(ret.resultStatus == '6001')
                {
                  prompt.showToast({
                      "message":'取消支付'
});
                }
else
                {
                  prompt.showToast({
                      "message":'支付失败：' + ret.resultStatus
});
                };
            }
});
        }
});
    }
});

