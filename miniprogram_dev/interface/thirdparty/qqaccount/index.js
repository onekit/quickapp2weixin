import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {service_qqaccount} from '../../../quickapp2weixin/index';const qqaccount = service_qqaccount;
Page({
    "data":{
        "componentName":'QQ账号'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'QQ账号'
});
    },
    "useQQAccount"(){
      let type = qqaccount.getType();
      if(type == 'APP')
      {
        qqaccount.authorize({
            "scope":'all',
            "success"(ret){
              console.info(`### qqaccount authorize success ### data:${JSON.stringify(ret)}`);
            },
            "fail"(errorMsg,errorCode){
              console.info(`### qqaccount authorize fail ### code:${errorCode}, message:${errorMsg}`);
            },
            "cancel"(){
              console.info(`### qqaccount authorize cancelled ###`);
            }
});
      }
else
      if(type == 'WEB')
        {
          qqaccount.authorize({
              "state":'randomString',
              "scope":'all',
              "redirectUri":'xxx',
              "fail"(data,code){
                console.info(`### qqaccount fail ### data:${JSON.stringify(data)}, code: ${code}`);
              },
              "cancel"(data){
                console.info(`### qqaccount cancel ### data: ${JSON.stringify(data)}`);
              },
              "success"(data){
                console.info(`### qqaccount success ### data: ${JSON.stringify(data)}`);
              }
});
        }
else
        {
          console.info(`### qqaccount not available ###`);
        };
    }
});

