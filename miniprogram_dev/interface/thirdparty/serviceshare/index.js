import {Page,Component} from '../../../quickapp2weixin/index.js';
import {service_share} from '../../../quickapp2weixin/index';const share = service_share;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'service.share',
        "title":'默认标题',
        "summary":'默认摘要',
        "platforms":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Service Share'
});
      this.getAllThePlatforms();
    },
    "updateTitle"(e){
      this.setData({title:e.text});
    },
    "updateSummary"(e){
      this.setData({summary:e.text});
    },
    "share"(){
      const self = this;
      share.share({
        "shareType":0,
        "title":self.title,
        "summary":self.summary,
        "targetUrl":'https://www.quickapp.cn/',
        "fail"(erromsg,errocode){
          console.info(`### share.share ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "shareTo"(){
      const self = this;
      share.share({
        "shareType":0,
        "title":self.title,
        "summary":self.summary,
        "targetUrl":'https://www.quickapp.cn/',
        "platforms":[
          'SYSTEM'
        ],
        "fail"(erromsg,errocode){
          console.info(`### share.share ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "getAllThePlatforms"(){
      const self = this;
      share.getAvailablePlatforms({
        "success"(data){
          self.platforms = data.platforms.join(',');
        },
        "fail"(data,code){
          console.info(`### "handling fail ### code: ${code}`);
        }
});
    }
});

