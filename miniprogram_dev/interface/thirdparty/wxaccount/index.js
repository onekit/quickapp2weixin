import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {service_wxaccount} from '../../../quickapp2weixin/index';const wxaccount = service_wxaccount;
import {system_fetch} from '../../../quickapp2weixin/index';const fetch = system_fetch;
Page({
    "data":{
        "componentName":'微信账号',
        "code":null
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'微信账号'
});
    },
    "useWXAccount"(){
      let type = wxaccount.getType();
      if(type == 'APP')
      {
        wxaccount.authorize({
            "scope":'snsapi_userinfo',
            "state":'randomString',
            "fail":(data,code)=>{console.info(`### wxaccount fail ### data:${JSON.stringify(data)}, code:${code}`)},
            "cancel":(data)=>{console.info(`### wxaccount cancel ### data:${JSON.stringify(data)}`)},
            "success":(data)=>{
              console.info(`### wxaccount success ### data:${JSON.stringify(data)}`);
              this.setData({code:data.code});
            }
});
      }
else
      {
        console.info(`### wxaccount not available ###`);
      };
    },
    "fetchUserInfo"(){
      var appid = 'xxx';
      var secret = 'xxx';
      var fetchTokenUrl = (((((((((('https://api.weixin.qq.com/sns/oauth2/access_token?appid=' + appid)) + '&secret=')) + secret)) + '&code=')) + this.data.code)) + '&grant_type=authorization_code';
      fetch.fetch({
        "url":fetchTokenUrl,
        "success":(ret)=>{
          var data = JSON.parse(ret.data);
          var userInfoUrl = (((('https://api.weixin.qq.com/sns/userinfo?access_token=' + data['access_token'])) + '&openid=')) + data['openid'];
          fetch.fetch({
              "url":userInfoUrl,
              "success":(ret)=>{console.info(`### fetch userInfo ### result=${JSON.stringify(ret)}`)}
});
        }
});
    }
});

