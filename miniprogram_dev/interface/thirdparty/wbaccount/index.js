import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {service_wbaccount} from '../../../quickapp2weixin/index';const wbaccount = service_wbaccount;
Page({
    "data":{
        "componentName":'微博账号'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'微博账号'
});
    },
    "useWBAccount"(){
      wbaccount.authorize({
        "redirectUri":'https://api.weibo.com/oauth2/default.html',
        "scope":'follow_app_official_microblog',
        "success"(data){
          console.info(`### handling success### accessToken: ${data.accessToken}`);
        },
        "fail"(data,code){
          console.info(`### handling fail### result data: ${data}, code: ${code}`);
        },
        "cancel"(data){
          console.info(`### handling cancel###`);
        }
});
    }
});

