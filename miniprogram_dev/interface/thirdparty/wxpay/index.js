import {Page,Component} from '../../../quickapp2weixin/index.js';
import {service_wxpay} from '../../../quickapp2weixin/index';const wxpay = service_wxpay;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_fetch} from '../../../quickapp2weixin/index';const fetch = system_fetch;
Page({
onekit_useWxpay(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.usewxpay0);this.useWxpay.apply(this,args);},
    "private":{
        "componentName":'wxpay'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Wxpay'
});
    },
    "useWxpay"(type){
      const payType = wxpay.getType();
      if(payType == 'APP')
      {
        prompt.showToast({
            "message":'当前可用微信支付方式：原生支付'
});
        fetch.fetch({
            "url":'https://www.quickapp.cn/',
            "success"(ret){
              const order = JSON.parse(ret.data);
              console.info('WXPAY:prepayid:' + order.prepayid);
              prompt.showToast({
                "message":'fetch order success  ' + ret.data
});
              wxpay.pay({
                "prepayid":order.prepayid,
                "extra":{
                    "app_id":order.appid,
                    "partner_id":order.partnerid,
                    "package_value":order.package,
                    "nonce_str":order.noncestr,
                    "time_stamp":order.timestamp,
                    "order_sign":order.sign
},
                "fail"(data,code){
                  console.info((((('WXPAY handling fail, code=' + code)) + ' data:')) + JSON.stringify(data));
                  if(code == '900')
                  {
                    prompt.showToast({
                        "message":'支付失败：签名错误'
});
                  }
else
                  if(code == '901')
                    {
                      prompt.showToast({
                          "message":'支付失败：包名错误'
});
                    }
else
                    if(code == '1000')
                      {
                        prompt.showToast({
                            "message":'支付失败：未安装微信'
});
                      }
else
                      if(code == '2001')
                        {
                          prompt.showToast({
                              "message":'支付失败：微信app返回订单错误'
});
                        }
else
                        {
                          prompt.showToast({
                              "message":'支付失败：' + code
});
                        };
                },
                "cancel"(){
                  console.info('WXPAY handling cancel');
                },
                "success"(data){
                  console.info((('WXPAY handling success' + ' data:')) + JSON.stringify(data));
                  prompt.showToast({
                    "message":'支付提交微信成功'
});
                }
});
            }
});
      }
else
      if(payType == 'MWEB')
        {
          prompt.showToast({
              "message":'当前可用微信支付方式：网页支付'
});
          let param = {
};
          if(type == 'referer')
            {
              param = {
                  "prepayid":'xxxxxxxxxx',
                  "referer":'http://****',
                  "extra":{
                      "mweb_url":'http://****'
}
};
            }
else
            {
              param = {
                  "prepayid":'xxxxxxxxxx',
                  "extra":{
                      "customeKey1":'customeValue1',
                      "customeKey2":'customeValue2'
}
};
            }
          const callbackFn = {
              "fail"(data,code){
                console.info((((('H5 WXPAY handling fail, code=' + code)) + ' data:')) + JSON.stringify(data));
                if(code == '1000')
                {
                  prompt.showToast({
                      "message":'支付失败：未安装微信'
});
                }
else
                if(code == '1001')
                  {
                    prompt.showToast({
                        "message":'支付失败：url not found'
});
                  }
else
                  {
                    prompt.showToast({
                        "message":'支付失败：' + code
});
                  };
              },
              "cancel"(){
                console.info('H5 WXPAY handling cancel');
              },
              "success"(data){
                console.info((('H5 WXPAY handling success' + ' data:')) + JSON.stringify(data));
                prompt.showToast({
                  "message":'支付提交微信成功'
});
              }
};
          wxpay.pay(Object.assign({
},param,callbackFn));
        }
else
        {
          prompt.showToast({
              "message":'当前无可用微信支付方式'
});
        };
    }
});

