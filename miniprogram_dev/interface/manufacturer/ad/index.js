import {Page,Component} from '../../../quickapp2weixin/index.js';
import {service_ad} from '../../../quickapp2weixin/index';const ad = service_ad;
Page({
onekit_reportNativeAdClick(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.reportnativeadclick0);this.reportNativeAdClick.apply(this,args);},
onekit_nativeHide(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.nativehide0);this.nativeHide.apply(this,args);},
onekit_reportNativeAdShow(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.reportnativeadshow0);this.reportNativeAdShow.apply(this,args);},
    "data":{
        "bannerStyle":{
},
        "bannerAds":[
        ],
        "bannerStyleIndex":-1,
        "interstitialAd":null,
        "nativeAd":null,
        "show":true,
        "componentName":'广告',
        "nativeAdResult":null,
        "logs":[
        ],
        "cbsBanner":[
        ],
        "nativeIsShow":false,
        "nativeDataList":[
        ],
        "nativeOnErrorFunc":null,
        "bannerStyleAttr":'width',
        "bannerStyleAttrVal":100,
        "bannerAdUnitIds":[
          '1'
        ],
        "interstitialAdUnitId":'2',
        "nativeAdUnitId":'3',
        "rewardedVideoAdUnitId":'4'
},
    "generateCbs"(method,adIndex,func,funcIndex){
      let result = null;
      if(this.data.cbsBanner[adIndex])
      {
        if(!this.data.cbsBanner[adIndex][method])
          {
            this.data.cbsBanner[adIndex][method] = {
};
          }
        if(typeof this.data.cbsBanner[adIndex][method][funcIndex] !== 'function')
          {
            this.data.cbsBanner[adIndex][method][funcIndex] = func.bind(this);
          }
        result = this.data.cbsBanner[adIndex][method][funcIndex];
      };
      return result;
    },
    "getCbs"(method,adIndex,funcIndex){
      let result = null;
      if((this.data.cbsBanner[adIndex]) && (this.data.cbsBanner[adIndex][method]))
      {
        result = this.data.cbsBanner[adIndex][method][funcIndex];
      };
      return result;
    },
    "bannerStyleAttrValCreateChange"(e){
      let attrName = e.target.attr.name;
      if(e.value !== '')
      {
        this.setData({[`bannerStyle.${attrName}`]:e.value});
      }
else
      {
        deletethis.data.bannerStyle[attrName];
      };
    },
    "onInit"(){
      this.$page.setTitleBar({
        "text":'广告'
});
      this.data.logs.unshift(`provider: ${ad.getProvider()}`);
    },
    "initBannerAd"(){
      let bannerAd = ad.createBannerAd({
        "adUnitId":this.data.bannerAdUnitIds[this.data.bannerAds.length % this.data.bannerAdUnitIds.length],
        "style":this.data.bannerStyle
});
      const bannerLen =     const bannerAds=this.data.bannerAds;bannerAds.push(bannerAd);this.setData({[`bannerAds`]:bannerAds});
      this.setData({[`cbsBanner.${bannerLen - 1}`]:{
}});
      this.data.logs.unshift(`banner广告创建，监听onError、onLoad、onShow、onClose事件，广告实例数量${bannerLen}，传参：${JSON.stringify(this.data.bannerStyle)}`);
      bannerAd.onError(this.generateCbs('onError',bannerLen - 1,this.data.bannerOnError,0));
      bannerAd.onClose(this.generateCbs('onClose',bannerLen - 1,this.data.bannerOnCloseA,0));
      bannerAd.onResize(this.generateCbs('onResize',bannerLen - 1,this.data.bannerOnResize,0));
      bannerAd.onLoad(this.generateCbs('onLoad',bannerLen - 1,this.data.bannerOnLoadA,'A'));
      setTimeout(()=>{bannerAd.show().then(()=>{this.data.logs.unshift(`banner 广告show成功, realWith ${bannerAd.style.realWidth}, realHeight ${bannerAd.style.realHeight}`)},()=>{this.data.logs.unshift('banner 广告show失败')})},10);
    },
    "listenBannerAdOnloadB"(){
      for(const i in this.data.bannerAds){
        this.data.bannerAds[i].onLoad(this.generateCbs('onLoad',i,this.data.bannerOnLoadB,'B'));
      };
      this.data.logs.unshift('新增banner广告load监听事件B');
    },
    "showBannerAd"(){
      for(const i in this.data.bannerAds){
        this.data.bannerAds[i].show().then(()=>{this.data.logs.unshift('banner 广告show成功')},()=>{this.data.logs.unshift('banner 广告show失败')});
      };
      this.data.logs.unshift('调用banner广告展示');
    },
    "hideBannerAd"(){
      for(const i in this.data.bannerAds){
        this.data.bannerAds[i].hide().then(()=>{this.data.logs.unshift('banner 广告hide成功')},()=>{this.data.logs.unshift('banner 广告hide失败')});
      };
      this.data.logs.unshift('调用banner广告隐藏');
    },
    "offLoadB"(){
      for(const i in this.data.bannerAds){
        let cb = this.getCbs('onLoad',i,'B');
        this.data.bannerAds[i].offLoad(cb);
      };
      this.data.logs.unshift('移除banner广告load监听事件B');
    },
    "offLoadAll"(){
      for(const i in this.data.bannerAds){
        this.data.bannerAds[i].offLoad();
      };
      this.data.logs.unshift('移除banner广告全部load监听事件');
    },
    "offErrorAll"(){
      for(const i in this.data.bannerAds){
        this.data.bannerAds[i].offError();
      };
      this.data.logs.unshift('移除banner广告error监听事件');
    },
    "destroyBannerAd"(){
      for(const i in this.data.bannerAds){
        this.data.bannerAds[i].destroy();
      };
      this.data.logs.unshift('销毁banner广告');
      this.setData({bannerAds:[
    ]});
    },
    "bannerOnShow"(){
      const str = `bannerAd show 事件监听`;
      this.data.logs.unshift(str);
    },
    "bannerOnError"(err){
      const str = `banner error: ${err.errMsg}, ${err.errCode}`;
      this.data.logs.unshift(str);
    },
    "bannerOnLoadA"(){
      const str = 'bannerAd load 事件监听A';
      this.data.logs.unshift(str);
    },
    "bannerOnLoadB"(){
      const str = 'bannerAd load 事件监听B';
      this.data.logs.unshift(str);
    },
    "bannerOnCloseA"(){
      const str = 'bannerAd close 事件监听A';
      this.data.logs.unshift(str);
    },
    "bannerOnCloseB"(){
      const str = 'bannerAd close 事件监听B';
      this.data.logs.unshift(str);
    },
    "listenBannerAdOnCloseB"(){
      this.data.logs.unshift('新增banner广告隐藏监听事件');
      for(const i in this.data.bannerAds){
        this.data.bannerAds[i].onClose(this.generateCbs('onClose',this.data.bannerAds.length - 1,this.data.bannerOnCloseB,1));
      };
    },
    "offCloseAll"(){
      this.data.logs.unshift('移除bannerAd 全部关闭监听事件');
      for(const i in this.data.bannerAds){
        this.data.bannerAds[i].offClose();
      };
    },
    "bannerStyleAttrChange"(e){
      this.setData({bannerStyleAttr:e.newValue});
    },
    "bannerStyleAttrValChange"(e){
      this.setData({bannerStyleAttrVal:e.value});
    },
    "bannerStyleIndexChange"(e){
      this.setData({bannerStyleIndex:parseInt(e.newValue,10)});
    },
    "setBannerStyle"(){
      for(const i in this.data.bannerAds){
        if((this.data.bannerStyleIndex == -1) || ((this.data.bannerStyleIndex != -1) && this.data.bannerStyleIndex == i))
          {
            this.data.bannerAds[i].style[this.data.bannerStyleAttr] = this.data.bannerStyleAttrVal;
            const str = `第${i}个bannerAd style 设置${this.data.bannerStyleAttr}值：${this.data.bannerStyleAttrVal}`;
            this.data.logs.unshift(str);
          }
      };
    },
    "bannerOnResize"(res){
      let str = `bannerAd style设置回调 res值:${JSON.stringify(res)}值；`;
      for(const i in this.data.bannerAds){
        str += `第${i}个bannerAd style 值：${JSON.stringify(this.data.bannerAds[i].style)},`;
        str += `real width: ${this.data.bannerAds[i].style.realWidth} , real height:  ${this.data.bannerAds[i].style.realHeight}`;
      };
      this.data.logs.unshift(str);
    },
    "initInterstitialAd"(){
      let interstitialAd = ad.createInterstitialAd({
        "adUnitId":this.data.interstitialAdUnitId
});
      this.setData({interstitialAd:interstitialAd});
      interstitialAd.onError(this.data.interstitialOnError.bind(this));
      interstitialAd.onLoad(this.data.interstitialOnLoad.bind(this));
      interstitialAd.onClose(this.data.interstitialOnClose.bind(this));
      this.data.logs.unshift('插屏广告初始化，增加事件监听');
    },
    "showInterstitialAd"(){
      this.data.interstitialAd.show().then(()=>{this.data.logs.unshift('插屏广告show成功')},()=>{this.data.logs.unshift('插屏广告show失败')});
      this.data.logs.unshift('调用插屏广告展示');
    },
    "interstitialOnShow"(){
      const str = `插屏广告展示成功`;
      this.data.logs.unshift(str);
    },
    "interstitialOnError"(err){
      const str = `插屏广告错误: ${err.errMsg}, ${err.errCode}`;
      this.data.logs.unshift(str);
    },
    "interstitialOnLoad"(){
      const str = '插屏广告 load 事件';
      this.data.logs.unshift(str);
    },
    "interstitialOnClose"(){
      const str = '插屏广告 close 事件';
      this.data.logs.unshift(str);
    },
    "interstitialAdDestroy"(){
      this.data.interstitialAd.destroy();
      this.data.logs.unshift(`插屏广告销毁`);
    },
    "interstitialAdOffError"(){
      this.data.interstitialAd.offError();
      this.data.logs.unshift(`插屏广告取消error监听`);
    },
    "interstitialAdOffLoad"(){
      this.data.interstitialAd.offLoad();
      this.data.logs.unshift(`插屏广告取消load监听`);
    },
    "interstitialAdOffClose"(){
      this.data.interstitialAd.offClose();
      this.data.logs.unshift(`插屏广告取消close监听`);
    },
    "nativeAdUnitIdChange"(e){
      this.setData({nativeAdUnitId:e.value});
    },
    "initNativeAd"(){
      let nativeAd = ad.createNativeAd({
        "adUnitId":this.data.nativeAdUnitId
});
      this.setData({nativeAd:nativeAd});
      nativeAd.onLoad(this.data.nativeOnLoad.bind(this));
      nativeAd.onError(this.data.nativeOnError.bind(this));
      this.data.logs.unshift('原生广告已创建，增加事件监听');
    },
    "loadNativeAd"(){
      this.data.nativeAd.load();
      this.data.logs.unshift('加载原生广告');
    },
    "nativeOnError"(err){
      const str = `原生广告错误: ${err.errMsg}, ${err.errCode}`;
      console.log(str);
      this.data.logs.unshift(str);
      this.setData({nativeIsShow:false});
    },
    "nativeOnLoad"(result){
      console.log('原生广告结果',JSON.stringify(result));
      this.setData({nativeAdResult:result});
      this.setData({nativeDataList:result.adList});
      this.data.logs.unshift(`原生广告加载成功，内容:${JSON.stringify(result)}`);
      this.setData({nativeIsShow:true});
    },
    "reportNativeAdClick"(adId){
      if(((this.data.nativeAdResult) && (this.data.nativeAdResult.adList)) && this.data.nativeAdResult.adList.length > 0)
      {
        this.data.nativeAd.reportAdClick({
            adId
});
        this.data.logs.unshift(`原生广告点击上报，adid:${adId}`);
      };
    },
    "reportNativeAdShow"(adId){
      if(((this.data.nativeAdResult) && (this.data.nativeAdResult.adList)) && this.data.nativeAdResult.adList.length > 0)
      {
        this.data.nativeAd.reportAdShow({
            adId
});
        this.data.logs.unshift(`原生广告展示上报，adid:${adId}`);
      };
    },
    "nativeDestroy"(){
      this.data.nativeAd.destroy();
      this.data.logs.unshift(`原生广告销毁`);
      this.setData({nativeIsShow:false});
    },
    "nativeOffError"(){
      this.data.nativeAd.offError();
      this.data.logs.unshift(`原生广告取消error监听`);
    },
    "nativeOffLoad"(){
      this.data.nativeAd.offLoad();
      this.data.logs.unshift(`原生广告取消load监听`);
    },
    "nativeHide"(idx,e){
      if(e.stopPropagation)
      {
        e.stopPropagation();
      };
      this.data.nativeDataList.splice(idx,1);
    },
    "initRewardedVideoAd"(){
      let rewardedVideoAd = ad.createRewardedVideoAd({
        "adUnitId":this.data.rewardedVideoAdUnitId
});
      this.setData({rewardedVideoAd:rewardedVideoAd});
      rewardedVideoAd.onError(this.data.rewardedVideoOnError.bind(this));
      rewardedVideoAd.onLoad(this.data.rewardedVideoOnLoad.bind(this));
      rewardedVideoAd.onClose(this.data.rewardedVideoOnClose.bind(this));
      this.data.logs.unshift('激励视频广告初始化，增加事件监听');
    },
    "loadRewardedVideoAd"(){
      this.data.rewardedVideoAd.load();
      this.data.logs.unshift('调用激励视频广告加载');
    },
    "showRewardedVideoAd"(){
      this.data.rewardedVideoAd.show();
      this.data.logs.unshift('调用激励视频广告展示');
    },
    "rewardedVideoOnError"(err){
      const str = `激励视频广告错误: ${err.errMsg}, ${err.errCode}`;
      console.log(str);
      this.data.logs.unshift(str);
    },
    "rewardedVideoOnLoad"(){
      const str = '激励视频广告 load 事件';
      console.log(str);
      this.data.logs.unshift(str);
    },
    "rewardedVideoOnClose"(res){
      const str = `激励视频广告 close 事件，是否观看完整：${res.isEnded}`;
      console.log(str);
      this.data.logs.unshift(str);
    },
    "rewardedVideoOffError"(){
      this.data.rewardedVideoAd.offError();
      this.data.logs.unshift(`激励视频广告取消error监听`);
    },
    "rewardedVideoOffClose"(){
      this.data.rewardedVideoAd.offClose();
      this.data.logs.unshift(`激励视频广告取消close监听`);
    },
    "rewardedVideoOffLoad"(){
      this.data.rewardedVideoAd.offLoad();
      this.data.logs.unshift(`激励视频广告取消load监听`);
    },
    "destroyRewardedVideo"(){
      this.data.rewardedVideoAd.destroy();
      this.data.logs.unshift(`激励视频广告destroy`);
    }
});

