import {Page,Component} from '../../../quickapp2weixin/index.js';
let type;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_media} from '../../../quickapp2weixin/index';const media = system_media;
Page({
onekit_setRingtone(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.setringtone0);this.setRingtone.apply(this,args);},
onekit_getRingtone(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.getringtone0);this.getRingtone.apply(this,args);},
    "private":{
        "componentName":'media',
        "pathPhotoSave":'',
        "pathPhotoPick":'',
        "pathVideoSave":'',
        "pathVideoPick":'',
        "pathRecordStart":'',
        "pathRecordStop":'',
        "photoUri":'',
        "videoUri":'',
        "alarmName":'',
        "ringtoneName":'',
        "notificationName":'',
        "pathFilePick":'',
        "pathFileName":'',
        "pathFileSize":0,
        "isDisplayOneImage":true,
        "pathFilePickArr":[
        ]
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Media'
});
    },
    "takePhoto"(){
      const self = this;
      media.takePhoto({
        "success"(ret){
          self.pathPhotoSave = ret.uri;
          self.photoUri = ret.uri;
        },
        "fail"(erromsg,errocode){
          console.info(`### media.takePhoto ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "pickPhoto"(){
      const self = this;
      media.pickImage({
        "success"(ret){
          self.pathPhotoPick = ret.uri;
          self.photoUri = ret.uri;
          self.isDisplayOneImage = true;
        },
        "fail"(erromsg,errocode){
          console.info(`### media.pickImage ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "pickPhotos"(){
      media.pickImages({
        "success":(data)=>{
          this.setData({pathFilePickArr:data.uris});
          this.setData({isDisplayOneImage:false});
        },
        "fail"(erromsg,errocode){
          console.info(`### media.pickImages ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "takeVideo"(){
      const self = this;
      media.takeVideo({
        "success"(ret){
          self.pathVideoSave = ret.uri;
          self.videoUri = ret.uri;
        },
        "fail"(erromsg,errocode){
          console.info(`### media.takeVideo ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "pickVideo"(){
      const self = this;
      media.pickVideo({
        "success"(ret){
          self.pathVideoPick = ret.uri;
          self.videoUri = ret.uri;
        },
        "fail"(erromsg,errocode){
          console.info(`### media.pickVideo ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "pickFile"(){
      const self = this;
      media.pickFile({
        "success"(ret){
          self.pathFilePick = ret.uri;
          self.pathFileName = ret.name;
          self.pathFileSize = ret.size;
        },
        "fail"(erromsg,errocode){
          console.info(`### media.pickFile ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "saveImageToAlbum"(){
      if(!this.data.pathPhotoSave)
      {
        prompt.showToast({
            "message":`please take a photo firstly`
});
        return;
      };
      media.saveToPhotosAlbum({
        "uri":this.data.pathPhotoSave,
        "success"(){
          prompt.showToast({
            "message":`save success`
});
        },
        "fail"(erromsg,errocode){
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "saveVideoToAlbum"(){
      if(!this.data.pathVideoSave)
      {
        prompt.showToast({
            "message":`please record a video firstly`
});
        return;
      };
      media.saveToPhotosAlbum({
        "uri":this.data.pathVideoSave,
        "success"(){
          prompt.showToast({
            "message":`save success`
});
        },
        "fail"(erromsg,errocode){
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "setRingtone"(type){
      const _this = this;
      media.pickFile({
        "success"(data){
          media.setRingtone({
            "uri":data.uri,
            type,
            "success"(){
              prompt.showToast({
                "message":'设置成功'
});
              type = type + 'Name';
              _this[type] = data.uri.replace(/(.*\/)*([^.]+).*/ig,'$2');
            },
            "fail"(erromsg){
              prompt.showToast({
                "message":erromsg
});
            }
});
        }
});
    },
    "getRingtone"(type){
      const _this = this;
      media.getRingtone({
        type,
        "success"(data){
          type = type + 'Name';
          _this[type] = data.title;
        },
        "fail"(erromsg){
          prompt.showToast({
            "message":erromsg
});
        }
});
    },
    "previewImage"(){
      media.previewImage({
        "current":'https://via.placeholder.com/300.png/',
        "uris":[
          'https://via.placeholder.com/500.png/',
          'https://via.placeholder.com/400.png/',
          'https://via.placeholder.com/300.png/',
          'https://via.placeholder.com/200.png/',
          'https://via.placeholder.com/100.png/'
        ]
});
    }
});

