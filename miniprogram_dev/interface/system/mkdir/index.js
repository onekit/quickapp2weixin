import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_file} from '../../../quickapp2weixin/index';const file = system_file;
import {system_fetch} from '../../../quickapp2weixin/index';const fetch = system_fetch;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'目录读写',
        "dstUri":'internal://files/newtestfile/',
        "createMkdir":'',
        "deleteMkdir":'',
        "accessMkdir":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'目录读写'
});
    },
    "createMkdirData"(){
      const self = this;
      file.mkdir({
        "uri":self.dstUri,
        "success"(ret){
          self.deleteMkdir = '';
          self.accessMkdir = '';
          self.createMkdir = JSON.stringify(ret);
        },
        "fail"(errmsg,errcode){
          self.createMkdir = ((errcode + '---')) + errmsg;
          console.info(`### file.mkdir ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    },
    "deleteMkdirData"(){
      const self = this;
      file.rmdir({
        "uri":self.dstUri,
        "success"(ret){
          self.createMkdir = '';
          self.accessMkdir = '';
          self.deleteMkdir = JSON.stringify(ret);
        },
        "fail"(errmsg,errcode){
          self.deleteMkdir = ((errcode + '---')) + errmsg;
          console.info(`### file.rmdir ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    },
    "accessMkdirData"(){
      const self = this;
      file.access({
        "uri":self.dstUri,
        "success"(ret){
          self.createMkdir = '';
          self.deleteMkdir = '';
          self.accessMkdir = JSON.stringify(ret);
        },
        "fail"(errmsg,errcode){
          self.accessMkdir = ((errcode + '---')) + errmsg;
          console.info(`### file.access ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    }
});

