import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_package} from '../../../quickapp2weixin/index';const pkg = system_package;
Page({
    "private":{
        "componentName":'Package',
        "status":'',
        "packageName":'com.xiangkan.android',
        "packageStatus":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'应用管理'
});
    },
    "hasInstalled"(){
      const self = this;
      pkg.hasInstalled({
        "package":self.packageName,
        "success"(data){
          self.status = data.result;
        },
        "fail"(data,code){
          self.status = 'handling fail, code=' + code;
        }
});
    },
    "install"(){
      const self = this;
      pkg.install({
        "package":self.packageName,
        "success"(data){
          self.packageStatus = (data.result == true)?'安装成功':'安装失败';
        },
        "fail"(err,code){
          self.packageStatus = 'handling fail, code=' + code;
        }
});
    }
});

