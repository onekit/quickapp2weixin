import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_sms} from '../../../quickapp2weixin/index';const sms = system_sms;
Page({
    "data":{
        "componentName":'Short Message',
        "number":'',
        "content":'',
        "smsContent":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "numberChange"(e){
      this.setData({number:e.text});
    },
    "contentChange"(e){
      this.setData({content:e.text});
    },
    "readSMS"(){
      sms.readSafely({
        "success"(data){
          this.setData({smsContent:data.message});
          console.log('handling success. message=' + data.message);
        },
        "fail"(data,code){
          this.setData({smsContent:((data + ' ')) + code});
          console.log((((('handling fail, result data=' + data)) + ',code=')) + code);
        }
});
    },
    "sendSMS"(){
      sms.send({
        "address":this.data.number,
        "content":this.data.content,
        "success"(){
          prompt.showToast({
            "message":"发送成功"
});
        },
        "fail"(){
          prompt.showToast({
            "message":"发送失败"
});
        }
});
    }
});

