import {Page,Component} from '../../../quickapp2weixin/index.js';
import {service_health} from '../../../quickapp2weixin/index';const health = service_health;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "data":{
        "componentName":'sensor',
        "supportDayStep":'',
        "todaySteps":0,
        "weekSteps":[
          {
              "date":'2019-02-20',
              "steps":20100
}
        ]
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Health'
});
      health.hasStepsOfDay({
        "success":(data)=>{this.setData({supportDayStep:data.support})},
        "fail":(err,code)=>{prompt.showToast({
            "message":`失败原因：${err} 错误码：${code}`
})}
});
    },
    "getTodaySteps"(){
      health.getTodaySteps({
        "success":(data)=>{this.setData({todaySteps:data.steps})},
        "fail":(err,code)=>{
          let info = '';
          if(code == '201')
            {
              info = '用户拒绝，无法获取每日步数' + code;
            }
else
            if(code == '1001')
              {
                info = '不支持按天获取步数' + code;
              };
          prompt.showToast({
              "message":info
});
        }
});
    },
    "getLastWeekSteps"(){
      health.getLastWeekSteps({
        "success":(data)=>{this.setData({weekSteps:data.stepsList})},
        "fail":(err,code)=>{
          let info = '';
          if(code == '201')
            {
              info = '用户拒绝，无法获取每日步数' + code;
            }
else
            if(code == '1001')
              {
                info = '不支持按天获取步数' + code;
              };
          prompt.showToast({
              "message":info
});
        }
});
    }
});

