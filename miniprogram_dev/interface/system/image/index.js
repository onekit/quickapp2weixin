import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_image} from '../../../quickapp2weixin/index';const image = system_image;
import {system_request} from '../../../quickapp2weixin/index';const request = system_request;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'image',
        "imageUri":'',
        "showMask":true,
        "hint":'Loading...',
        "imageGetInfo":{
},
        "imageCompressInfo":{
},
        "imageOperateInfo":{
},
        "imageEditInfo":{
},
        "exifimageUri":'/interface/system/image/img/pic-160x160.jpg',
        "attributes":'',
        "aspectRatioX":16,
        "aspectRatioY":9
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Image'
});
      this.preTask();
    },
    "preTask"(){
      const self = this;
      request.download({
        "url":'https://ss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=2247692397,1189743173&fm=5',
        "success"(data){
          request.onDownloadComplete({
            "token":data.token,
            "success"(ret){
              self.imageUri = ret.uri;
              self.showMask = false;
            },
            "fail"(erromsg,errocode){
              self.hint = `${errocode}: ${erromsg}`;
              console.info(`### request.onDownloadComplete ### ${errocode}: ${erromsg}`);
              prompt.showToast({
                "message":`${errocode}: ${erromsg}`
});
            }
});
        }
});
    },
    "showToast"(){
      prompt.showToast({
        "message":`${this.data.hint}`
});
    },
    "getImageInfo"(){
      const self = this;
      image.getImageInfo({
        "uri":self.imageUri,
        "success"(data){
          self.imageGetInfo = data;
        },
        "fail"(erromsg,errocode){
          console.info(`### image.getInfo ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        },
        "complete"(){
          prompt.showToast({
            "message":`complete`
});
        }
});
    },
    "setImageExif"(){
      image.setExifAttributes({
        "uri":this.data.exifimageUri,
        "attributes":{
            "Orientation":'1',
            "Make":'quick app'
},
        "success"(data){
          prompt.showToast({
            "message":`handling success`
});
        },
        "fail"(data,code){
          prompt.showToast({
            "message":`handling fail, code = ${code}`
});
        }
});
    },
    "getImageExif"(){
      image.getExifAttributes({
        "uri":this.data.exifimageUri,
        "success":(data)=>{
          this.setData({attributes:JSON.stringify(data.attributes)});
          prompt.showToast({
              "message":`handling success`
});
        },
        "fail"(data,code){
          prompt.showToast({
            "message":`handling fail, code = ${code}`
});
        }
});
    },
    "compressImage"(){
      const self = this;
      image.compressImage({
        "uri":self.imageUri,
        "quality":50,
        "ratio":5,
        "format":'WEBP',
        "success"(data){
          self.imageCompressInfo = data;
        },
        "fail"(erromsg,errocode){
          console.info(`### image.compress ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        },
        "complete"(){
          prompt.showToast({
            "message":`complete`
});
        }
});
    },
    "operateImage"(){
      const self = this;
      image.applyOperations({
        "uri":self.imageUri,
        "operations":[
          {
              "action":'crop',
              "x":30,
              "y":50,
              "width":50,
              "height":50
},
          {
              "action":'scale',
              "scaleX":2,
              "scaleY":2
},
          {
              "action":'rotate',
              "degree":180
}
        ],
        "quality":50,
        "format":'WEBP',
        "success"(data){
          self.imageOperateInfo = data;
        },
        "fail"(erromsg,errocode){
          console.info(`### image.applyOperations ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        },
        "complete"(){
          prompt.showToast({
            "message":`complete`
});
        }
});
    },
    "changeValueX"(e){
      this.setData({aspectRatioX:e.value});
    },
    "changeValueY"(e){
      this.setData({aspectRatioY:e.value});
    },
    "editImage"(){
      const self = this;
      image.editImage({
        "uri":self.imageUri,
        "aspectRatioX":self.aspectRatioX,
        "aspectRatioY":self.aspectRatioY,
        "success"(data){
          self.imageEditInfo = data;
        },
        "fail"(erromsg,errocode){
          console.info(`### image.edit ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        },
        "complete"(){
          prompt.showToast({
            "message":`complete`
});
        }
});
    }
});

