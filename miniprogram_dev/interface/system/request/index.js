import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_request} from '../../../quickapp2weixin/index';const request = system_request;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'request',
        "fileUploadData":'',
        "token":'',
        "fileDownloadData":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Requst'
});
    },
    "downloadFile"(){
      const self = this;
      request.download({
        "url":'https://www.quickapp.cn/assets/images/home/logo.png',
        "description":'一个网络图片',
        "filename":'quickappLogo.png',
        "success"(ret){
          self.token = ret.token;
          console.info(`### request.download ### ${JSON.stringify(ret)}`);
        }
});
    },
    "downloadFileComplete"(){
      const self = this;
      request.onDownloadComplete({
        "token":self.token,
        "success"(ret){
          self.fileDownloadData = ret.uri;
          self.fileUploadData = ret.uri;
        },
        "fail"(msg,code){
          console.info(`### request.onDownloadComplete ### ${code}: ${msg}`);
          prompt.showToast({
            "message":`${code}: ${msg}`
});
        }
});
    },
    "uploadFile"(){
      const self = this;
      request.upload({
        "url":'https://www.quickapp.cn/',
        "files":[
          {
              "uri":self.fileUploadData,
              "name":'component_test.pdf'
}
        ],
        "data":[
          {
              "name":'param1',
              "value":'value1'
}
        ],
        "success"(ret){
          self.fileUploadData = ret.data;
        },
        "fail"(msg,code){
          self.fileUploadData = `${code}: ${msg}`;
          console.info(`### request.upload ### ${code}: ${msg}`);
          prompt.showToast({
            "message":`${code}: ${msg}`
});
        }
});
    }
});

