import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_calendar} from '../../../quickapp2weixin/index';const calendar = system_calendar;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
onekit_updateData(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.updatedata0);this.updateData.apply(this,args);},
    "private":{
        "componentName":'calendar',
        "title":'',
        "description":'',
        "startDate":'',
        "endDate":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Calendar'
});
    },
    "updateData"(name,e){
      this.setData({name:e.text});
    },
    "insertEvent"(){
      const self = this;
      if(((self.title) && (self.startDate)) && (self.endDate))
      {
        calendar.insert({
            "title":self.title,
            "description":self.description,
            "startDate":self.startDate,
            "endDate":self.endDate,
            "success"(ret){
              prompt.showToast({
                "message":'插入成功，id:' + ret
});
            },
            "fail"(erromsg,errocode){
              console.info(`### calendar.insert ### ${errocode}: ${erromsg}`);
              prompt.showToast({
                "message":`${errocode}: ${erromsg}`
});
            }
});
      }
else
      {
        prompt.showToast({
            "message":'标题，开始时间，结束事件为必填项！'
});
      };
    }
});

