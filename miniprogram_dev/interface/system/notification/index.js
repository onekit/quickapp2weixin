import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_notification} from '../../../quickapp2weixin/index';const notification = system_notification;
Page({
    "private":{
        "componentName":'notification',
        "inputTitle":'title',
        "inputContent":'content'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Notification'
});
    },
    "show"(){
      const self = this;
      prompt.showToast({
        "message":(('标题为: "' + self.inputTitle)) + '" 的消息已发送，请查看消息通知栏'
});
      notification.show({
        "contentTitle":self.inputTitle,
        "contentText":self.inputContent,
        "clickAction":{
            "uri":'/Home'
}
});
    },
    "titleFn"(e){
      this.setData({inputTitle:e.text});
    },
    "contentFn"(e){
      this.setData({inputContent:e.text});
    }
});

