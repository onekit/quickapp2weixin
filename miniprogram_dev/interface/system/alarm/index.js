import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_alarm} from '../../../quickapp2weixin/index';const alarm = system_alarm;
import {system_media} from '../../../quickapp2weixin/index';const media = system_media;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'Alarm',
        "hour":0,
        "minute":0,
        "message":'快应用闹钟',
        "vibrate":true,
        "repeat":false,
        "days":[
        ],
        "provider":'',
        "ringtone":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'闹钟设置'
});
      this.setData({provider:alarm.getProvider()});
    },
    "setAlarm"(){
      const day = [
      new Date().getDay() - 1
    ];
      this.setData({days:this.data.repeat?[
      0,
      1,
      2,
      3,
      4,
      5,
      6
    ]:day});
      alarm.setAlarm({
        "hour":this.data.hour,
        "minute":this.data.minute,
        "message":this.data.message,
        "vibrate":this.data.vibrate,
        "ringtone":this.data.ringtone,
        "days":this.data.days,
        "success"(){
          prompt.showToast({
            "message":'设置闹钟成功'
});
        },
        "fail"(data,code){
          console.log('set alarm failed：code' + code.code);
        }
});
    },
    "pickVideo"(){
      const _this = this;
      media.pickFile({
        "success"(data){
          prompt.showToast({
            "message":'data.uri:' + data.uri
});
          _this.ringtone = data.uri;
        },
        "fail"(data,code){
          prompt.showToast({
            "message":'选择铃声失败:' + code
});
        }
});
    },
    "setAlarmHour"(e){
      this.setData({hour:e.progress});
    },
    "setAlarmMinute"(e){
      this.setData({minute:e.progress});
    },
    "setAlarmMsg"(e){
      this.setData({message:e.value});
    },
    "switchAlarmVibrate"(e){
      this.setData({vibrate:e.checked});
    },
    "switchAlarmRepeat"(e){
      this.setData({repeat:e.checked});
    }
});

