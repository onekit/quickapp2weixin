import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_sensor} from '../../../quickapp2weixin/index';const sensor = system_sensor;
Page({
    "private":{
        "componentName":'sensor',
        "sensorAccelerometerData":{
            "x":'',
            "y":'',
            "z":''
},
        "stepCount":'',
        "sensorCompassData":0,
        "subscribeProximityData":0,
        "lightData":0
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Sensor'
});
    },
    "accelerometer"(){
      const self = this;
      sensor.subscribeAccelerometer({
        "reserved":false,
        "interval":'normal',
        "callback"(ret){
          self.sensorAccelerometerData = ret;
        }
});
    },
    "unaccelerometer"(){
      sensor.unsubscribeAccelerometer();
    },
    "compass"(){
      const self = this;
      sensor.subscribeCompass({
        "callback"(e){
          self.sensorCompassData = e.direction;
        }
});
    },
    "uncompass"(){
      sensor.unsubscribeCompass();
    },
    "proximity"(){
      const self = this;
      sensor.subscribeProximity({
        "callback"(e){
          self.subscribeProximityData = e.distance;
        }
});
    },
    "unproximity"(){
      sensor.unsubscribeProximity();
    },
    "light"(){
      const self = this;
      sensor.subscribeLight({
        "callback"(e){
          self.lightData = e.intensity;
        }
});
    },
    "unlight"(){
      sensor.unsubscribeLight();
    },
    "listenStep"(){
      sensor.subscribeStepCounter({
        "callback":(e)=>{this.setData({stepCount:e.steps})}
});
    },
    "cancelListenStep"(){
      sensor.unsubscribeStepCounter();
    }
});

