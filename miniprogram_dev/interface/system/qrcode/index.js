import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_barcode} from '../../../quickapp2weixin/index';const barcode = system_barcode;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'qrcode',
        "qrCodeData":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Qrcode'
});
    },
    "qrCode"(){
      const self = this;
      barcode.scan({
        "success"(ret){
          self.qrCodeData = ret.result;
        },
        "fail"(erromsg,errocode){
          self.qrCodeData = ((errocode + ': ')) + erromsg;
          console.info(`### barcode.scan ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    }
});

