import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import settings from '@android.settings';
Page({
    "private":{
        "componentName":'System Setting',
        "settingName":'ringtone',
        "settingValue":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'System Setting'
});
    },
    "nameChange"(e){
      this.setData({settingName:e.text});
    },
    "getSettingValue"(e){
      const self = this;
      settings.getString({
        "key":self.settingName,
        "success"(ret){
          self.settingValue = ret.value;
        },
        "fail"(msg,code){
          self.settingValue = ((code + ': ')) + msg;
          prompt.showToast({
            "message":`${code}: ${msg}`
});
        }
});
    }
});

