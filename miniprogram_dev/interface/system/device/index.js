import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_device} from '../../../quickapp2weixin/index';const device = system_device;
Page({
    "private":{
        "componentName":'Device',
        "deviceInfo":'',
        "deviceIdList":'',
        "deviceId":'',
        "userId":'',
        "serial":'',
        "advertisingId":'',
        "totalStorage":'',
        "availableStorage":'',
        "cpuInfo":'',
        "platformInfo":'',
        "oaId":'',
        "allowTrackOAID":'',
        "windowWidth":0,
        "windowHeight":0,
        "statusBarHeight":0
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'设备信息'
});
    },
    "onShow"(){
      device.getInfo({
        "success":(ret)=>{
          this.setData({windowWidth:ret.windowWidth});
          this.setData({windowHeight:ret.windowHeight});
          this.setData({statusBarHeight:ret.statusBarHeight});
        },
        "fail":(data,code)=>{console.log(`getinfo handling fail, code = ${code}`)}
});
    },
    "getDeviceInfo"(){
      const self = this;
      device.getInfo({
        "success"(data){
          self.deviceInfo = JSON.stringify(data);
        }
});
    },
    "getDeviceIdList"(){
      const self = this;
      device.getId({
        "type":[
          'device',
          'mac',
          'user',
          'advertising',
          'oaid'
        ],
        "success"(data){
          self.deviceIdList = JSON.stringify(data);
        },
        "fail"(data,code){
          self.deviceIdList = 'handling fail, code=' + code;
        }
});
    },
    "getDeviceId"(){
      const self = this;
      device.getDeviceId({
        "success"(data){
          self.deviceId = JSON.stringify(data);
        },
        "fail"(data,code){
          self.deviceId = 'handling fail, code=' + code;
        }
});
    },
    "getUserId"(){
      const self = this;
      device.getUserId({
        "success"(data){
          self.userId = JSON.stringify(data);
        },
        "fail"(data,code){
          self.userId = 'handling fail, code=' + code;
        }
});
    },
    "getSerial"(){
      const self = this;
      device.getSerial({
        "success"(data){
          self.serial = JSON.stringify(data);
        },
        "fail"(data,code){
          self.serial = 'handling fail, code=' + code;
        }
});
    },
    "getAdvertisingId"(){
      const self = this;
      device.getAdvertisingId({
        "success"(data){
          self.advertisingId = JSON.stringify(data);
        },
        "fail"(data,code){
          self.advertisingId = 'handling fail, code=' + code;
        }
});
    },
    "getTotalStorage"(){
      const self = this;
      device.getTotalStorage({
        "success"(data){
          self.totalStorage = JSON.stringify(data);
        },
        "fail"(data,code){
          self.totalStorage = 'handling fail, code=' + code;
        }
});
    },
    "getAvailableStorage"(){
      const self = this;
      device.getAvailableStorage({
        "success"(data){
          self.availableStorage = JSON.stringify(data);
        },
        "fail"(data,code){
          self.availableStorage = 'handling fail, code=' + code;
        }
});
    },
    "getCpuInfo"(){
      const self = this;
      device.getCpuInfo({
        "success"(data){
          self.cpuInfo = JSON.stringify(data);
        },
        "fail"(data,code){
          self.cpuInfo = 'handling fail, code=' + code;
        }
});
    },
    "getPlatformInfo"(){
      const self = this;
      var data = device.platform;
      self.platformInfo = JSON.stringify(data);
    },
    "getOAID"(){
      const self = this;
      device.getOAID({
        "success"(data){
          self.oaId = JSON.stringify(data);
        },
        "fail"(data,code){
          self.oaId = 'handling fail, code=' + code;
        }
});
    },
    "getAllowTrackOAID"(){
      const self = this;
      self.allowTrackOAID = device.allowTrackOAID;
    }
});

