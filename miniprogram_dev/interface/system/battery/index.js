import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_battery} from '../../../quickapp2weixin/index';const battery = system_battery;
Page({
    "private":{
        "componentName":'Battery',
        "info":'',
        "charging":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'电量信息'
});
    },
    "getInfo"(){
      const self = this;
      battery.getStatus({
        "success"(data){
          self.info = data.level;
          self.charging = data.charging;
        },
        "fail"(error){
          self.info = error;
        }
});
    }
});

