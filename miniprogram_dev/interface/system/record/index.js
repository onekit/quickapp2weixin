import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_record} from '../../../quickapp2weixin/index';const record = system_record;
import {system_audio} from '../../../quickapp2weixin/index';const audio = system_audio;
import {system_storage} from '../../../quickapp2weixin/index';const storage = system_storage;
Page({
onekit_changeValue(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.changevalue0);this.changeValue.apply(this,args);},
    "private":{
        "componentName":'Record',
        "path":'',
        "duration":1000,
        "sampleRate":8000,
        "numberOfChannels":1,
        "encodeBitRate":16000,
        "format":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "start"(){
      this.setData({path:'录音中...'});
      record.start({
        "duration":this.data.duration,
        "sampleRate":this.data.sampleRate,
        "numberOfChannels":this.data.numberOfChannels,
        "encodeBitRate":this.data.encodeBitRate,
        "format":this.data.format,
        "success":(data)=>{this.setData({path:data.uri})},
        "fail":(err,code)=>{this.setData({path:'handling fail, code=' + code})}
});
    },
    "stop"(){
      record.stop();
    },
    "playAudio"(){
      audio.src = this.data.path;
      audio.play();
    },
    "changeValue"(args,evt){
      this.setData({args:evt.value});
      storage.set({
        "key":args,
        "value":evt.value
});
    }
});

