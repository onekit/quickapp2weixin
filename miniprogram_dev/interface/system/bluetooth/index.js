import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_bluetooth} from '../../../quickapp2weixin/index';const bluetooth = system_bluetooth;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
onekit_createBLEConnection(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.createbleconnection0);this.createBLEConnection.apply(this,args);},
    "private":{
        "componentName":'Bluetooth',
        "devices":[
        ],
        "deviceLen":0,
        "deviceId":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'蓝牙'
});
    },
    "openAdapter"(){
      let _this = this;
      bluetooth.openAdapter({
        "operateAdapter":true,
        "success"(res){
          prompt.showToast({
            "message":'蓝牙初始化成功'
});
          setTimeout(()=>{_this.startDevicesDiscovery()},2000);
        },
        "fail"(data,code){
          prompt.showToast({
            "message":'蓝牙初始化失败'
});
        }
});
    },
    "startDevicesDiscovery"(){
      let _this = this;
      bluetooth.startDevicesDiscovery({
        "allowDuplicatesKey":true,
        "success"(res){
          prompt.showToast({
            "message":'开始搜寻附近的蓝牙外围设备'
});
          _this.ondevicefound();
        },
        "fail"(data,code){
          prompt.showToast({
            "message":`蓝牙搜寻失败,code=${code}`
});
        }
});
    },
    "stopDevicesDiscovery"(){
      bluetooth.stopDevicesDiscovery({
        "success"(){
          prompt.showToast({
            "message":'停止扫描'
});
        }
});
    },
    "ondevicefound"(){
      let _this = this;
      bluetooth.ondevicefound = (res)=>{res.devices.forEach((device)=>{
      if((!device.name) && (!device.localName))
        {
          return;
        };
      const foundDevices = _this.devices;
      const idx = _this.inArray(foundDevices,'deviceId',device.deviceId);
      if(idx == -1)
        {
          _this.devices.push(device);
        }
else
        {
          _this.devices.splice(idx,1,device);
        };
      _this.deviceLen = _this.devices.length;
    })};
    },
    "writeBLECharacteristicValue"(){
      const _this = this;
      const buffer = new ArrayBuffer(1);
      const dataView = new DataView(buffer);
      dataView.setUint8(0,((Math.random() * 255)) | 0);
      bluetooth.writeBLECharacteristicValue({
        "deviceId":_this._deviceId,
        "serviceId":_this._serviceId,
        "characteristicId":_this._characteristicId,
        "value":buffer,
        "success"(){
          prompt.showToast({
            "message":'写入成功'
});
        }
});
    },
    "closeAdapter"(){
      bluetooth.closeAdapter();
    },
    "createBLEConnection"(data){
      const _this = this;
      const deviceId = data.deviceId;
      const name = data.name;
      bluetooth.createBLEConnection({
        deviceId,
        "success"(res){
          _this.connected = true;
          prompt.showToast({
            "message":'连接成功'
});
          _this.deviceId = deviceId;
          _this.getBLEDeviceServices(deviceId);
        }
});
    },
    "getBLEDeviceServices"(deviceId){
      const _this = this;
      bluetooth.getBLEDeviceServices({
        deviceId,
        "success"(res){
          for(let i = 0;i < res.services.length;i++){
          if(res.services[i].isPrimary)
            {
              _this.getBLEDeviceCharacteristics(deviceId,res.services[i].uuid);
            }
        };
        }
});
    },
    "getBLEDeviceCharacteristics"(deviceId,serviceId){
      const _this = this;
      bluetooth.getBLEDeviceCharacteristics({
        deviceId,
        serviceId,
        "success"(res){
          for(let i = 0;i < res.characteristics.length;i++){
          let item = res.characteristics[i];
          if(item.properties.read)
            {
              bluetooth.readBLECharacteristicValue({
                  deviceId,
                  serviceId,
                  "characteristicId":item.uuid,
                  "success"(){
                    prompt.showToast({
                      "message":'读取成功'
});
                  }
});
            }
          if(item.properties.write)
            {
              _this._deviceId = deviceId;
              _this._serviceId = serviceId;
              _this._characteristicId = item.uuid;
              _this.writeBLECharacteristicValue();
            }
          if((item.properties.notify) || (item.properties.indicate))
            {
              bluetooth.notifyBLECharacteristicValueChange({
                  deviceId,
                  serviceId,
                  "characteristicId":item.uuid,
                  "state":true,
                  "success"(){
                    prompt.showToast({
                      "message":'订阅成功'
});
                  }
});
            }
        };
        }
});
    },
    "closeBLEConnection"(){
      const _this = this;
      bluetooth.closeBLEConnection({
        "deviceId":_this.deviceId,
        "success"(){
          prompt.showToast({
            "message":'已断开连接'
});
        },
        "fail"(err){
          prompt.showToast({
            "message":err
});
        }
});
    },
    "inArray"(arr,key,val){
      for(let i = 0;i < arr.length;i++){
      if(arr[i][key] == val)
        {
          return i;
        }
    };
      return -1;
    }
});

