import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_brightness} from '../../../quickapp2weixin/index';const brightness = system_brightness;
Page({
onekit_setMode(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.setmode0);this.setMode.apply(this,args);},
    "private":{
        "componentName":'Brightness',
        "value":0,
        "sliderValue":0,
        "status":'',
        "mode":'',
        "setModStatus":'',
        "keepScreenStatus":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'屏幕亮度'
});
    },
    "getValue"(){
      const self = this;
      brightness.getValue({
        "success"(ret){
          self.value = ret.value;
        },
        "fail"(error){
          self.value = error;
        }
});
    },
    "getSliderValue"(e){
      this.setData({sliderValue:e.progress});
    },
    "setValue"(){
      const self = this;
      brightness.setValue({
        "value":self.sliderValue,
        "success"(){
          self.status = '亮度设置成功';
        },
        "fail"(err){
          self.status = '设置失败： ' + err;
        }
});
    },
    "getMode"(){
      const self = this;
      brightness.getMode({
        "success"(ret){
          self.mode = ret.mode;
        }
});
    },
    "setMode"(index){
      const self = this;
      brightness.setMode({
        "mode":index,
        "success"(){
          self.setModStatus = 'mode=' + index;
        },
        "fail"(err,code){
          self.setModStatus = 'handling fail, code=' + code;
        }
});
    },
    "setKeepScreenOn"(){
      let self = this;
      brightness.setKeepScreenOn({
        "keepScreenOn":true,
        "success"(){
          self.keepScreenStatus = '常亮状态设置成功';
          console.log(`handling success`);
        },
        "fail"(data,code){
          console.log(`handling fail, data=${data},code=${code}`);
        }
});
    }
});

