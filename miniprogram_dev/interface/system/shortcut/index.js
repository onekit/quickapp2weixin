import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_shortcut} from '../../../quickapp2weixin/index';const shortcut = system_shortcut;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'shortcut'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Shortcut'
});
    },
    "getShortcut"(){
      shortcut.hasInstalled({
        "success"(ret){
          if(ret)
          {
            prompt.showToast({
                "message":'桌面图标已创建'
});
          }
else
          {
            prompt.showToast({
                "message":'桌面图标未创建'
});
          };
        }
});
    },
    "installShortcut"(){
      shortcut.install({
        "success"(){
          prompt.showToast({
            "message":'桌面图标创建成功'
});
        }
});
    },
    "installShortcutWithCustomMessage"(){
      shortcut.install({
        "message":'自定义提示信息',
        "success"(){
          prompt.showToast({
            "message":'桌面图标创建成功'
});
        }
});
    },
    "openShortcut"(){
      shortcut.systemPromptEnabled = true;
      prompt.showToast({
        "message":'已开启弹窗'
});
    },
    "closeShortcut"(){
      shortcut.systemPromptEnabled = false;
      prompt.showToast({
        "message":'已关闭弹窗'
});
    },
    "getShortcutStatus"(){
      const status = shortcut.systemPromptEnabled;
      prompt.showToast({
        "message":(status == true)?'已开启':'已关闭'
});
    }
});

