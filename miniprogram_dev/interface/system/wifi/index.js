import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_wifi} from '../../../quickapp2weixin/index';const wifi = system_wifi;
Page({
    "private":{
        "componentName":'Wi-Fi',
        "wifiState":'',
        "wifiList":'',
        "wifiConnected":'',
        "wifiConnectRes":'请根据实际情况,配置参数进行连接',
        "wifiScanRes":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Wi-Fi'
});
    },
    "onReady"(){
      wifi.onstatechanged = (data)=>{if(data.state == 0)
      {
        this.setData({wifiState:'连接断开' + JSON.stringify(data)});
      }
else
      {
        this.setData({wifiState:'连接成功' + JSON.stringify(data)});
      }};
      wifi.onscanned = (data)=>{this.setData({wifiList:JSON.stringify(data)})};
    },
    "getConnectedWifiMsg"(){
      wifi.getConnectedWifi({
        "success":(data)=>{this.setData({wifiConnected:JSON.stringify(data)})},
        "fail":(data,code)=>{this.setData({wifiConnected:`handling fail, code = ${code}`})}
});
    },
    "connectWifi"(){
      wifi.connect({
        "SSID":'test',
        "BSSID":'xxx',
        "password":'xxx',
        "success":()=>{this.setData({wifiConnectRes:'connect success'})},
        "fail":(data,code)=>{this.setData({wifiConnectRes:`handling fail, code = ${code}`})}
});
    },
    "scanWifi"(){
      wifi.scan({
        "success":()=>{this.setData({wifiScanRes:'scan success'})},
        "fail":(data,code)=>{this.setData({wifiScanRes:`handling fail, code = ${code}`})}
});
    }
});

