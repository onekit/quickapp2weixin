import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_websocketfactory} from '../../../quickapp2weixin/index';const websocketfactory = system_websocketfactory;
let ws = {
    "websock1":null,
    "websock2":null
};
Page({
onekit_create(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.create0);args.push(dataset.create1);this.create.apply(this,args);},
onekit_send(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.send0);this.send.apply(this,args);},
onekit_close(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.close0);this.close.apply(this,args);},
    "data":{
        "componentName":'websocket',
        "message":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "create"(name,isbuffer){
      if(ws[name])
      {
        prompt.showToast({
            "message":`[client ${name}] has been existed`
});
        return false;
      };
      prompt.showToast({
        "message":`[client ${name}] is creating, wait please.`
});
      let _ws = websocketfactory.create({
        "url":(isbuffer == true)?'ws://demos.kaazing.com/echo':'wss://echo.websocket.org',
        "header":{
            "content-type":'application/json'
},
        "protocols":[
          'protocol'
        ]
});
      if(isbuffer == true)
      {
        _ws.binaryType = 'arraybuffer';
      };
      _ws.onopen = function(){
      prompt.showToast({
        "message":`[client ${name}] connect open`
});
    };
      _ws.onmessage = function(data){
      let res = data.data;
      let type = Object.prototype.toString.call(res);
      function ab2str(buf){
      return String.fromCharCode.apply(null,new Uint16Array(buf));
    };
      if(Object.prototype.toString.call(res) == '[object ArrayBuffer]')
      {
        res = ab2str(res);
      };
      prompt.showToast({
        "message":`[client ${name}] has received,type: ${type}; message: ${res} from server`
});
    };
      _ws.onerror = function(data){
      prompt.showToast({
        "message":`onerror [client ${name}] data.data = ${data.data}`
});
    };
      _ws.onclose = function(data){
      prompt.showToast({
        "message":`onclose [client ${name}] data.code = ${data.code}, data.reason = ${data.reason}, data.wasClean = ${data.wasClean}`
});
    };
      ws[name] = _ws;
    },
    "handleChange"(e){
      this.setData({message:e.value});
    },
    "send"(name){
      let _ws = ws[name];
      if(!_ws)
      {
        return false;
      };
      function str2ab(str){
      var buf = new ArrayBuffer(str.length * 2);
      var bufView = new Uint16Array(buf);
      for(var i = 0,strLen = str.length;i < strLen;i++){
        bufView[i] = str.charCodeAt(i);
      };
      return buf;
    };
      let msg = (_ws.binaryType !== 'arraybuffer')?this.data.message:str2ab(this.data.message);
      _ws.send({
        "data":msg,
        "success"(){
          prompt.showToast({
            "message":`[client ${name}] send success`
});
        },
        "fail"(data,code){
          prompt.showToast({
            "message":`[client ${name}] handling fail, code = ${code}`
});
        }
});
    },
    "close"(name){
      if(!ws[name])
      {
        return false;
      };
      ws[name].close({
        "success"(){
          prompt.showToast({
            "message":`[client ${name}] close success`
});
          ws[name] = null;
        },
        "fail"(data,code){
          prompt.showToast({
            "message":`[client ${name}] handling fail, code = ${code}`
});
        }
});
    }
});

