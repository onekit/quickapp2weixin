import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_storage} from '../../../quickapp2weixin/index';const storage = system_storage;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import catching from 'catching';
Page({
    "private":{
        "componentName":'storage',
        "storageKey":'',
        "storageValue":'',
        "inputKey":'',
        "inputValue":'',
        "storageIndex":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Storage'
});
    },
    "keyFn"(e){
      this.setData({storageKey:e.text});
    },
    "valueFn"(e){
      this.setData({storageValue:e.text});
    },
    "indexFn"(e){
      this.setData({storageIndex:e.text});
    },
    async "setStorage"(){
      const self = this;
      if((this.data.storageKey) && (this.data.storageValue))
      {
        await storage.set({
            "key":this.data.storageKey,
            "value":this.data.storageValue
});
        prompt.showToast({
            "message":(((((('{key:' + self.storageKey)) + ',value:')) + self.storageValue)) + '}'
});
      }
else
      {
        prompt.showToast({
            "message":'请输入key和value值'
});
      };
    },
    async "getStorageUseAsync"(){
      const ret = await catching(storage.get({
        "key":this.data.storageKey
}));
      prompt.showToast({
        "message":'value: ' + ret.data
});
    },
    async "getStorageUseAsyncTryCatch"(){
      try{
      const ret = await storage.get({
          "key":this.data.storageKey
});
      prompt.showToast({
          "message":'value: ' + ret.data
});
    }
    catch(err){
      prompt.showToast({
          "message":'err: ' + JSON.stringify(err)
});
    };
    },
    "getStorageNormal"(){
      storage.get({
        "key":this.data.storageKey,
        "success"(ret){
          prompt.showToast({
            "message":'value: ' + ret
});
        }
});
    },
    async "getStorageUseIndex"(){
      const ret = await catching(storage.key({
        "index":this.data.storageIndex
}));
      prompt.showToast({
        "message":'value: ' + ret.data
});
    },
    "clearStorage"(){
      const self = this;
      storage.clear({
        "success"(){
          self.storageKey = '';
          self.storageValue = '';
          self.inputKey = '';
          self.inputValue = '';
          prompt.showToast({
            "message":'success'
});
        }
});
    }
});

