import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_contact} from '../../../quickapp2weixin/index';const contact = system_contact;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'联系人',
        "contactName":'',
        "contactNumber":'',
        "contactList":[
        ]
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'联系人'
});
    },
    "pickContact"(){
      const self = this;
      contact.pick({
        "success"(data){
          self.contactName = data.displayName;
          self.contactNumber = data.number;
        },
        "fail"(data,code){
          console.info(`### handling fail ### code:${code}`);
        }
});
    },
    "getContactList"(){
      contact.list({
        "success":(data)=>{this.setData({contactList:data.contactList})},
        "fail":(err,code)=>{prompt.showToast({
            "message":`错误原因：${err} 错误码：${code}`
})}
});
    }
});

