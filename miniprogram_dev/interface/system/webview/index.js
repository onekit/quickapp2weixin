import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_webview} from '../../../quickapp2weixin/index';const webview = system_webview;
Page({
    "private":{
        "componentName":'webview'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Webview'
});
    },
    "loadUrl"(){
      webview.loadUrl({
        "url":'https://www.quickapp.cn/'
});
    }
});

