import {Page,Component} from '../../../quickapp2weixin/index.js';
import {service_exchange} from '../../../quickapp2weixin/index';const exchange = service_exchange;
Page({
    "private":{
        "componentName":'应用数据共享'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'应用数据共享'
});
    },
    "onShow"(){
    },
    "exchangeGet"(){
      exchange.get({
        "package":"com.example",
        "sign":"7a12ec1d66233f20a20141035b1f7937",
        "key":"token",
        "success"(ret){
          console.log(`handling success, value = ${ret.value}`);
        },
        "fail"(data,code){
          console.log(`handling fail, code = ${code}`);
        }
});
    },
    "exchangeSet"(){
      exchange.set({
        "key":"token",
        "value":"12347979",
        "enforcePackage":false,
        "success"(){
          console.log(`handling success`);
        },
        "fail"(data,code){
          console.log(`handling fail, code = ${code}`);
        }
});
    },
    "exchangeRemove"(){
      exchange.remove({
        "key":"token",
        "success"(){
          console.log(`handling success`);
        },
        "fail"(data,code){
          console.log(`handling fail, code = ${code}`);
        }
});
    },
    "exchangeClear"(){
      exchange.clear({
        "success"(){
          console.log(`handling success`);
        },
        "fail"(data,code){
          console.log(`handling fail, code = ${code}`);
        }
});
    },
    "exchangeGrantPermission"(){
      exchange.grantPermission({
        "sign":"7a12ec1d66233f20a20141035b1f7937",
        "pkg":"com.example",
        "success"(){
          console.log(`handling success`);
        },
        "fail"(data,code){
          console.log(`handling fail, code = ${code}`);
        }
});
    },
    "exchangeRevokePermission"(){
      exchange.revokePermission({
        "pkg":"com.example",
        "success"(){
          console.log(`handling success`);
        },
        "fail"(data,code){
          console.log(`handling fail, code = ${code}`);
        }
});
    }
});

