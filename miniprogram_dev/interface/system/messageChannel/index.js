import {Page,Component} from '../../../quickapp2weixin/index.js';
import MessageChannel from '@hap.io.MessageChannel';
Page({
    "private":{
        "componentName":'原生应用通信',
        "package":'com.miui.hybrid.channel.demo',
        "signature":'fcaa113908d343444c1894dc4b42ac13'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'原生应用通信'
});
      this.createChannel();
    },
    "createChannel"(){
      const self = this;
      new MessageChannel({
        "package":self.package,
        "signature":self.signature
});
    },
    "sendChannel"(){
      channel.send({
        "message":{
            "code":0,
            "data":'data from hap app',
            "extra":{
                "customeKey1":'customeValue1',
                "customeKey2":'customeValue2'
}
},
        "success"(){
          console.log('send success');
        },
        "fail"(data,code){
          console.log(`handling fail, code = ${code}`);
        }
});
    },
    "closeChannel"(){
      channel.close({
        "reason":'hap app close',
        "success"(){
          console.log('close success');
        },
        "fail"(data,code){
          console.log(`handling fail, code = ${code}`);
        }
});
    },
    "openCallback"(){
      channel.onopen = function(){
      console.log('channel open');
    };
    },
    "onmessage"(){
      channel.onmessage = function(data){
      console.log(`onmessage code: ${data.message.code}, data: ${data.message.data}`);
    };
    },
    "onclose"(){
      console.log(`onclose data.code = ${data.code}, data.reason = ${data.reason}`);
    },
    "onerror"(){
      channel.onerror = function(data){
      console.log(`onerror data.code = ${data.code}, data.data = ${data.data}`);
    };
    }
});

