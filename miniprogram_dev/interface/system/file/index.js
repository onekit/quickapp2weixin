import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_file} from '../../../quickapp2weixin/index';const file = system_file;
import {system_fetch} from '../../../quickapp2weixin/index';const fetch = system_fetch;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'文件存储',
        "fileDownloadData":'',
        "fileMoveData":'',
        "fileMoveToFolderData":'',
        "fileCopyData":'',
        "fileGetListData":'',
        "fileGetData":'',
        "fileDeleteData":'',
        "fileAccessData":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'文件存储'
});
    },
    "downloadFile"(){
      const self = this;
      fetch.fetch({
        "url":'https://statres.quickapp.cn/quickapp/quickapp/201803/file/201803200129552999556.apk',
        "success"(ret){
          self.fileDownloadData = ret.data;
          self.fileDeleteData = self.fileDownloadData;
        }
});
    },
    "moveFile"(){
      const self = this;
      const dstUri = 'internal://files/testfile/file.pdf';
      file.move({
        "srcUri":this.data.fileDownloadData,
        dstUri,
        "success"(){
          self.fileMoveData = dstUri;
          self.fileDownloadData = dstUri;
          self.fileDeleteData = self.fileDownloadData;
        },
        "fail"(errmsg,errcode){
          self.fileMoveData = ((errcode + '---')) + errmsg;
          console.info(`### file.move ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    },
    "moveFileToFolder"(){
      const self = this;
      const dstUri = 'internal://files/testfile/';
      file.move({
        "srcUri":this.data.fileDownloadData,
        dstUri,
        "success"(ret){
          self.fileMoveToFolderData = ret;
          self.fileDownloadData = ret;
          self.fileDeleteData = ret;
        },
        "fail"(errmsg,errcode){
          self.fileMoveToFolderData = ((errcode + '---')) + errmsg;
          console.info(`### file.move ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    },
    "copyFile"(){
      const self = this;
      const dstUri = 'internal://files/testfile/filecopy.pdf';
      file.copy({
        "srcUri":self.fileDownloadData,
        dstUri,
        "success"(ret){
          self.fileCopyData = ret;
        },
        "fail"(errmsg,errcode){
          self.fileCopyData = ((errcode + '---')) + errmsg;
          console.info(`### file.copy ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    },
    "getFileList"(){
      const self = this;
      file.list({
        "uri":'internal://files/testfile',
        "success"(ret){
          self.fileGetListData = JSON.stringify(ret);
        },
        "fail"(errmsg,errcode){
          self.fileGetListData = ((errcode + '---')) + errmsg;
          console.info(`### file.list ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    },
    "getFile"(){
      const self = this;
      file.get({
        "uri":self.fileDownloadData,
        "success"(ret){
          self.fileGetData = JSON.stringify(ret);
        },
        "fail"(errmsg,errcode){
          self.fileGetData = ((errcode + '---')) + errmsg;
          console.info(`### file.get ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    },
    "deleteFile"(){
      const self = this;
      file.delete({
        "uri":self.fileDownloadData,
        "success"(ret){
          self.fileDownloadData = '';
          self.fileMoveData = '';
          self.fileMoveToFolderData = '';
          self.fileCopyData = '';
          self.fileGetListData = '';
          self.fileGetData = '';
          self.fileDeleteData = JSON.stringify(ret);
        },
        "fail"(errmsg,errcode){
          self.fileDeleteData = ((errcode + '---')) + errmsg;
          console.info(`### file.delete ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    },
    "accessFile"(){
      const self = this;
      file.access({
        "uri":self.fileDownloadData,
        "success"(ret){
          self.fileDownloadData = '';
          self.fileMoveData = '';
          self.fileMoveToFolderData = '';
          self.fileCopyData = '';
          self.fileGetListData = '';
          self.fileGetData = '';
          self.fileDeleteData = '';
          self.fileAccessData = JSON.stringify(ret);
        },
        "fail"(errmsg,errcode){
          self.fileAccessData = ((errcode + '---')) + errmsg;
          console.info(`### file.access ### ${errcode}: ${errmsg}`);
          prompt.showToast({
            "message":`${errcode}: ${errmsg}`
});
        }
});
    }
});

