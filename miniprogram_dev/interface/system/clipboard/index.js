import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_clipboard} from '../../../quickapp2weixin/index';const clipboard = system_clipboard;
Page({
    "private":{
        "componentName":'clipboard',
        "show":true,
        "clipboard":'新应用测试版1.0',
        "showSet":'',
        "showGet":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Clipboard'
});
    },
    "set"(){
      this.setData({showSet:this.data.clipboard});
      clipboard.set({
        "text":this.data.clipboard
});
    },
    "get"(){
      const self = this;
      clipboard.get({
        "success"(obj){
          self.showGet = obj.text;
        }
});
    }
});

