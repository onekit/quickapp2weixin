import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_vibrator} from '../../../quickapp2weixin/index';const vibrator = system_vibrator;
Page({
    "private":{
        "componentName":'vibrator',
        "vibratorInfo":'触发震动，支持长时和短时两种'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Vibrator'
});
    },
    "vibratorLong"(){
      vibrator.vibrate({
        "mode":'long'
});
    },
    "vibratorShort"(){
      vibrator.vibrate({
        "mode":'short'
});
    }
});

