import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_volume} from '../../../quickapp2weixin/index';const volume = system_volume;
Page({
    "private":{
        "componentName":'Volume',
        "mediaValue":0,
        "setMediaValue":0,
        "sliderValue":0,
        "status":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'系统音量'
});
    },
    "getMediaValueFn"(){
      const self = this;
      volume.getMediaValue({
        "success"(data){
          self.mediaValue = data.value.toFixed(1);
          self.sliderValue = self.mediaValue;
        }
});
    },
    "sliderValueFn"(e){
      this.setData({sliderValue:e.progress / 10});
    },
    "setMediaValueFn"(){
      const self = this;
      volume.setMediaValue({
        "value":self.sliderValue,
        "success"(){
          self.status = '音量设置成功，volume = ' + self.sliderValue;
        },
        "fail"(data,code){
          self.status = 'handling fail, code=' + code;
        }
});
    }
});

