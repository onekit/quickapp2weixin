import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_geolocation} from '../../../quickapp2weixin/index';const geolocation = system_geolocation;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'geolocation',
        "geolocationGetData":{
            "latitude":'',
            "longitude":'',
            "accuracy":'',
            "time":''
},
        "geolocationListenData":{
            "latitude":'',
            "longitude":'',
            "accuracy":'',
            "time":''
},
        "locationType":'',
        "coordTypes":'',
        "objectFitValues":[
          "gcj02",
          "wgs84"
        ],
        "coordTypeValue":'gcj02'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Geolocation'
});
    },
    "changeOption"(e){
      this.setData({coordTypeValue:e.newValue});
      prompt.showToast({
        "message":`坐标系类型切换为 ${e.newValue}`
});
    },
    "getCoordTypes"(){
      let self = this;
      let SupportedCoordTypesArr = geolocation.getSupportedCoordTypes();
      self.coordTypes = SupportedCoordTypesArr.join();
    },
    "getGeolocation"(){
      const self = this;
      geolocation.getLocation({
        "coordType":self.coordTypeValue,
        "success"(ret){
          self.geolocationGetData = ret;
        },
        "fail"(erromsg,errocode){
          console.info(`### geolocation.getLocation ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "listenGeolocation"(){
      const self = this;
      geolocation.subscribe({
        "coordType":self.coordTypeValue,
        "success"(ret){
          self.geolocationListenData = ret;
        },
        "fail"(erromsg,errocode){
          console.info(`### geolocation.subscribe ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "cancelGeolocation"(){
      geolocation.unsubscribe();
    },
    "getGeolocationType"(){
      let self = this;
      geolocation.getLocationType({
        "success"(data){
          self.locationType = data.types;
          console.log(`### handling success ### locationType: ${data.types}`);
        },
        "fail"(data,code){
          self.locationType = `获取失败，code: ${code}`;
          console.log(`### handling fail ### code: ${code}`);
        }
});
    }
});

