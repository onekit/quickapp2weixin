import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_zip} from '../../../quickapp2weixin/index';const zip = system_zip;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'解压文件',
        "srcUri":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'解压文件'
});
    },
    "decompress"(){
      const self = this;
      zip.decompress({
        "srcUri":self.srcUri,
        "dstUri":'internal://files/unzip/',
        "success"(){
          prompt.showToast({
            "message":`handling success`
});
        },
        "fail"(data,code){
          console.log(`handling fail`);
          prompt.showToast({
            "message":`handling fail, ${code}: ${data}`
});
        }
});
    }
});

