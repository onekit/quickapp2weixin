import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_network} from '../../../quickapp2weixin/index';const network = system_network;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'network',
        "networkType":'',
        "currentType":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Network'
});
    },
    "getNetworkType"(){
      const self = this;
      network.getType({
        "success"(ret){
          self.networkType = JSON.stringify(ret);
        },
        "fail"(erromsg,errocode){
          console.info(`### network.getType ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "listenNetwork"(){
      const self = this;
      network.subscribe({
        "callback"(ret){
          self.currentType = JSON.stringify(ret);
        },
        "fail"(erromsg,errocode){
          console.info(`### network.subscribe ### ${errocode}: ${erromsg}`);
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "cancelNetwork"(){
      network.unsubscribe();
    }
});

