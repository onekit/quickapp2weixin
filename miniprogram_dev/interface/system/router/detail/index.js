import {Page,Component} from '../../../../quickapp2weixin/index.js';
import {system_router} from '../../../../quickapp2weixin/index';const router = system_router;
Page({
onekit_routeBack(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.routeback0);this.routeBack.apply(this,args);},
    "public":{
        "pageInfo":'',
        "pageInfoObject":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Detail'
});
      (this.data.pageInfo) && (console.info('pageInfo: ' + this.data.pageInfo));
      (this.data.pageInfoObject) && (console.info('pageInfoObject.msg: ' + JSON.parse(this.data.pageInfoObject).msg));
    },
    "routeBack"(){
      router.back({
        "path":arguments[0]
});
    }
});

