import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_fetch} from '../../../quickapp2weixin/index';const fetch = system_fetch;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'fetch',
        "fetchedData":'init',
        "fetchCookieData":'无'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Fetch'
});
    },
    "fetchDataNormal"(){
      const self = this;
      this.setData({fetchedData:'fetching...'});
      fetch.fetch({
        "url":'http://news.baidu.com/widget',
        "data":{
            "id":'LocalNews',
            "ajax":'json'
},
        "responseType":'json',
        "success"(ret){
          self.fetchedData = '网络数据(jsonp)：' + JSON.stringify(ret.data);
          prompt.showToast({
            "message":'success'
});
        }
});
    },
    async "fetchDataUseAsync"(){
      const self = this;
      this.setData({fetchedData:'fetching...'});
      const ret = await fetch.fetch({
        "url":'http://news.baidu.com/widget',
        "data":{
            "id":'LocalNews',
            "ajax":'json'
},
        "responseType":'json'
});
      self.fetchedData = '网络数据(jsonp)：' + JSON.stringify(ret.data.data);
      prompt.showToast({
        "message":'success'
});
    },
    "fetchWithCookie"(){
      const self = this;
      const host = 'IP:PORT';
      const url = `http://${host}/echocookie`;
      const header = {
        "Cookie":'dummy_cookie=foo; yummy_cookie=bar; tasty_cookie=strawberry'
};
      fetch.fetch({
        url,
        header,
        "method":'GET',
        "success"(resData){
          self.fetchCookieData = '网络数据(json):' + JSON.stringify(resData);
          prompt.showToast({
            "message":'success'
});
        }
});
    }
});

