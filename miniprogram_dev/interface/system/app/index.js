import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_app} from '../../../quickapp2weixin/index';const app = system_app;
Page({
    "private":{
        "componentName":'App',
        "info":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'应用上下文'
});
    },
    "getDeviceInfo"(){
      this.setData({info:app.getInfo()});
    }
});

