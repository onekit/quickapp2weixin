import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_file} from '../../../quickapp2weixin/index';const file = system_file;
import {system_fetch} from '../../../quickapp2weixin/index';const fetch = system_fetch;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'文件读写',
        "inputValue":'',
        "fileContent":'',
        "fileArrayBuffer":'',
        "fileUri":'internal://files/testfile/file.txt'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'文件读写'
});
    },
    "keyFn"(e){
      this.setData({inputValue:e.text});
    },
    "writeFile"(){
      const self = this;
      file.writeText({
        "uri":self.fileUri,
        "text":self.inputValue,
        "success"(){
          prompt.showToast({
            "message":`success`
});
        },
        "fail"(data,code){
          console.info(`### writeFile fail ### ${data}:${code}`);
        }
});
    },
    "readFile"(){
      const self = this;
      file.readText({
        "uri":self.fileUri,
        "success"(data){
          self.fileContent = data.text;
        },
        "fail"(data,code){
          console.info(`### readFile fail ### ${data}:${code}`);
        }
});
    },
    "readFileBuffer"(){
      const self = this;
      file.readArrayBuffer({
        "uri":self.fileUri,
        "success"(data){
          self.fileArrayBuffer = data.buffer;
        },
        "fail"(data,code){
          console.info(`### readFileBuffer fail ### ${data}:${code}`);
        }
});
    },
    "writeFileBuffer"(){
      const self = this;
      file.writeArrayBuffer({
        "uri":self.fileUri,
        "buffer":self.fileArrayBuffer,
        "success"(){
          prompt.showToast({
            "message":`success`
});
        },
        "fail"(data,code){
          console.info(`### writeFileBuffer fail ### ${data}:${code}`);
        }
});
    }
});

