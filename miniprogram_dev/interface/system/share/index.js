import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_share} from '../../../quickapp2weixin/index';const share = system_share;
import {system_media} from '../../../quickapp2weixin/index';const media = system_media;
import {system_file} from '../../../quickapp2weixin/index';const file = system_file;
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'share',
        "shareData":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Share'
});
    },
    "shareContent"(e){
      this.setData({shareData:e.text});
    },
    "shareText"(){
      share.share({
        "type":'text/html',
        "data":this.data.shareData,
        "success"(){
          console.info('share success');
        },
        "fail"(erromsg,errocode){
          prompt.showToast({
            "message":`${errocode}: ${erromsg}`
});
        }
});
    },
    "shareImage"(){
      media.pickImage({
        "success"(ret){
          file.copy({
            "srcUri":ret.uri,
            "dstUri":'internal://files/a.jpg',
            "success"(){
              share.share({
                "type":'image/*',
                "data":'internal://files/a.jpg'
});
            }
});
        }
});
    }
});

