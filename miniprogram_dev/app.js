import {App} from './quickapp2weixin/index.js';
App({})
getApp().onekit.router = {
  "entry": "home",
  "pages": {
    "home": {
      "component": "index"
    },
    "component/basic/a": {
      "component": "index"
    },
    "component/media/web/detail": {
      "component": "index"
    },
    "component/media/web": {
      "component": "index"
    },
    "component/container/div": {
      "component": "index"
    },
    "component/basic/image": {
      "component": "index"
    },
    "component/basic/image-alt": {
      "component": "index"
    },
    "component/form/input": {
      "component": "index"
    },
    "component/form/label": {
      "component": "index"
    },
    "component/container/list": {
      "component": "index"
    },
    "component/form/picker": {
      "component": "index"
    },
    "component/form/multi-picker": {
      "component": "index"
    },
    "component/form/select": {
      "component": "index"
    },
    "component/basic/progress": {
      "component": "index"
    },
    "component/container/popup": {
      "component": "index"
    },
    "component/container/refresh": {
      "component": "index"
    },
    "component/container/richtext": {
      "component": "index"
    },
    "component/form/slider": {
      "component": "index"
    },
    "component/basic/span": {
      "component": "index"
    },
    "component/container/stack": {
      "component": "index"
    },
    "component/container/swiper": {
      "component": "index"
    },
    "component/form/switch": {
      "component": "index"
    },
    "component/container/tabs": {
      "component": "index"
    },
    "component/basic/text": {
      "component": "index"
    },
    "component/basic/rating": {
      "component": "index"
    },
    "component/basic/marquee": {
      "component": "index"
    },
    "component/form/textarea": {
      "component": "index"
    },
    "component/media/video": {
      "component": "index"
    },
    "component/media/canvas": {
      "component": "index"
    },
    "component/media/audio": {
      "component": "index"
    },
    "component/media/camera": {
      "component": "index"
    },
    "component/thirdParty/map": {
      "component": "index"
    },
    "framework/communication/parent-children": {
      "component": "index"
    },
    "framework/communication/siblings": {
      "component": "index"
    },
    "framework/deeplink": {
      "component": "index"
    },
    "framework/async": {
      "component": "index"
    },
    "framework/lifecycle": {
      "component": "index"
    },
    "framework/exit": {
      "component": "index"
    },
    "framework/navigate": {
      "component": "index"
    },
    "framework/exit/pages/page1": {
      "component": "index",
      "path": "/exit/page1"
    },
    "framework/exit/pages/page2": {
      "component": "index",
      "path": "/exit/page2"
    },
    "framework/exit/pages/page3": {
      "component": "index",
      "path": "/exit/page3"
    },
    "framework/screenorientation": {
      "component": "index"
    },
    "framework/pageWH": {
      "component": "index"
    },
    "framework/style-layout/style": {
      "component": "index"
    },
    "framework/style-layout/less": {
      "component": "index"
    },
    "framework/style-layout/layout": {
      "component": "index"
    },
    "framework/style-layout/update-style": {
      "component": "index"
    },
    "framework/style-layout/position": {
      "component": "index"
    },
    "framework/filter": {
      "component": "index"
    },
    "framework/i18n": {
      "component": "index"
    },
    "framework/computed": {
      "component": "index"
    },
    "framework/directive/for": {
      "component": "index"
    },
    "framework/directive/if-show": {
      "component": "index"
    },
    "framework/directive/data": {
      "component": "index"
    },
    "framework/directive/block": {
      "component": "index"
    },
    "framework/directive/slot": {
      "component": "index"
    },
    "framework/events/native-events": {
      "component": "index"
    },
    "framework/events/custom-events": {
      "component": "index"
    },
    "framework/events/event-bubble": {
      "component": "index"
    },
    "framework/optimization/JSONparse": {
      "component": "index"
    },
    "framework/optimization/bindPage": {
      "component": "index"
    },
    "framework/componentlifecycle": {
      "component": "index"
    },
    "interface/thirdparty/alipay": {
      "component": "index"
    },
    "interface/thirdparty/wxpay": {
      "component": "index"
    },
    "interface/thirdparty/serviceshare": {
      "component": "index"
    },
    "interface/thirdparty/wxaccount": {
      "component": "index"
    },
    "interface/thirdparty/qqaccount": {
      "component": "index"
    },
    "interface/thirdparty/wbaccount": {
      "component": "index"
    },
    "interface/system/brightness": {
      "component": "index"
    },
    "interface/system/calendar": {
      "component": "index"
    },
    "interface/system/messageChannel": {
      "component": "index"
    },
    "interface/system/clipboard": {
      "component": "index"
    },
    "interface/system/device": {
      "component": "index"
    },
    "interface/system/app": {
      "component": "index"
    },
    "interface/system/fetch": {
      "component": "index"
    },
    "interface/system/file": {
      "component": "index"
    },
    "interface/system/filerw": {
      "component": "index"
    },
    "interface/system/mkdir": {
      "component": "index"
    },
    "interface/system/geolocation": {
      "component": "index"
    },
    "interface/system/image": {
      "component": "index"
    },
    "interface/system/media": {
      "component": "index"
    },
    "interface/system/network": {
      "component": "index"
    },
    "interface/system/notification": {
      "component": "index"
    },
    "interface/system/prompt": {
      "component": "index"
    },
    "interface/system/package": {
      "component": "index"
    },
    "interface/system/forbid": {
      "component": "index"
    },
    "interface/system/qrcode": {
      "component": "index"
    },
    "interface/system/request": {
      "component": "index"
    },
    "interface/system/router": {
      "component": "index"
    },
    "interface/system/cipher": {
      "component": "index"
    },
    "interface/system/fitCutout": {
      "component": "index"
    },
    "interface/system/router/detail": {
      "component": "index"
    },
    "interface/system/record": {
      "component": "index"
    },
    "interface/system/sensor": {
      "component": "index"
    },
    "interface/system/health": {
      "component": "index"
    },
    "interface/system/share": {
      "component": "index"
    },
    "interface/system/shortcut": {
      "component": "index"
    },
    "interface/system/storage": {
      "component": "index"
    },
    "interface/system/exchange": {
      "component": "index"
    },
    "interface/system/vibrator": {
      "component": "index"
    },
    "interface/system/volume": {
      "component": "index"
    },
    "interface/system/battery": {
      "component": "index"
    },
    "interface/system/webview": {
      "component": "index"
    },
    "interface/system/sms": {
      "component": "index"
    },
    "interface/system/contact": {
      "component": "index"
    },
    "interface/system/websocket": {
      "component": "index"
    },
    "interface/system/wifi": {
      "component": "index"
    },
    "interface/system/setting": {
      "component": "index"
    },
    "interface/system/alarm": {
      "component": "index"
    },
    "interface/system/bluetooth": {
      "component": "index"
    },
    "interface/system/zip": {
      "component": "index"
    },
    "interface/manufacturer/ad": {
      "component": "index"
    },
    "component/style/animation": {
      "component": "index"
    },
    "component/style/transform": {
      "component": "index"
    },
    "component/style/translatepercent": {
      "component": "index"
    },
    "component/style/backgroundposition": {
      "component": "index"
    },
    "component/style/background9image": {
      "component": "index"
    },
    "component/style/font-family": {
      "component": "index"
    },
    "scenario/effect/changestyle": {
      "component": "index"
    },
    "scenario/scene/form": {
      "component": "index"
    },
    "scenario/scene/menutop": {
      "component": "index"
    },
    "scenario/scene/ceiling": {
      "component": "index"
    },
    "scenario/effect/photo": {
      "component": "index"
    },
    "scenario/web/h5download": {
      "component": "index"
    },
    "scenario/web/postMessage": {
      "component": "index"
    },
    "scenario/scene/qrcode": {
      "component": "index"
    },
    "scenario/component/titlebar": {
      "component": "index"
    },
    "scenario/component/statusbar": {
      "component": "index"
    },
    "scenario/scene/listcombination": {
      "component": "index"
    },
    "scenario/scene/verification": {
      "component": "index"
    },
    "scenario/effect/simulate-swipe": {
      "component": "index"
    },
    "scenario/effect/offset": {
      "component": "index"
    },
    "scenario/scene/elec-business": {
      "component": "index"
    },
    "scenario/web/customWeb": {
      "component": "index"
    },
    "scenario/web/customWeb/wrap": {
      "component": "index"
    },
    "scenario/component/finegrainsize": {
      "component": "index"
    },
    "scenario/scene/render-control": {
      "component": "index"
    },
    "scenario/scene/accessible": {
      "component": "index"
    }
  }
}
;