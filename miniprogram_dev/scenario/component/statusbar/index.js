import {Page,Component} from '../../../quickapp2weixin/index.js';
var hsl = require('hsl-to-hex');
Page({
onekit_getValue(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.getvalue0);this.getValue.apply(this,args);},
    "private":{
        "componentName":'沉浸式状态栏',
        "step":10,
        "opacityValue":0,
        "hueValue":0,
        "hexValue":'#000000',
        "saturation":0,
        "luminosity":0,
        "enabled":true,
        "autoHue":false
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "setStatusBarOpacity"(v){
      this.$page.setStatusBar({
        "backgroundOpacity":parseInt(v) / 100
});
    },
    "setStatusBarBg"(v){
      this.$page.setStatusBar({
        "backgroundColor":v
});
    },
    "switchStatusBar"(){
      this.setData({enabled:!this.data.enabled});
      this.$page.setStatusBar({
        "immersive":this.data.enabled
});
    },
    "getValue"(name,e){
      this.setData({name:e.progress});
          switch(name){
case 'opacityValue':
        this.setStatusBarOpacity(this.data.opacityValue);
case 'hueValue':
        this.setData({hexValue:hsl(this.data.hueValue,40,60)});
        this.setStatusBarBg(this.data.hexValue);
      default:
        console.log('');
    };
    },
    "autoSetOpacity"(){
      this.setData({opacityValue:100});
      var flag = 1;
      var count = 10;
      let t = setInterval(()=>{
      if((((this.data.opacityValue < 0) || this.data.opacityValue == 0) || this.data.opacityValue > 100) || this.data.opacityValue == 100)
        {
          flag = -flag;
          count = count - 1;
          if(count < 0)
            {
              clearInterval(t);
            }
        };
      this.setData({opacityValue:this.data.opacityValue + ((1 * flag))});
      this.setStatusBarOpacity(this.data.opacityValue);
    },10);
    },
    "autoSetHue"(){
      if(this.data.hueValue != 0)
      {
        this.setData({autoHue:false});
        return;
      };
      this.setData({autoHue:true});
      this.setData({saturation:0});
      this.setData({luminosity:0});
      var lumFlag = -1;
      var satFlag = -1;
      let t = setInterval(()=>{
      if((this.data.hueValue < 360) && (this.data.autoHue))
        {
          if((this.data.luminosity < 100) && this.data.luminosity > 0)
            {
              this.setData({luminosity:this.data.luminosity + ((1 * lumFlag))});
            }
else
            if((this.data.luminosity == 100) || this.data.luminosity == 0)
              {
                lumFlag = -lumFlag;
                this.setData({luminosity:this.data.luminosity + ((1 * lumFlag))});
                this.setData({hueValue:this.data.hueValue + 10});
              }
          if((this.data.saturation < 100) && this.data.saturation > 0)
            {
              this.setData({saturation:this.data.saturation + ((1 * satFlag))});
            }
else
            if((this.data.saturation == 100) || this.data.saturation == 0)
              {
                satFlag = -satFlag;
                this.setData({saturation:this.data.saturation + ((1 * satFlag))});
                this.setData({hueValue:this.data.hueValue + 10});
              }
        }
else
        {
          clearInterval(t);
          this.setData({hueValue:0});
          this.setData({autoHue:false});
        };
      this.setData({hexValue:hsl(this.data.hueValue,this.data.saturation,this.data.luminosity)});
      this.setStatusBarBg(this.data.hexValue);
    },10);
    }
});

