import {Page,Component} from '../../../quickapp2weixin/index.js';
import {dataComponentListFinegrainsize} from '../../../common/js/data';
Page({
    "private":{
        "productList":[
        ],
        "productAddList":dataComponentListFinegrainsize
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'细粒度划分list-item'
});
      this.setData({productList:[
    ].concat(this.data.productAddList)});
    },
    "loadMoreData"(){
      this.setData({productList:this.data.productList.concat(this.data.productAddList)});
    }
});

