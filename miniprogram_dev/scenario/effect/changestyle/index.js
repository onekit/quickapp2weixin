import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_router} from '../../../quickapp2weixin/index';const router = system_router;
Page({
    "private":{
        "params":'',
        "times":0,
        "text":'style 属性的数据绑定',
        "str":'',
        "typeList":[
          'button',
          'checkbox',
          'radio',
          'text',
          'email',
          'date',
          'time',
          'number',
          'password'
        ],
        "csstext":'fontSize: 19px; backgroundColor: red; color: #123456;',
        "csstextJson":{
            "fontSize":'20px',
            "backgroundColor":'yellow',
            "color":'black'
},
        "csstextListJson":[
          {
              "fontSize":'20px',
              "background-color":'black',
              "color":'green'
},
          {
              "fontSize":'40px',
              "backgroundColor":'yellow',
              "color":'blue'
},
          {
              "font-size":'60px',
              "backgroundColor":'pink',
              "color":'red'
}
        ],
        "csstextList":[
          'font-size: 40px; backgroundColor: green; color: black;',
          'font-size: 80px; background-color: blue; color: white;',
          'fontSize: 120px; background-color: red; color: pink;'
        ]
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.text
});
      this.setData({str:JSON.stringify(this.data.csstextJson)});
    },
    "rolling"(){
          let times=this.data.times;times++;this.setData({[`times`]:times});
      this.setData({csstext:this.data.csstextList[this.data.times % this.data.csstextList.length]});
      this.setData({csstextJson:this.data.csstextListJson[this.data.times % this.data.csstextListJson.length]});
      this.setData({str:JSON.stringify(this.data.csstextJson)});
    }
});

