import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
import {system_router} from '../../../quickapp2weixin/index';const router = system_router;
Page({
    "private":{
        "text":'获取元素宽高及位置',
        "offsetWidth":0,
        "offsetHeight":0,
        "offsetLeft":0,
        "offsetTop":0
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.text
});
    },
    "getOffset"(e){
      this.setData({offsetWidth:e.offsetWidth});
      this.setData({offsetHeight:e.offsetHeight});
      this.setData({offsetLeft:e.offsetLeft});
      this.setData({offsetTop:e.offsetTop});
    }
});

