import {Page,Component} from '../../../../quickapp2weixin/index.js';
Page({
    "protected":{
        "url":'',
        "titleBarText":'',
        "titleBarTextColor":'',
        "titleBarBackgroundColor":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.titleBarText,
        "textColor":this.data.titleBarTextColor,
        "backgroundColor":this.data.titleBarBackgroundColor
});
    },
    "onTitleReceived"(e){
      const title = (e.title) || ('');
      this.$page.setTitleBar({
        "text":title
});
    }
});

