import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_router} from '../../../quickapp2weixin/index';const router = system_router;
Page({
    "onInit"(){
      this.$page.setTitleBar({
        "text":'自定义web组件'
});
    },
    "openWeb"(){
      router.push({
        "uri":'/scenario/web/customWeb/wrap',
        "params":{
            "url":'https://www.quickapp.cn/',
            "titleBarText":'',
            "titleBarTextColor":'',
            "titleBarBackgroundColor":''
}
});
    }
});

