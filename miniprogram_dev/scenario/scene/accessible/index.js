import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "ariaLabel":'',
        "checkedVal":true,
        "val":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'无障碍模式'
});
    },
    "accessibleFocus"(){
      this.setData({checkedVal:!this.data.checkedVal});
    },
    "showChangePrompt"(e){
      this.setData({val:e.value});
    }
});

