import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
onekit_showMenu(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.showmenu0);this.showMenu.apply(this,args);},
    "private":{
        "menu1":{
            "open":false,
            "list":[
              '广场舞',
              '双人舞',
              '健身操'
            ]
},
        "menu2":{
            "open":false,
            "list":[
              '简单',
              '适中',
              '略难'
            ]
},
        "menuShowList":[
        ]
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'顶部菜单'
});
    },
    "showMenu"(menuName){
      if(this.data.menuShowList.length == 0)
      {
        this.setData({menuShowList:this.data[menuName].list});
        this.setData({[`menuName.open`]:true});
      }
else
      {
        this.setData({menuShowList:[
        ]});
        this.setData({[`menuName.open`]:false});
      };
    }
});

