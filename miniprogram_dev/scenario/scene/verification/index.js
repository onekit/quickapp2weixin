import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "btn_class":'disabled',
        "btn_submit_class":'disabled',
        "btn_val":'发送验证码',
        "phone":'',
        "second":0
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'手机号验证'
});
    },
    "verifyPhone"(e){
      this.setData({phone:e.text});
      if((/^1[34578]\d{9}$/.test(this.data.phone)) && (!this.data.second))
      {
        this.setData({btn_class:''});
      }
else
      {
        this.setData({btn_class:'disabled'});
      };
      if(!/^1[34578]\d{9}$/.test(this.data.phone))
      {
        this.setData({btn_submit_class:'disabled'});
      };
    },
    "verifyCode"(e){
      if((/^\d{6}$/.test(e.text)) && (/^1[34578]\d{9}$/.test(this.data.phone)))
      {
        this.setData({btn_submit_class:''});
      }
else
      {
        this.setData({btn_submit_class:'disabled'});
      };
    },
    "sendVerification"(){
      const self = this;
      if(!self.btn_class)
      {
        self.second = 60;
        self.btn_class = 'disabled';
        self.intervalID = setInterval(self.count.bind(self),1000);
      };
    },
    "count"(){
      const self = this;
      --self.second;
      self.btn_val = self.second + 's后再次发送验证码';
      if(self.second == 0)
      {
        clearInterval(self.intervalID);
        self.btn_class = '';
        self.btn_val = '发送验证码';
      };
    }
});

