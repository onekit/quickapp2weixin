import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "onInit"(){
      this.$page.setTitleBar({
        "text":'表单示例'
});
    },
    "private":{
        "range":[
          'text1',
          'text2',
          'text3'
        ],
        "text":'default',
        "date":'2017-7-1',
        "time":'00:00',
        "progressValue":0,
        "switchStatus":'on'
},
    "changeText"(e){
      this.setData({text:e.newValue});
    },
    "changeDate"(e){
      this.setData({date:((((((e.year + '-')) + ((e.month + 1)))) + '-')) + e.day});
    },
    "changeTime"(e){
      this.setData({time:((e.hour + ':')) + e.minute});
    },
    "getProgressValue"(e){
      this.setData({progressValue:e.progress});
    },
    "changeSwitchStatus"(e){
      this.setData({switchStatus:e.checked?'on':'off'});
    }
});

