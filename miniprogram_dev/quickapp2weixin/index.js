module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {


module.exports = function (body, success, fail, complete) {
  try {
    return body(function (res) {
      if (success) {
        success(res)
      }
      if (complete) {
        complete(res)
      }
    }, function (res) {
      if (fail) {
        fail(res)
      }
      if (complete) {
        complete(res)
      }
    })
  } catch (e) {
    var res = {
      errMsg: e.message
    }
    if (fail) {
      fail(res)
    }
    if (complete) {
      complete(res)
    }
    return null
  }
}


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("oneutil");

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", { value: true });
exports.system_zip = exports.system_wifi = exports.system_websocketfactory = exports.system_webview = exports.system_vibrator = exports.system_storage = exports.system_share = exports.system_sensor = exports.system_router = exports.system_request = exports.system_record = exports.system_prompt = exports.system_network = exports.system_media = exports.system_image = exports.system_geolocation = exports.system_file = exports.system_fetch = exports.system_dhtml = exports.system_device = exports.system_contact = exports.system_clipboard = exports.system_calendar = exports.system_brightness = exports.system_bluetooth = exports.system_battery = exports.system_barcode = exports.system_audio = exports.service_account = exports.service_wxpay = exports.service_wxaccount = exports.service_share = exports.service_alipay = exports.Page = exports.Component = exports.Behavior = exports.App = void 0;
var OnekitApp_1 = __webpack_require__(10);
exports.App = OnekitApp_1.default;
var OnekitBehavior_1 = __webpack_require__(11);
exports.Behavior = OnekitBehavior_1.default;
var OnekitComponent_1 = __webpack_require__(12);
exports.Component = OnekitComponent_1.default;
var OnekitPage_1 = __webpack_require__(13);
exports.Page = OnekitPage_1.default;
var service_alipay_1 = __webpack_require__(14);
exports.service_alipay = service_alipay_1.default;
var service_share_1 = __webpack_require__(15);
exports.service_share = service_share_1.default;
var service_wxaccount_1 = __webpack_require__(16);
exports.service_wxaccount = service_wxaccount_1.default;
var service_wxpay_1 = __webpack_require__(17);
exports.service_wxpay = service_wxpay_1.default;
var service_account_1 = __webpack_require__(18);
exports.service_account = service_account_1.default;
var system_audio_1 = __webpack_require__(19);
exports.system_audio = system_audio_1.default;
var system_barcode_1 = __webpack_require__(20);
exports.system_barcode = system_barcode_1.default;
var system_battery_1 = __webpack_require__(21);
exports.system_battery = system_battery_1.default;
var system_bluetooth_1 = __webpack_require__(22);
exports.system_bluetooth = system_bluetooth_1.default;
var system_brightness_1 = __webpack_require__(23);
exports.system_brightness = system_brightness_1.default;
var system_calendar_1 = __webpack_require__(24);
exports.system_calendar = system_calendar_1.default;
var system_clipboard_1 = __webpack_require__(25);
exports.system_clipboard = system_clipboard_1.default;
var system_contact_1 = __webpack_require__(26);
exports.system_contact = system_contact_1.default;
var system_device_1 = __webpack_require__(27);
exports.system_device = system_device_1.default;
var system_dhtml_1 = __webpack_require__(28);
exports.system_dhtml = system_dhtml_1.default;
var system_fetch_1 = __webpack_require__(29);
exports.system_fetch = system_fetch_1.default;
var system_file_1 = __webpack_require__(30);
exports.system_file = system_file_1.default;
var system_geolocation_1 = __webpack_require__(31);
exports.system_geolocation = system_geolocation_1.default;
var system_image_1 = __webpack_require__(32);
exports.system_image = system_image_1.default;
var system_media_1 = __webpack_require__(33);
exports.system_media = system_media_1.default;
var system_network_1 = __webpack_require__(34);
exports.system_network = system_network_1.default;
var system_prompt_1 = __webpack_require__(35);
exports.system_prompt = system_prompt_1.default;
var system_record_1 = __webpack_require__(36);
exports.system_record = system_record_1.default;
var system_request_1 = __webpack_require__(37);
exports.system_request = system_request_1.default;
var system_router_1 = __webpack_require__(39);
exports.system_router = system_router_1.default;
var system_sensor_1 = __webpack_require__(40);
exports.system_sensor = system_sensor_1.default;
var system_share_1 = __webpack_require__(41);
exports.system_share = system_share_1.default;
var system_storage_1 = __webpack_require__(42);
exports.system_storage = system_storage_1.default;
var system_vibrator_1 = __webpack_require__(43);
exports.system_vibrator = system_vibrator_1.default;
var system_websocketfactory_1 = __webpack_require__(44);
exports.system_websocketfactory = system_websocketfactory_1.default;
var system_webview_1 = __webpack_require__(46);
exports.system_webview = system_webview_1.default;
var system_wifi_1 = __webpack_require__(47);
exports.system_wifi = system_wifi_1.default;
var system_zip_1 = __webpack_require__(48);
exports.system_zip = system_zip_1.default;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = OnekitApp;

var _oneutil = __webpack_require__(1);

function OnekitApp(quick_object) {
  // const wx_object = quick_object
  // return App(wx_object)

  var wx_object = {
    onekit: {},
    onLaunch: function onLaunch() {
      // GUID
      var GUID = _oneutil.STRING.GUID();
      // 取 GUID
      wx.getStorage({
        key: 'GUID',
        success: function success(res) {
          if (res.data === '') {
            // 存 GUID
            wx.setStorage({
              key: 'GUID',
              data: GUID
            });
          }
        }
      });

      // getApp().on_launch_time = new Date().getTime()
      if (quick_object.onLaunch) {
        quick_object.onLaunch();
      }
    },
    onShow: function onShow() {
      // getApp().show_time_consuming = new Date().getTime() - getApp().on_launch_time
      if (quick_object.onShow) {
        quick_object.onShow();
      }
    },
    onHide: function onHide() {
      if (quick_object.onHide) {
        quick_object.onHide();
      }
    },
    onError: function onError() {
      if (quick_object.onError) {
        quick_object.onError();
      }
    },
    onPageNotFound: function onPageNotFound() {
      if (quick_object.onPageNotFound) {
        quick_object.onPageNotFound();
      }
    }
  };
  for (var _iterator = Object.keys(quick_object), _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
    var _ref;

    if (_isArray) {
      if (_i >= _iterator.length) break;
      _ref = _iterator[_i++];
    } else {
      _i = _iterator.next();
      if (_i.done) break;
      _ref = _i.value;
    }

    var key = _ref;

    var member = quick_object[key];
    switch (key) {
      case 'onLaunch':
      case 'onShow':
      case 'onHide':
      case 'onError':
      case 'onPageNotFound':
        break;
      default:
        wx_object[key] = member;
        break;
    }
  }
  return App(wx_object);
} /* eslint-disable no-console */
/* eslint-disable camelcase */
/* eslint-disable object-curly-spacing */

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = OnekitBehavior;

var _oneutil = __webpack_require__(1);

function OnekitBehavior(object) {
  var quick_object = {
    onInit: function onInit(query) {
      var created = void 0;
      if (object.lifetimes && object.lifetimes.created) {
        created = object.lifetimes.created;
      } else if (object.created) {
        created = object.created;
      } else {
        created = function created() {};
      }
      created.call(this, query);
    },
    attached: function attached() {
      var attached = void 0;
      if (object.lifetimes && object.lifetimes.attached) {
        attached = object.lifetimes.attached;
      } else if (object.attached) {
        attached = object.attached;
      } else {
        attached = function attached() {};
      }
      attached.call(this);
      // //////
      var ready = void 0;
      if (object.lifetimes && object.lifetimes.ready) {
        ready = object.lifetimes.ready;
      } else if (object.ready) {
        ready = object.ready;
      } else {
        ready = function ready() {};
      }
      ready.call(this);
    },
    didUnmount: function didUnmount() {
      var detached = void 0;
      if (object.lifetimes && object.lifetimes.detached) {
        detached = object.lifetimes.detached;
      } else if (object.detached) {
        detached = object.detached;
      } else {
        detached = function detached() {};
      }
      detached.call(this);
    }
  };
  if (object) {
    if (!object.methods) {
      object.methods = {};
    }
    object.methods.triggerEvent = function (name, data) {
      var funcName = 'on' + _oneutil.STRING.firstUpper(name);
      if (this.props[funcName]) {
        this.props[funcName](data);
      }
    };
    // object.methods.createSelectorQuery = wx.createSelectorQuery
  }
  for (var _iterator = Object.keys(object), _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
    var _ref;

    if (_isArray) {
      if (_i >= _iterator.length) break;
      _ref = _iterator[_i++];
    } else {
      _i = _iterator.next();
      if (_i.done) break;
      _ref = _i.value;
    }

    var key = _ref;

    var value = object[key];
    if (!value) {
      continue;
    }
    switch (key) {
      case 'properties':
        quick_object.props = {};
        for (var _iterator2 = Object.keys(value), _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
          var _ref2;

          if (_isArray2) {
            if (_i2 >= _iterator2.length) break;
            _ref2 = _iterator2[_i2++];
          } else {
            _i2 = _iterator2.next();
            if (_i2.done) break;
            _ref2 = _i2.value;
          }

          var p = _ref2;

          var v = value[p];
          quick_object.props[p] = v && v.value ? v.value : null;
        }
        break;
      default:
        quick_object[key] = value;
    }
  }

  return quick_object;
} /* eslint-disable camelcase */

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = OnekitComponent;

var _oneutil = __webpack_require__(1);

function OnekitComponent(quick_object) {
  var properties = {};
  var wx_object = {
    behaviors: [],
    data: {},
    created: function created() {
      if (this.props) {
        for (var _iterator = Object.keys(this.props), _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
          var _ref;

          if (_isArray) {
            if (_i >= _iterator.length) break;
            _ref = _iterator[_i++];
          } else {
            _i = _iterator.next();
            if (_i.done) break;
            _ref = _i.value;
          }

          var k = _ref;

          var v = this.props[k];
          properties[k] = v;
          wx_object.data[k] = v;
        }
      }
    },
    attached: function attached() {
      // ///
      var onInit = void 0;
      if (quick_object.onInit) {
        onInit = quick_object.onInit;
      } else {
        onInit = function onInit() {};
      }
      onInit.call(this);
      // //////
      var onReady = void 0;
      if (quick_object.onReady) {
        onReady = quick_object.onReady;
      } else {
        onReady = function onReady() {};
      }
      onReady.call(this);
    },
    didUnmount: function didUnmount() {
      var onDestroy = void 0;
      if (quick_object.onDestroy) {
        onDestroy = quick_object.onDestroy;
      } else {
        onDestroy = function onDestroy() {};
      }
      onDestroy.call(this);
    },

    methods: {
      get properties() {
        return properties;
      }

    }
  };
  if (quick_object) {
    if (!quick_object.methods) {
      quick_object.methods = {};
    }
    quick_object.methods.triggerEvent = function (name, data) {
      var funcName = 'on' + _oneutil.STRING.firstUpper(name);
      if (this.props[funcName]) {
        this.props[funcName](data);
      }
    };
    quick_object.methods.createSelectorQuery = wx.createSelectorQuery;
  }
  for (var _iterator2 = Object.keys(quick_object), _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
    var _ref2;

    if (_isArray2) {
      if (_i2 >= _iterator2.length) break;
      _ref2 = _iterator2[_i2++];
    } else {
      _i2 = _iterator2.next();
      if (_i2.done) break;
      _ref2 = _i2.value;
    }

    var key = _ref2;

    var value = quick_object[key];
    if (!value) {
      continue;
    }
    switch (key) {
      case 'properties':
        wx_object.props = {};
        for (var _iterator4 = Object.keys(value), _isArray4 = Array.isArray(_iterator4), _i4 = 0, _iterator4 = _isArray4 ? _iterator4 : _iterator4[Symbol.iterator]();;) {
          var _ref4;

          if (_isArray4) {
            if (_i4 >= _iterator4.length) break;
            _ref4 = _iterator4[_i4++];
          } else {
            _i4 = _iterator4.next();
            if (_i4.done) break;
            _ref4 = _i4.value;
          }

          var _k = _ref4;

          var p = value[_k];
          var _v = p && p.value ? p.value : null;
          wx_object.props[_k] = _v;
          properties[_k] = _v;
        }
        break;
      case 'data':
        for (var _iterator5 = Object.keys(value), _isArray5 = Array.isArray(_iterator5), _i5 = 0, _iterator5 = _isArray5 ? _iterator5 : _iterator5[Symbol.iterator]();;) {
          var _ref5;

          if (_isArray5) {
            if (_i5 >= _iterator5.length) break;
            _ref5 = _iterator5[_i5++];
          } else {
            _i5 = _iterator5.next();
            if (_i5.done) break;
            _ref5 = _i5.value;
          }

          var _k2 = _ref5;

          wx_object.data[_k2] = value[_k2];
        }
        break;

      default:
        if (typeof value === 'function') {
          wx_object.methods[key] = value;
        } else {
          wx_object[key] = value;
        }
        break;
    }
  }
  if (quick_object.data) {
    for (var _iterator3 = Object.keys(quick_object.data), _isArray3 = Array.isArray(_iterator3), _i3 = 0, _iterator3 = _isArray3 ? _iterator3 : _iterator3[Symbol.iterator]();;) {
      var _ref3;

      if (_isArray3) {
        if (_i3 >= _iterator3.length) break;
        _ref3 = _iterator3[_i3++];
      } else {
        _i3 = _iterator3.next();
        if (_i3.done) break;
        _ref3 = _i3.value;
      }

      var k = _ref3;

      var v = quick_object.data[k];
      wx_object.data[k] = v;
    }
  }
  return Component(wx_object);
} /* eslint-disable camelcase */

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = OnekitPage;
/* eslint-disable camelcase */
/* eslint-disable no-console */
function prop(THIS, p) {
  Object.defineProperty(THIS, p, {
    configurable: true,
    enumerable: true,
    get: function get() {
      return THIS.data[p];
    },
    set: function set(value) {
      // console.log('setData', p, value)
      var json = {};
      json[p] = value;
      THIS.setData(json);
    }
  });
}
function OnekitPage(quick_object) {
  var wx_object = {
    data: {},
    $set: function $set(key, value) {
      var json = {};
      json[key] = value;
      this.setData(json);
    },
    onLoad: function onLoad() {
      this.$page = {
        name: this.route,
        path: this.__route__,
        component: '',
        setTitleBar: function setTitleBar(quick_object) {
          var quick_text = quick_object.text || '';
          quick_object = null;
          //
          var wx_title = quick_text;
          wx.setNavigationBarTitle({ title: wx_title });
        }
      };
      var data = {};
      if (quick_object.private) {
        for (var _iterator = Object.keys(quick_object.private), _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
          var _ref;

          if (_isArray) {
            if (_i >= _iterator.length) break;
            _ref = _iterator[_i++];
          } else {
            _i = _iterator.next();
            if (_i.done) break;
            _ref = _i.value;
          }

          var ky = _ref;

          prop(this, ky);
          data[ky] = quick_object.private[ky];
        }
      }
      this.setData(data);
      if (quick_object.onInit) {
        quick_object.onInit.apply(this);
      }
    }
  };
  for (var _iterator2 = Object.keys(quick_object), _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
    var _ref2;

    if (_isArray2) {
      if (_i2 >= _iterator2.length) break;
      _ref2 = _iterator2[_i2++];
    } else {
      _i2 = _iterator2.next();
      if (_i2.done) break;
      _ref2 = _i2.value;
    }

    var key = _ref2;

    var value = quick_object[key];
    switch (key) {
      case 'private':
        break;
      case 'onInit':
        break;
      default:
        wx_object[key] = value;
    }
  }
  return Page(wx_object);
}

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable no-console */
/* eslint-disable camelcase */
exports.default = {
  /**
   * 支付宝app支付暂未开通
   *
   */
  pay: function pay(quick_object) {
    if (!quick_object) {
      return;
    }
    // const quick_callback = quick_object.callback
    // //////////////////////
    console.error('[quick2baidu]', '暂未开通支付宝app支付');
  }
};

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable no-console */
/* eslint-disable camelcase */
exports.default = {

  // 获取服务提供商
  getProvider: function getProvider() {
    return wx.getSystemInfoSync().brand;
  },
  share: function share(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_shareType = quick_object.shareType;
    var quick_title = quick_object.title;
    // const quick_summary = quick_object.summary
    // const quick_targetUrl = quick_object.targetUrl
    var quick_imagePath = quick_object.imagePath;
    // const quick_mediaUrl = quick_object.mediaUrl
    var quick_platforms = quick_object.platforms;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_cancel = quick_object.cancel;
    // ////////////////////////////////////////
    var wx_object = {};
    if (quick_platforms) {
      var quick_temp = [];
      for (var _iterator = quick_platforms, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
        var _ref;

        if (_isArray) {
          if (_i >= _iterator.length) break;
          _ref = _iterator[_i++];
        } else {
          _i = _iterator.next();
          if (_i.done) break;
          _ref = _i.value;
        }

        var platforms = _ref;

        console.log(platforms);
        if (['baidu_CIRCLE', 'WEIBO', 'QQ', 'SYSTEM'].indexOf(platforms) > 0) {
          quick_temp.push(platforms);
        }
      }

      if (quick_temp.lenght > 0) {
        console.warn("[quick2baidu] only support 'baidu'");
      }
    }

    var flag = [0, 2].indexOf(quick_shareType);
    if (flag === 0) {
      // 图文类型
      wx_object.title = quick_title;
      if (quick_imagePath) {
        wx_object.imageUrl = quick_imagePath;
      }
    } else if (flag === 1) {
      // 纯图片
      wx_object.imageUrl = quick_imagePath;
    } else {
      // 其它类型暂不支持
      if (quick_fail) {
        quick_fail("[quick2baidu] only support 'quick_shareType: 0 or 2'", 203);
      }
      return;
    }
    var wx_callback = {};
    if (quick_success) {
      wx_callback.success = quick_success;
    }
    if (quick_fail) {
      wx_callback.fail = quick_fail;
    }
    if (quick_cancel) {
      wx_callback.cancel = quick_cancel;
    }

    wx.navigateTo({
      url: '/onekit/page/share.share/share.share?param=' + JSON.stringify(wx_object),
      events: wx_callback
    });
  },
  getAvailablePlatforms: function getAvailablePlatforms(quick_object) {
    if (!quick_object) {
      return;
    }
    // /////////////////////////////////////////
    var quick_success = quick_object.success;
    // const quick_fail = quick_object.fail
    var quick_complete = quick_object.complete;
    // //////////////////////////////////////
    var result = ['baidu']; // , "baidu_CIRCLE"
    if (quick_success) {
      quick_success({
        platforms: result
      });
    }
    if (quick_complete) {
      quick_complete({
        platforms: result
      });
    }
  }
};

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable no-console */
/* eslint-disable camelcase */

exports.default = {
  getType: function getType() {
    return 'APP';
  },
  authorize: function authorize(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    // const quick_cancel = quick_object.cancel
    // ////////////////////////////////////////
    wx.login({
      success: function success(res) {
        console.log('授权成功 ：', res);
        wx.request({
          url: 'http://192.168.22.116/quick/baidu/wxaccount/authorize',
          data: {
            JSCODE: res.code
          },
          success: function success(res) {
            if (quick_success) {
              quick_success(res);
            }
          },
          fail: function fail(res) {
            if (quick_fail) {
              quick_fail(res);
            }
          }
        });
      },
      fail: function fail(res) {
        console.log('授权失败：', res);
      }
    });
  }
};

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable no-console */
/* eslint-disable camelcase */
// import PROMISE from '../node_modules/oneutil/PROMISE'

exports.default = {
  getType: function getType() {
    return {
      trade_type: 'APP'
    };
  },
  pay: function pay(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_extra = quick_object.extra;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_cancel = quick_object.cancel;
    // // /////////////////////////////////
    // PROMISE((SUCCESS) => {
    //   wx.requestPayment({
    //     timeStamp: quick_extra.time_stamp,
    //     nonceStr: quick_extra.nonce_str,
    //     package: quick_extra.package_value,
    //     paySign: quick_extra.order_sign,
    //     signType: 'MD5',
    //     success: () => {
    //       const quick_res = {
    //         prepayid: ''
    //       }
    //       SUCCESS(quick_res)
    //     }
    //   })
    // }, quick_success, quick_fail)
    var wx_object = {};
    wx_object.timeStamp = quick_extra.timeStamp;
    wx_object.nonceStr = quick_extra.nonceStr;
    wx_object.package = quick_extra.package;
    wx_object.paySign = quick_extra.paySign;

    wx_object.success = function (res) {
      console.log('wx-pay-success', res);
      if (quick_success) {
        quick_success({
          prepayid: wx_object.prepayid
        });
      }
    };

    wx_object.fail = function (res) {
      console.log('wx-pay-fail', res);

      if (res.errMsg.indexOf('cancel') > -1 && quick_cancel) {
        // 用户取消支付
        quick_cancel(res);
      } else {
        // 支付失败
        quick_fail(res);
      }
    };

    console.log('支付参数', wx_object);
    wx.requestPayment(wx_object);
  }
};

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable no-console */
/* eslint-disable camelcase */
// import PROMISE from '../node_modules/oneutil/PROMISE'

exports.default = {
  getProvider: function getProvider() {
    return '';
  },
  isLogin: function isLogin() {}
};

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable consistent-return */
/* eslint-disable getter-return */
/* eslint-disable no-console */
/* eslint-disable camelcase */

exports.default = {
  play: function play() {
    getApp().onekit_inneraudioContextplay = 'play';
    this.inneraudioContext.play();
  },
  pause: function pause() {
    getApp().onekit_inneraudioContextplay = 'pause';
    this.inneraudioContext.pause();
  },
  stop: function stop() {
    getApp().onekit_inneraudioContextplay = 'stop';
    this.inneraudioContext.stop();
  },
  getPlayState: function getPlayState() {
    var state = void 0;
    switch (getApp().onekit_inneraudioContextplay) {
      case 'play':
        state = 'play';
        break;
      case 'pase':
        state = 'pase';
        break;
      default:
        state = 'stop';
        break;
    }
    var quick_res = {
      state: state
    };
    return quick_res;
  },


  set src(src) {
    var InnerAudioContext = wx.createInnerAudioContext();
    this.inneraudioContext = InnerAudioContext;
    this.inneraudioContext.src = src;
  },

  set currentTime(currentTime) {
    this.inneraudioContext.currentTime = currentTime;
  },
  get currentTime() {
    if (wx.createInnerAudioContext().currentTime) {
      return wx.createInnerAudioContext().currentTime;
    }
  },

  get duration() {
    if (wx.createInnerAudioContext().duration) {
      return wx.createInnerAudioContext().duration;
    }
  },

  set autoplay(autoplay) {
    this.inneraudioContext.autoplay = autoplay;
  },
  get autoplay() {
    if (getApp().onekit_autoplay) {
      return getApp().onekit_autoplay;
    } else {
      return false;
    }
  },

  set loop(loop) {
    this.inneraudioContext.loop = loop;
  },
  get loop() {
    if (this.inneraudioContext.loop) {
      return this.inneraudioContext.loop;
    } else {
      return false;
    }
  },

  set volume(volume) {
    this.inneraudioContext.volume = volume;
  },
  get volume() {
    if (this.inneraudioContext.volume) {
      return this.inneraudioContext.volume;
    } else {
      return 1;
    }
  },

  set muted(muted) {
    this.inneraudioContext.obeyMuteSwitch = muted;
  },

  get notificationVisible() {
    if (this.inneraudioContext.obeyMuteSwitch) {
      return this.inneraudioContext.obeyMuteSwitch;
    } else {
      return true;
    }
  },

  set title(title) {
    console.log('title is not support');
  },

  set artist(artist) {
    console.log('artist is not support');
  },

  set cover(cover) {
    console.log('cover is not support');
  },

  set streamType(streamType) {
    console.log('streamType is not support');
  }
};

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /* storage.set/// */
  scan: function scan(quick_object) {
    if (!quick_object) {
      return null;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    //  const quick_cancel = quick_object.cancel
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.scanCode({
        success: function success(wx_res) {
          var quick_res = {
            result: wx_res.result,
            scanType: wx_res.scanType,
            charSet: wx_res.charSet,
            path: wx_res.path,
            rawData: wx_res.rawData
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  }
}; /* eslint-disable consistent-return */
/* eslint-disable camelcase */

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /*
  battery.getStatus* */
  getStatus: function getStatus(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getBatteryInfo({
        success: function success(wx_res) {
          var quick_res = {
            charging: wx_res.isCharging,
            level: wx_res.level / 100
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  }
}; /* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable no-console */
/* eslint-disable camelcase */
// import PROMISE from '../node_modules/oneutil/PROMISE'

exports.default = {
  /* bluetooth.openAdapter */
  openAdapter: function openAdapter() {
    return console.warn('openAdapter is not support');
  },

  /**
     bluetooth.closeAdapter({
    *
    */

  closeAdapter: function closeAdapter() {
    return console.warn('closeAdapter is not support');
  },

  /** bluetooth.getAdapterState */

  getAdapterState: function getAdapterState() {
    return console.warn('getAdapterState is not support');
  },

  /** bluetooth.onadapterstatechange 监听监听蓝牙适配器状态变化事件 */

  onadapterstatechange: function onadapterstatechange() {
    return console.warn('onadapterstatechange is not support');
  },

  /** bluetooth.startDevicesDiscovery */

  startDevicesDiscovery: function startDevicesDiscovery() {
    return console.warn('startDevicesDiscovery is not support');
  },

  /** bluetooth.stopDevicesDiscovery */

  stopDevicesDiscovery: function stopDevicesDiscovery() {
    return console.warn('stopDevicesDiscovery is not support');
  },

  /** bluetooth.getDevices */

  getDevices: function getDevices() {
    return console.warn('getDevices is not support');
  },

  /** bluetooth.ondevicefound */

  ondevicefound: function ondevicefound() {
    return console.warn('ondevicefound is not support');
  },

  /** bluetooth.getConnectedDevices */

  getConnectedDevices: function getConnectedDevices() {
    return console.warn('getConnectedDevices is not support');
  },

  /** bluetooth.createBLEConnection */

  createBLEConnection: function createBLEConnection() {
    return console.warn('createBLEConnection is not support');
  },

  /** bluetooth.closeBLEConnection */

  closeBLEConnection: function closeBLEConnection() {
    return console.warn('closeBLEConnection is not support');
  },

  /** bluetooth.getBLEDeviceServices */
  getBLEDeviceServices: function getBLEDeviceServices() {
    return console.warn('getBLEDeviceServices is not support');
  },

  /** bluetooth.getBLEDeviceCharacteristics */

  getBLEDeviceCharacteristics: function getBLEDeviceCharacteristics() {
    return console.warn('getBLEDeviceCharacteristics is not support');
  },

  /** bluetooth.readBLECharacteristicValue */

  readBLECharacteristicValue: function readBLECharacteristicValue() {
    return console.warn('readBLECharacteristicValue is not support');
  },

  /** bluetooth.writeBLECharacteristicValue */

  writeBLECharacteristicValue: function writeBLECharacteristicValue() {
    return console.warn('writeBLECharacteristicValue is not support');
  },

  /** bluetooth.notifyBLECharacteristicValueChange */

  notifyBLECharacteristicValueChange: function notifyBLECharacteristicValueChange() {
    return console.warn('notifyBLECharacteristicValueChange is not support');
  },

  /** bluetooth.onblecharacteristicvaluechange */

  onblecharacteristicvaluechange: function onblecharacteristicvaluechange() {
    return console.warn('onblecharacteristicvaluechange is not support');
  },

  /** bluetooth.onbleconnectionstatechange  */

  onbleconnectionstatechange: function onbleconnectionstatechange() {
    return console.warn('onbleconnectionstatechange is not support');
  }
};

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /*
  brightness.setValue
  * */
  setValue: function setValue(quick_object) {
    if (!quick_object) {
      return null;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_value = quick_object.value;
    quick_object = null;
    var wx_object = {
      value: quick_value / 255,
      success: quick_success,
      fail: quick_fail,
      complete: quick_complete
    };
    return wx.setScreenBrightness(wx_object);
  },

  /*
  brightness.getValue
  * */

  getValue: function getValue(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getScreenBrightness({
        success: function success(wx_res) {
          var quick_res = {
            value: wx_res.value * 255
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /**
   * brightness.setKeepScreenOn
   */

  setKeepScreenOn: function setKeepScreenOn(quick_object) {
    return wx.setKeepScreenOn(quick_object);
  },

  /**
     brightness.setMode
     */

  setMode: function setMode() {
    console.log('setMode is not support');
  },

  /**
   brightness.getMode
   */

  getMode: function getMode() {
    console.log('getMode is not support');
  }
}; /* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  insert: function insert(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_title = quick_object.title;
    var quick_startDate = quick_object.startDate;
    var quick_endDate = quick_object.endDate;
    var quick_remindMinutes = quick_object.remindMinutes || [10];
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.addEventOnCalendar({
        title: quick_title,
        startTime: quick_startDate,
        endTime: quick_endDate,
        remindMinutesBefore: quick_remindMinutes[0],
        success: function success(wx_res) {
          var quick_res = {
            eventId: wx_res.eventId,
            errMsg: 'addEventOnCalendar: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  }
}; /* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /*  clipboard.set/// */
  set: function set(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_text = quick_object.text;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.setClipboardData({
        data: quick_text,
        success: function success() {
          var quick_res = {
            errMsg: 'set: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /* clipboard.get/// */

  get: function get(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getClipboardData({
        success: function success(wx_res) {
          var quick_res = {
            text: wx_res.data
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  }
}; /* eslint-disable camelcase */

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable no-console */
exports.default = {
  /* contact.pick */
  pick: function pick() {
    console.log('contact.pick暂不支持');
  },

  /* contact.list */

  list: function list() {
    console.log('contact.list暂不支持');
  }
};

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /** device.getInfo */
  getInfo: function getInfo(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getSystemInfo({
        success: function success(wx_res) {
          var quick_res = {
            brand: wx_res.brand,
            model: wx_res.model,
            osVersionName: wx_res.system.split(' ')[0],
            osVersionCode: wx_res.system.split(' ')[1],
            pixelRatio: wx_res.pixelRatio,
            language: wx_res.language,
            screenWidth: wx_res.screenWidth,
            screenHeight: wx_res.screenHeight,
            windowWidth: wx_res.windowWidth,
            windowHeight: wx_res.windowHeight,
            statusBarHeight: wx_res.statusBarHeight,
            vendorOsVersion: wx_res.version,
            theme: wx_res.theme,
            locationReducedAccuracy: wx_res.locationReducedAccuracy,
            safeArea: wx_res.safeArea,
            wifiEnabled: wx_res.wifiEnabled,
            locationEnabled: wx_res.locationEnabled,
            bluetoothEnabled: wx_res.bluetoothEnabled,
            notificationSoundAuthorized: wx_res.notificationSoundAuthorized,
            notificationBadgeAuthorized: wx_res.notificationBadgeAuthorized,
            notificationAlertAuthorized: wx_res.notificationAlertAuthorized,
            notificationAuthorized: wx_res.notificationAuthorized,
            microphoneAuthorized: wx_res.microphoneAuthorized,
            locationAuthorized: wx_res.locationAuthorized,
            cameraAuthorized: wx_res.cameraAuthorized,
            albumAuthorized: wx_res.albumAuthorized,
            benchmarkLevel: wx_res.benchmarkLevel,
            SDKVersion: wx_res.SDKVersion,
            fontSizeSetting: wx_res.fontSizeSetting,
            platform: wx_res.platform
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  /** device.getId */
  getId: function getId() {
    return console.warn('getId is not support');
  },


  /** device.getDeviceId */
  getDeviceId: function getDeviceId() {
    return console.warn('getDeviceId is not support');
  },


  /** device.getUserId */
  getUserId: function getUserId() {
    return console.warn('getUserId is not support');
  },


  /** device.getAdvertisingId */
  getAdvertisingId: function getAdvertisingId() {
    return console.warn('getAdvertisingId is not support');
  },


  /** device.getSerial */
  getSerial: function getSerial() {
    return console.warn('getSerial is not support');
  },


  /** device.getTotalStorage */
  getTotalStorage: function getTotalStorage() {
    return console.warn('getTotalStorage is not support');
  },


  /** device.getAvailableStorage */
  getAvailableStorage: function getAvailableStorage() {
    return console.warn('getAvailableStorage is not support');
  },


  /** device.getCpuInfo */
  getCpuInfo: function getCpuInfo() {
    return console.warn('getCpuInfo is not support');
  },


  /** device.getOAID */
  getOAID: function getOAID() {
    return console.warn('getOAID is not support');
  },


  /** device.platform */
  platform: function platform() {
    return console.warn('platform is not support');
  },


  /** device.host */
  host: function host() {
    return console.warn('host is not support');
  }
}; /* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.default = {
  getElementById: function getElementById() {}
};

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  fetch: function fetch(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_url = quick_object.url;
    var quick_data = quick_object.data || '';
    var quick_header = quick_object.header || {};
    var quick_method = quick_object.method || 'GET';
    var quick_responseType = quick_object.responseType || 'json';
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.request({
        url: quick_url,
        data: quick_data,
        header: quick_header,
        method: quick_method,
        responseType: quick_responseType,
        success: function success(wx_res) {
          var quick_res = {
            errno: wx_res.errno,
            errmsg: wx_res.errmsg,
            code: wx_res.statusCode,
            data: wx_res.data,
            headers: wx_res.header,
            logid: wx_res.logid
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  }
}; /* eslint-disable camelcase */

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /** file.move */
  move: function move(quick_object) {
    var quick_srcUri = quick_object.srcUri;
    var quick_dstUri = quick_object.dstUri;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    var srcPath = wx.env.USER_DATA_PATH + quick_srcUri.substring(10);
    var destPath = wx.env.USER_DATA_PATH + quick_dstUri.substring(10);
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.copyFile({
        srcPath: srcPath,
        destPath: destPath,
        success: function success() {
          var quick_res = {
            errMsg: 'copyFile: ok'
          };
          SUCCESS(quick_res);
        }
      });
      fileSystemManager.unlink({
        filePath: srcPath,
        success: function success() {
          var quick_res = {
            errMsg: 'unlink: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.copy */

  copy: function copy(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_srcUri = quick_object.srcUri;
    var quick_dstUri = quick_object.dstUri;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    var srcPath = wx.env.USER_DATA_PATH + quick_srcUri.substring(10);
    var destPath = wx.env.USER_DATA_PATH + quick_dstUri.substring(10);
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.copyFile({
        srcPath: srcPath,
        destPath: destPath,
        success: function success() {
          var quick_res = {
            errMsg: 'copyFile: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.list */

  list: function list(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    var dirPath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.readdir({
        dirPath: dirPath,
        success: function success(wx_res) {
          var quick_res = {
            fileList: wx_res.files
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.get */

  get: function get(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    var filePath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.getFileInfo({
        filePath: filePath,
        success: function success(wx_res) {
          var quick_res = {
            length: wx_res.size,
            lastModifiedTime: new Date().getTime(),
            errMsg: 'getFileInfo: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.delete */

  delete: function _delete(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    quick_object = null;
    var filePath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
    var wx_object = {
      filePath: filePath,
      success: quick_success,
      fail: quick_fail,
      complete: quick_complete
    };
    return wx.getFileSystemManager().unlink(wx_object);
  },

  /** file.writeText */

  writeText: function writeText(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    var quick_text = quick_object.text;
    var quick_encoding = quick_object.encoding || 'utf8';
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    var filePath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.writeFile({
        filePath: filePath,
        data: quick_text,
        encoding: quick_encoding,
        success: function success() {
          var quick_res = {
            errMsg: 'writeFile: ok'
          };
          SUCCESS(quick_res);
        },
        fail: function fail(res) {
          console.log(res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.writeArrayBuffer */

  writeArrayBuffer: function writeArrayBuffer(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    var quick_buffer = quick_object.buffer;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    var filePath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.writeFile({
        filePath: filePath,
        data: quick_buffer,
        success: function success() {
          var quick_res = {
            errMsg: 'writeFile: ok'
          };
          SUCCESS(quick_res);
        },
        fail: function fail(res) {
          console.log(res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.readText */

  readText: function readText(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    var quick_encoding = quick_object.encoding || 'utf8';
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    var filePath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.readFile({
        filePath: filePath,
        encoding: quick_encoding,
        success: function success(wx_res) {
          var quick_res = {
            text: wx_res.data
          };
          SUCCESS(quick_res);
        },
        fail: function fail(res) {
          console.log(res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.readArrayBuffer */

  readArrayBuffer: function readArrayBuffer(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    var filePath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.readFile({
        filePath: filePath,
        success: function success(wx_res) {
          var quick_res = {
            buffer: wx_res.data
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.access */

  access: function access(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    (0, _PROMISE2.default)(function (SUCCESS, FAIL) {
      var path = wx.env.USER_DATA_PATH + quick_uri.substring(10);
      fileSystemManager.access({
        path: path,
        success: function success() {
          var quick_res = 'Response { code=0 content=success }';
          SUCCESS(quick_res);
        },
        fail: function fail() {
          var quick_res = 'file does not exists';
          FAIL(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.mkdir */

  mkdir: function mkdir(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    var quick_recursive = quick_object.recursive || false;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    (0, _PROMISE2.default)(function (SUCCESS) {
      var dirPath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
      fileSystemManager.mkdir({
        dirPath: dirPath,
        recursive: quick_recursive,
        success: function success() {
          var quick_res = {
            errMsg: 'mkdir: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** file.rmdir */

  rmdir: function rmdir(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    var quick_recursive = quick_object.recursive || false;
    quick_object = null;
    var fileSystemManager = wx.getFileSystemManager();
    (0, _PROMISE2.default)(function (SUCCESS) {
      var dirPath = wx.env.USER_DATA_PATH + quick_uri.substring(10);
      fileSystemManager.rmdir({
        dirPath: dirPath,
        recursive: quick_recursive,
        success: function success() {
          var quick_res = {
            errMsg: 'rmdir: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  }
}; /* eslint-disable no-console */
/* eslint-disable consistent-return */
/* eslint-disable camelcase */
/* eslint-disable no-undef */

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  getLocation: function getLocation(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_timeout = quick_object.timeout || 3000;
    var quick_coordType = quick_object.coordType || 'wgs84';
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getLocation({
        type: quick_coordType,
        highAccuracyExpireTime: quick_timeout,
        success: function success(wx_res) {
          var quick_res = {
            latitude: wx_res.latitude,
            longitude: wx_res.longitude,
            speed: wx_res.speed,
            accuracy: wx_res.accuracy,
            altitude: wx_res.altitude,
            verticalAccuracy: wx_res.verticalAccuracy,
            horizontalAccuracy: wx_res.horizontalAccuracy,
            time: new Date().getTime()
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** openLocation */
  openLocation: function openLocation(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_latitude = quick_object.latitude;
    var quick_longitude = quick_object.longitude;
    var quick_scale = quick_object.scale || 18;
    var quick_name = quick_object.name || '';
    var quick_address = quick_object.address || '';
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.openLocation({
        latitude: quick_latitude,
        longitude: quick_longitude,
        scale: quick_scale,
        name: quick_name,
        address: quick_address,
        success: function success() {
          var quick_res = {
            errMsg: 'openLocation: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** chooseLocation */

  chooseLocation: function chooseLocation(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_latitude = quick_object.latitude;
    var quick_longitude = quick_object.longitude;
    var quick_coordType = quick_object.coordType || 'wgs84';
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      if (!quick_latitude || !quick_longitude) {
        wx.getLocation({
          type: quick_coordType,
          success: function success(wx_res) {
            var quick_res = {
              name: wx_res.street,
              address: wx_res.street,
              latitude: wx_res.latitude,
              longitude: wx_res.longitude,
              coordType: quick_coordType
            };
            SUCCESS(quick_res);
          }
        });
      } else {
        wx.chooseLocation({
          success: function success(wx_res) {
            var quick_res = {
              name: wx_res.name,
              address: wx_res.address,
              latitude: wx_res.latitude,
              longitude: wx_res.longitude,
              coordType: quick_coordType
            };
            SUCCESS(quick_res);
          }
        });
      }
    }, quick_success, quick_fail, quick_complete);
  },

  /** getLocationType */

  getLocationType: function getLocationType(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_res = {
      types: ['gps', 'network']
    };
    quick_success(quick_res);
  },

  /** geolocation.subscribe */

  subscribe: function subscribe(quick_object) {
    if (!quick_object) {
      return;
    }
    wx.startLocationUpdate();
    var quick_callback = quick_object.callback;
    quick_object = null;
    wx.onLocationChange(function (wx_res) {
      var quick_res = {
        latitude: wx_res.latitude,
        longitude: wx_res.longitude,
        speed: wx_res.speed,
        accuracy: wx_res.accuracy,
        altitude: wx_res.altitude,
        verticalAccuracy: wx_res.verticalAccuracy,
        horizontalAccuracy: wx_res.horizontalAccuracy,
        time: new Date().getTime()
      };
      quick_callback(quick_res);
    });
  },

  /** wx.offLocationChange */
  unsubscribe: function unsubscribe() {
    wx.offLocationChange();
  },

  /** geolocation.getSupportedCoordTypes() */
  getSupportedCoordTypes: function getSupportedCoordTypes() {
    console.log('getSupportedCoordTypes is not support');
  },


  /** geolocation.geocodeQuery() */
  geocodeQuery: function geocodeQuery() {
    console.log('geocodeQuery is not support');
  },


  /** geolocation.reverseGeocodeQuery() */
  reverseGeocodeQuery: function reverseGeocodeQuery() {
    console.log('reverseGeocodeQuery is not support');
  }
}; /* eslint-disable consistent-return */
/* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {

  // 获取图片信息
  getImageInfo: function getImageInfo(quick_object) {
    var quick_uri = quick_object.uri;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getImageInfo({
        src: quick_uri,
        success: function success(wx_res) {
          var quick_res = {
            uri: wx_res.path,
            width: wx_res.width,
            height: wx_res.height,
            orientation: wx_res.orientation,
            type: wx_res.type
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },
  compressImage: function compressImage(quick_object) {
    var quick_uri = quick_object.uri;
    var quick_quality = quick_object.quality || 75;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.compressImage({
        src: quick_uri,
        quality: quick_quality,
        success: function success(wx_res) {
          var quick_res = {
            uri: wx_res.tempFilePath
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },
  applyOperations: function applyOperations() {
    console.warn('applyOperations is not support');
  },
  editImage: function editImage() {
    console.warn('editImage is not support');
  },
  getExifAttributes: function getExifAttributes() {
    console.warn('getExifAttributes is not support');
  },
  setExifAttributes: function setExifAttributes() {
    console.warn('setExifAttributes is not support');
  }
}; /* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /** media.takePhoto */

  takePhoto: function takePhoto(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    // const quick_cancel = quick_object.cancel
    var CameraContext = wx.createCameraContext();
    (0, _PROMISE2.default)(function (SUCCESS) {
      CameraContext.takePhoto({
        success: function success(wx_res) {
          var quick_res = {
            uri: wx_res.tempImagePath
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** media.takeVideo */
  takeVideo: function takeVideo(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    // const quick_cancel = quick_object.cancel
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.chooseVideo({
        sourceType: ['album'],
        success: function success(wx_res) {
          var quick_res = {
            uri: wx_res.tempFilePath,
            size: wx_res.size,
            duration: wx_res.duration,
            height: wx_res.height,
            width: wx_res.width
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  /** media.pickImage */
  pickImage: function pickImage(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    // const quick_cancel = quick_object.cancel
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.chooseImage({
        count: 1,
        success: function success(wx_res) {
          var quick_res = {
            tempFilePaths: wx_res.tempFilePaths,
            uri: wx_res.tempFiles[0].path,
            size: wx_res.tempFiles[0].size
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  /** media.pickImages */
  pickImages: function pickImages(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    // const quick_cancel = quick_object.cancel
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.chooseImage({
        success: function success(wx_res) {
          var quick_files = wx_res.tempFiles.map(function (file) {
            return {
              uri: file.path,
              size: file.size
            };
          });
          var quick_res = {
            uris: wx_res.tempFilePaths,
            files: quick_files
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  /** media.pickVideo */
  pickVideo: function pickVideo(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    // const quick_cancel = quick_object.cancel
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.chooseVideo({
        sourceType: ['camera'],
        success: function success(wx_res) {
          var quick_res = {
            uri: wx_res.tempFilePath,
            size: wx_res.size,
            duration: wx_res.duration,
            height: wx_res.height,
            width: wx_res.width
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  /** media.pickVideos */
  pickVideos: function pickVideos(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    // const quick_cancel = quick_object.cancel
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.chooseVideo({
        sourceType: ['album'],
        success: function success(wx_res) {
          var quick_res = {
            uris: [wx_res.tempFilePath],
            files: [{
              size: wx_res.size,
              uri: wx_res.tempFilePath
            }],
            duration: wx_res.duration,
            height: wx_res.height,
            width: wx_res.width
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  /** media.pickFile */
  pickFile: function pickFile(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    // const quick_cancel = quick_object.cancel
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.chooseMessageFile({
        count: 1,
        success: function success(wx_res) {
          var quick_res = {
            tempFiles: wx_res.tempFiles,
            uri: wx_res.tempFiles[0].path,
            size: wx_res.tempFiles[0].size,
            name: wx_res.tempFiles[0].name
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  /** media.saveToPhotosAlbum */
  saveToPhotosAlbum: function saveToPhotosAlbum(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uri = quick_object.uri;
    quick_object = null;
    var wx_object = {
      filePath: quick_uri,
      success: quick_success,
      fail: quick_fail,
      complete: quick_complete
    };
    wx.saveImageToPhotosAlbum(wx_object);
  },


  /** media.previewImage */
  previewImage: function previewImage(quick_object) {
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_uris = quick_object.uris;
    var quick_current = quick_object.current || quick_uris[0];
    // const quick_cancel = quick_object.cancel
    quick_object = null;
    var wx_object = {
      urls: quick_uris,
      current: quick_current,
      success: quick_success,
      fail: quick_fail,
      complete: quick_complete
    };
    wx.previewImage(wx_object);
  },

  /** media.getRingtone */
  getRingtone: function getRingtone() {
    console.warn('getRingtone is not support');
  },

  /** setRingtone */
  setRingtone: function setRingtone() {
    console.warn('setRingtone is not support');
  }
}; /* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /*  network.getType/// */
  getType: function getType(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getNetworkType({
        success: function success(wx_res) {
          var quick_res_type = void 0;
          switch (wx_res.networkType) {
            case 'unknown':
              quick_res_type = 'others';
              break;
            default:
              quick_res_type = wx_res.networkType;
              break;
          }
          var quick_res = {
            type: quick_res_type,
            metered: false
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  /*  network.subscribe/// */
  subscribe: function subscribe(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_callback = quick_object.callback;
    quick_object = null;
    getApp().onekit_NetworkStatusChange = true;
    if (getApp().onekit_NetworkStatusChange) {
      wx.onNetworkStatusChange(function (wx_res) {
        var quick_res_type = void 0;
        switch (wx_res.networkType) {
          case 'unknown':
            quick_res_type = 'others';
            break;
          default:
            quick_res_type = wx_res.networkType;
            break;
        }
        var quick_res = {
          type: quick_res_type,
          metered: false,
          isConnected: wx_res.isConnected
        };
        quick_callback(quick_res);
      });
    }
  },

  // ///////
  unsubscribe: function unsubscribe() {
    getApp().onekit_NetworkStatusChange = false;
  },

  /** getSimOperator */
  getSimOperator: function getSimOperator() {
    console.log('getSimOperator is not support');
  }
}; /* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  showToast: function showToast(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_message = quick_object.message;
    var quick_duration = quick_object.duration || 0;
    var wx_duration = void 0;
    if (quick_duration === 0) {
      wx_duration = 1500;
    } else {
      wx_duration = 3000;
    }
    var wx_object = {
      title: quick_message,
      duration: wx_duration
    };
    wx.showToast(wx_object);
  },
  showDialog: function showDialog(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_title = quick_object.title || '';
    var quick_message = quick_object.message || '';
    var quick_buttons = quick_object.buttons;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    if (quick_buttons.length === 1) {
      var confirmText = quick_buttons[0].text;
      var confirmColor = quick_buttons[0].color;
      (0, _PROMISE2.default)(function (SUCCESS) {
        wx.showModal({
          title: quick_title,
          content: quick_message,
          confirmText: confirmText,
          confirmColor: confirmColor,
          success: function success(wx_res) {
            var quick_res = void 0;
            if (wx_res.confirm) {
              quick_res = {
                index: 0
              };
            } else {
              quick_res = {
                index: 1
              };
            }
            SUCCESS(quick_res);
          }
        });
      }, quick_success, quick_fail, quick_complete);
    }
    if (quick_buttons.length === 2 || quick_buttons.length === 3) {
      var cancelText = quick_buttons[0].text;
      var cancelColor = quick_buttons[0].color;
      var _confirmText = quick_buttons[1].text;
      var _confirmColor = quick_buttons[1].color;
      (0, _PROMISE2.default)(function (SUCCESS) {
        wx.showModal({
          title: quick_title,
          content: quick_message,
          cancelText: cancelText,
          cancelColor: cancelColor,
          confirmText: _confirmText,
          confirmColor: _confirmColor,
          success: function success(wx_res) {
            var quick_res = void 0;
            if (wx_res.confirm) {
              quick_res = {
                index: 0
              };
            } else {
              quick_res = {
                index: 1
              };
            }
            SUCCESS(quick_res);
          }
        });
      }, quick_success, quick_fail, quick_complete);
    }
  },
  showContextMenu: function showContextMenu(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_itemList = quick_object.itemList;
    var quick_itemColor = quick_object.itemColor || '#000000';
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.showActionSheet({
        itemList: quick_itemList,
        itemColor: quick_itemColor,
        success: function success(wx_res) {
          var quick_res = {
            index: wx_res.tapIndex
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  }
}; /* eslint-disable block-scoped-var */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable no-console */
/* eslint-disable camelcase */

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable camelcase */
exports.default = {
  /** * record.start */
  start: function start(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_duration = quick_object.duration || 60000;
    var quick_sampleRate = quick_object.sampleRate || 8000;
    var quick_numberOfChannels = quick_object.numberOfChannels || 2;
    var quick_encodeBitRate = quick_object.encodeBitRate || 48000;
    var quick_format = quick_object.format || 'aac';
    quick_object = null;
    var wx_object = {
      duration: quick_duration,
      sampleRate: quick_sampleRate,
      numberOfChannels: quick_numberOfChannels,
      encodeBitRate: quick_encodeBitRate,
      format: quick_format,
      success: quick_success,
      fail: quick_fail,
      complete: quick_complete
    };
    var recorderManager = wx.getRecorderManager();
    this.recorderManager = recorderManager;
    recorderManager.start(wx_object);
  },

  /** record.stop */
  stop: function stop() {
    this.recorderManager.stop();
  }
};

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

var _TASK = __webpack_require__(38);

var _TASK2 = _interopRequireDefault(_TASK);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable no-console */
/* eslint-disable camelcase */
exports.default = {
  /** request.upload */
  upload: function upload(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_url = quick_object.url;
    var quick_header = quick_object.header || {};
    var quick_files = quick_object.files;
    var quick_data = quick_object.data || [];
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      (0, _TASK2.default)(quick_files, function (quick_file, callback) {
        var filePath = quick_file.uri;
        var name = quick_file.name;
        wx.uploadFile({
          url: quick_url,
          name: name,
          filePath: filePath,
          header: quick_header,
          formData: quick_data[0],
          success: function success(wx_res) {
            var quick_res = {
              code: wx_res.statusCode,
              data: wx_res.data
            };
            callback(quick_res);
          }

        });
      }, function (quick_res) {
        SUCCESS(quick_res);
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /** request.download */

  download: function download(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    var quick_url = quick_object.url;
    var filename = quick_object.filename || quick_url.substring(quick_url.lastIndexOf('/') + 1);
    var filePath = wx.env.USER_DATA_PATH + '/' + filename;
    quick_object = null;
    var wx_object = {
      url: quick_url,
      filePath: filePath
    };
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.downloadFile({
        url: quick_url,
        filePath: filePath,
        success: function success(wx_res) {
          var token = '' + new Date().getTime();
          var quick_res = {
            filePath: wx_res.filePath,
            statusCode: wx_res.statusCode,
            token: token
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
    getApp().onekit_DownloadTask = wx.downloadFile(wx_object);
    getApp().onekit_download_url = quick_url;
  },

  /** onDownloadComplete */

  onDownloadComplete: function onDownloadComplete(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    if (getApp().onekit_DownloadTask) {
      var DownloadTask = getApp().onekit_DownloadTask;
      DownloadTask.onProgressUpdate(function (wx_res) {
        if (wx_res.progress === '100') {
          quick_success({
            uri: getApp().onekit_download_url
          });
        }
      });
    }
  }
};

/***/ }),
/* 38 */
/***/ (function(module, exports) {


module.exports = function (items, func, success) {
  var result = []
  var i = 0
  var itemCallback = null
  itemCallback = function (res) {
    result.push(res)
    if (i >= items.length) {
      success(result)
      return
    }
    func(items[i++], itemCallback)
  }
  func(items[i++], itemCallback)
}


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _oneutil = __webpack_require__(1);

exports.default = {
  /*  network.getType/// */
  push: function push(quick_object) {
    if (!quick_object) {
      return;
    }
    function path2url(path) {
      var paths = getApp().onekit.router.pages;
      for (var _iterator = Object.keys(paths), _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
        var _ref;

        if (_isArray) {
          if (_i >= _iterator.length) break;
          _ref = _iterator[_i++];
        } else {
          _i = _iterator.next();
          if (_i.done) break;
          _ref = _i.value;
        }

        var name = _ref;

        var page = paths[name];
        if (page.path === path || name === path) {
          return '/' + name + '/' + page.component;
        }
      }
      throw new Error('[path2url]', path);
    }
    var quick_uri = quick_object.uri;
    //    const quick_params = quick_object.params
    if (quick_uri.startsWith('/')) {
      if (quick_uri === '/') {
        wx.reLaunch({
          // url: `/quickapp2baidu/page/router.push/ie?url=${encodeURI(quick_uri)}`
        });
      } else {
        wx.navigateTo({
          url: path2url(quick_uri) // + '/index'
        });
      }
      return;
    }

    var url = _oneutil.URL.parse(quick_uri);
    if (!url.host || !url.scheme) {
      wx.navigateTo({
        url: path2url(quick_uri) // + '/index'
      });
      return;
    }
    switch (url.scheme) {
      case 'tel':
        wx.makePhoneCall({
          phoneNumber: url.host
        });
        break;
      case 'sms':
        wx.showModal({
          title: '不支持',
          content: '微信小程序暂不支持发短信'
        });
        break;
      case 'http':
      case 'https':
        wx.navigateTo({
          url: '/quickapp2baidu/page/router.push/ie?url=' + encodeURI(quick_uri)
        });
        break;
      case 'internal':
        wx.showModal({
          title: '带配置',
          content: '微信小程序暂不支持打开文件'
        });
        break;
      case 'hap':
        switch (url.host) {
          case 'app':
            wx.showModal({
              title: '带配置',
              content: '请配置要打开的小程序'
            });
            break;
          case 'settings':
            wx.showModal({
              title: '带配置',
              content: '微信小程序暂不支持打开手机设置'
            });
            break;
          default:
            throw new Error(url.host);
        }

        break;
      default:
        throw new Error(url.scheme);
    }
  },


  /** router.replace */

  replace: function replace(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_uri = quick_object.uri;
    if (quick_uri.startsWith('/')) {
      if (quick_uri === '/') {
        wx.reLaunch({
          url: '/quickapp2baidu/page/router.push/ie?url=' + encodeURI(quick_uri)
        });
      } else {
        wx.navigateTo({
          url: quick_uri
        });
      }
    }
  },

  /** router.back */

  back: function back(quick_object) {
    var quick_path = quick_object.path || '';
    if (!quick_path) {
      wx.navigateBack({});
    } else if (quick_path.startsWith('/')) {
      if (quick_path === '/') {
        wx.navigateTo({
          url: '/quickapp2baidu/page/router.push/ie?url=' + encodeURI(quick_path)
        });
      } else {
        wx.reLaunch({
          url: quick_path
        });
      }
    }
  },

  /** router.clear() */

  clear: function clear() {
    //  const wx_object = {}
  },

  /** router.getLength */

  getLength: function getLength() {
    //  const wx_object = {}
  },

  /** router.getState */

  getState: function getState() {
    //  const wx_object = {}
  },

  /** router.getPages */

  getPages: function getPages() {
    //  const wx_object = {}
  }
}; /* eslint-disable no-console */
/* eslint-disable camelcase */
/* eslint-disable object-curly-spacing */

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable no-console */
/* eslint-disable camelcase */
exports.default = {
  subscribeAccelerometer: function subscribeAccelerometer(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_interval = quick_object.interval || 'normal';
    var quick_callback = quick_object.callback;
    var wx_object = {
      interval: quick_interval
    };
    wx.startAccelerometer(wx_object);
    wx.onAccelerometerChange(function (wx_res) {
      var quick_res = {
        x: wx_res.x,
        y: wx_res.y,
        z: wx_res.z
      };
      quick_callback(quick_res);
    });
  },


  /** unsubscribeAccelerometer */
  unsubscribeAccelerometer: function unsubscribeAccelerometer() {
    return wx.offAccelerometerChange();
  },

  /** sensor.subscribeCompass */
  subscribeCompass: function subscribeCompass(quick_object) {
    if (!quick_object) {
      return;
    }
    wx.startCompass();
    var quick_callback = quick_object.callback;
    wx.onCompassChange(function (res) {
      quick_callback({
        direction: res.direction,
        accuracy: res.accuracy
      });
    });
  },


  /** sensor.unsubscribeCompass() */
  unsubscribeCompass: function unsubscribeCompass() {
    return wx.offCompassChange();
  },


  /** sensor.subscribeProximity */
  subscribeProximity: function subscribeProximity() {
    return console.warn('subscribeProximity is not support');
  },

  /** sensor.unsubscribeProximity() */
  unsubscribeProximity: function unsubscribeProximity() {
    return console.warn('unsubscribeProximity is not support');
  },


  /** sensor.subscribeLight */
  subscribeLight: function subscribeLight() {
    return console.warn('subscribeLight is not support');
  },


  /** sensor.unsubscribeLight() */
  unsubscribeLight: function unsubscribeLight() {
    return console.warn('unsubscribeLight is not support');
  },


  /** sensor.subscribeStepCounter */
  subscribeStepCounter: function subscribeStepCounter(quick_object) {
    if (!quick_object) {
      return;
    }
    wx.startAccelerometer({
      interval: 'game'
    });
    var quick_callback = quick_object.callback;

    function check(a1, a2) {
      return a1 > 0 && a2 < 0 || a1 < 0 && a2 > 0;
    }
    var steps = 0;

    function add() {
      steps++;
      quick_callback({
        steps: steps
      });
    }
    var x = void 0;
    var y = void 0;
    var z = void 0;
    wx.onAccelerometerChange(function (res) {
      if (x && check(x, res.x)) {
        add();
        x = 0;
      } else if (y && check(y, res.y)) {
        add();
        y = 0;
      } else if (z && check(z, res.z)) {
        add();
        z = 0;
      }
      x = res.x;

      y = res.y;

      z = res.z;
    });
  },

  // ////////////////////////////
  /** sensor.unsubscribeStepCounter() */
  unsubscribeStepCounter: function unsubscribeStepCounter() {
    return wx.offCompassChange();
  }
};

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable camelcase */
/* eslint-disable no-console */
exports.default = {
  share: function share(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_type = quick_object.type;
    var quick_data = quick_object.data;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    // ////////////////////////////////////////////
    var wx_object = {};
    if (quick_type.indexOf('text/') === 0) {
      console.log('[quick2baidu] not support share text');
    } else if (quick_type.indexOf('/') === 0) {
      // 应用内路径
      wx_object.path = quick_data;
    } else {
      // 下载的文件路径

    }

    var wx_callback = {};
    if (quick_complete) {
      wx_callback.complete = quick_complete;
    }
    if (quick_success) {
      wx_callback.success = quick_success;
    }
    if (quick_fail) {
      wx_callback.fail = quick_fail;
    }
    if (quick_object.cancel) {
      wx_callback.cancel = quick_object.cancel;
    }

    wx.navigateTo({
      url: '/onekit/page/share.share/share.share?param=' + JSON.stringify(wx_object),
      events: wx_callback
    });
  }
};

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  /* storage.set/// */
  set: function set(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_key = quick_object.key;
    var quick_value = quick_object.value;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      if (quick_value != null) {
        if (quick_value.length === 0) {
          wx.clearStorage({
            success: function success() {
              var quick_res = {
                errMsg: 'clearStorage: ok'
              };
              SUCCESS(quick_res);
            }
          });
        } else {
          wx.setStorage({
            key: quick_key,
            data: quick_value,
            success: function success() {
              var quick_res = {
                errMsg: 'setStorage: ok'
              };
              SUCCESS(quick_res);
            }
          });
        }
      } else {
        wx.clearStorage({
          success: function success() {
            var quick_res = {
              errMsg: 'clearStorage: ok'
            };
            SUCCESS(quick_res);
          }
        });
      }
    }, quick_success, quick_fail, quick_complete);
  },

  /* storage.get */

  get: function get(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_key = quick_object.key;
    var quick_default = quick_object.default || '';
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getStorage({
        key: quick_key,
        success: function success(wx_res) {
          var quick_res = wx_res.data;
          SUCCESS(quick_res);
        },
        fail: function fail() {
          SUCCESS(quick_default);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /* Storage.clear */

  clear: function clear(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.clearStorage({
        success: function success() {
          var quick_res = {};
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /* Storage.delete */

  delete: function _delete(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_key = quick_object.key;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.removeStorage({
        key: quick_key,
        success: function success() {
          var quick_res = {};
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },

  /* storage.key */

  key: function key(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_index = quick_object.index;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    (0, _PROMISE2.default)(function (SUCCESS) {
      wx.getStorageInfo({
        success: function success(wx_res) {
          var quick_res = wx_res.keys[quick_index];
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  },


  get length() {
    var wx_res = wx.getStorageInfoSync();
    return wx_res.keys.length;
  }
}; /* eslint-disable consistent-return */
/* eslint-disable getter-return */
/* eslint-disable camelcase */

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable camelcase */
/* eslint-disable consistent-return */

exports.default = {
  vibrate: function vibrate(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_mode = quick_object.mode || 'long';
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    var wx_object = {
      type: 'medium',
      success: quick_success,
      fail: quick_fail,
      complete: quick_complete
    };
    if (quick_mode === 'short') {
      return wx.vibrateShort(wx_object);
    } else {
      return wx.vibrateLong(wx_object);
    }
  }
};

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _WebSocket = __webpack_require__(45);

var _WebSocket2 = _interopRequireDefault(_WebSocket);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  create: function create(quick_object) {
    if (!quick_object) {
      return null;
    }
    // const url = quick_object.url
    // const header = quick_object.header
    // const protocols = quick_object.protocols
    // // /////////////////////////////////////////
    // const DATA = ['HTTPS', 'HTTP']
    // const wx_object = {
    //   url,
    //   header,
    //   protocols: []
    // }
    // for (const protocol of protocols) {
    //   if (DATA.indexOf(protocol.toLowerCase()) >= 0) {
    //     wx_object.protocols.push(protocol)
    //   }
    // }
    var socket = wx.connectSocket(quick_object);
    return new _WebSocket2.default(socket);
  }
}; /* eslint-disable camelcase */

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* eslint-disable camelcase */
var WebSocket = function () {
  function WebSocket(socket) {
    _classCallCheck(this, WebSocket);

    this.socket = socket;
  }

  WebSocket.prototype.close = function close(quick_object) {
    this.socket.close(quick_object);
  };

  WebSocket.prototype.send = function send(quick_object) {
    this.socket.send(quick_object);
  };

  WebSocket.prototype.onopen = function onopen(callback) {
    this.socket.onOpen(callback);
  };

  WebSocket.prototype.onmessage = function onmessage(callback) {
    this.socket.onMessage(callback);
  };

  WebSocket.prototype.onclose = function onclose(callback) {
    this.socket.onClose(function (wx_res) {
      var quick_res = {
        code: wx_res.code,
        reason: wx_res.reason,
        wasClean: 'normal closure'
      };
      callback(quick_res);
    });
  };

  WebSocket.prototype.onerror = function onerror(callback) {
    this.socket.onError(callback);
  };

  return WebSocket;
}();

exports.default = WebSocket;

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable camelcase */

exports.default = {
  /** webview.loadUrl */
  loadUrl: function loadUrl(quick_object) {
    var quick_url = quick_object.url;
    quick_object = null;
    wx.navigateTo({
      url: "/quickapp2baidu/page/router.push/ie?url=" + encodeURI(quick_url)
    });
  }
};

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
/* eslint-disable consistent-return */
/* eslint-disable no-console */
/* eslint-disable camelcase */
// import PROMISE from '../node_modules/oneutil/PROMISE'

exports.default = {
  /** wifi.connect */
  connect: function connect(quick_object) {
    // return console.warn('connect is not support')
    return wx.connectWifi(quick_object);
  },

  /**
   * wifi.scan
   */
  scan: function scan(quick_object) {
    // return console.warn('scan is not support')
    return wx.getWifiList(quick_object);
  },

  /**
   * wifi.getConnectedWifi */

  getConnectedWifi: function getConnectedWifi(quick_object) {
    // return console.warn('getConnectedWifi is not support')
    return wx.getConnectedWifi(quick_object);
  },

  /**
   * wifi.onscanned */
  onscanned: function onscanned(quick_object) {
    // return console.warn('onscanned is not support')
    return wx.onGetWifiList(quick_object);
  },

  /** wifi.onstatechanged */
  onstatechanged: function onstatechanged(quick_object) {
    // return console.warn('onstatechanged is not support')
    return wx.onWifiConnected(quick_object);
  }
};

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _PROMISE = __webpack_require__(0);

var _PROMISE2 = _interopRequireDefault(_PROMISE);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  decompress: function decompress(quick_object) {
    if (!quick_object) {
      return;
    }
    var quick_srcUri = quick_object.srcUri;
    var quick_dstUri = quick_object.dstUri;
    var quick_success = quick_object.success;
    var quick_fail = quick_object.fail;
    var quick_complete = quick_object.complete;
    quick_object = null;
    var path = wx.env.USER_DATA_PATH;
    var fileSystemManager = wx.getFileSystemManager();
    (0, _PROMISE2.default)(function (SUCCESS) {
      fileSystemManager.unzip({
        zipFilePath: quick_srcUri.indexOf('internal://') === 0 ? path + quick_srcUri.substring(10) : quick_srcUri,
        targetPath: quick_dstUri.indexOf('internal://') === 0 ? path + quick_dstUri.substring(10) : quick_dstUri,
        success: function success() {
          var quick_res = {
            errMsg: 'decompress: ok'
          };
          SUCCESS(quick_res);
        }
      });
    }, quick_success, quick_fail, quick_complete);
  }
}; /* eslint-disable camelcase */
/* eslint-disable no-console */

/***/ })
/******/ ]);