import {Page,Component} from '../quickapp2weixin/index.js';
import {system_router} from '../quickapp2weixin/index';const router = system_router;
import {tabsData} from './data';
Page({
onekit_routePath(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.routepath0);args.push(dataset.routepath1);this.routePath.apply(this,args);},
onekit_selectConFn(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.selectconfn0);args.push(dataset.selectconfn1);this.selectConFn.apply(this,args);},
    "private":{
        "dataMap":tabsData,
        "tabItemList":[
        ],
        "selectedIdxMap":{
}
},
    "onInit"(){
      this.setData({tabItemList:[
    ].concat(Object.keys(this.data.dataMap))});
      this.data.tabItemList.forEach((tabItem)=>{this.$set(`selectedIdxMap.${tabItem}`,-1)});
    },
    "selectConFn"(tabItem,index){
      this.setData({[`selectedIdxMap.${tabItem}`]:(this.data.selectedIdxMap[tabItem] == index)?-1:index});
    },
    "routePath"(path,params){
      router.push({
        "uri":path,
        params
});
    }
});

