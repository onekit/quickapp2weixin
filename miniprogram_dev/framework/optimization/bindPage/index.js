import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_router} from '../../../quickapp2weixin/index';const router = system_router;
import {fetchData} from './interface';
Page({
    "private":{
        "person":{
            "name":'张三'
},
        "componentName":'回调函数'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "execCallback"(){
      fetchData(function(){
      console.info(`获取person.name: ${this.data.person.name}`);
    }.bindPage(this));
      router.back();
    }
});

