import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "className":'text-blue',
        "textColor":'#0faeff',
        "styleObj":{
            "color":'red'
},
        "styleText":'color: blue',
        "componentName":'动态修改样式'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "changeClassName"(){
      this.setData({className:'text-red'});
    },
    "changeInlineStyle"(){
      this.setData({textColor:'#f76160'});
    },
    "changeStyleObj"(){
      this.setData({styleObj:{
        "color":'yellow'
}});
    },
    "changeStyleText"(){
      this.setData({styleText:'color: green'});
    }
});

