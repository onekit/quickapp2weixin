import {Page,Component} from '../../../quickapp2weixin/index.js';
let offsetNumber;
Page({
onekit_getSliderValue(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.getslidervalue0);this.getSliderValue.apply(this,args);},
onekit_getCheckValue(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.getcheckvalue0);this.getCheckValue.apply(this,args);},
    "private":{
        "top":0,
        "left":0,
        "right":0,
        "bottom":0,
        "width":100,
        "height":100,
        "position":'relative',
        "offsetPrefix":'margin-',
        "topCheckFlag":false,
        "leftCheckFlag":false,
        "rightCheckFlag":false,
        "bottomCheckFlag":false
},
    "computed":{
        "getStyleText"(){
          let {top,left,right,bottom,"position":position = 'relative',topCheckFlag,leftCheckFlag,rightCheckFlag,bottomCheckFlag,"width":width = 100,"height":height = 100} = this;
          let styleText = `position:${position};`;
          let addStyleOffset = (offsetDirection,offsetNumber,positionAttribute,enableFlag = false)=>{
          if(!offsetDirection)
            return;
          const relativeFlag = positionAttribute == 'relative';
          let newStringTextPrefix = relativeFlag?'margin-':'';
          let oppositeStringTextPrefix = relativeFlag?'':'margin-';
          styleText += `${oppositeStringTextPrefix}${offsetDirection}:0;`;
          if(!enableFlag)
            {
              offsetNumber = 0;
            }
else
            {
              offsetNumber += 'px';
            };
          styleText += `${newStringTextPrefix}${offsetDirection}:${offsetNumber};`;
        };
          let addStyleLength = (lengthName,lengthNumber)=>{styleText += `${lengthName}:${lengthNumber}px;`};
          addStyleOffset('top',top,position,topCheckFlag);
          addStyleOffset('left',left,position,leftCheckFlag);
          addStyleOffset('right',right,position,rightCheckFlag);
          addStyleOffset('bottom',bottom,position,bottomCheckFlag);
          addStyleLength('width',width);
          addStyleLength('height',height);
          return styleText;
        }
},
    "onSelectChanged"(e){
      let newPosition = e.newValue;
      this.setData({position:newPosition});
      this.setData({offsetPrefix:(newPosition == 'relative')?'margin-':''});
    },
    "getSliderValue"(name,e){
      this.setData({name:e.progress});
    },
    "getCheckValue"(name,e){
      this.setData({name:e.checked});
    },
    "resetState"(){
      this.setData({top:0});
      this.setData({left:0});
      this.setData({right:0});
      this.setData({bottom:0});
      this.setData({width:100});
      this.setData({height:100});
      this.setData({topCheckFlag:false});
      this.setData({leftCheckFlag:false});
      this.setData({rightCheckFlag:false});
      this.setData({bottomCheckFlag:false});
    }
});

