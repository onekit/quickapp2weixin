import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'flex 布局示例',
        "flex_direction":'row',
        "justify_content":'flex-start',
        "align_items":'flex-start'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "changeFlexDirection"(e){
      let newValue = e.newValue;
      this.setData({flex_direction:e.newValue});
    },
    "changeJustifyContainer"(e){
      let newValue = e.newValue;
      this.setData({justify_content:newValue});
    },
    "changeAlignItems"(e){
      let newValue = e.newValue;
      this.setData({align_items:newValue});
    }
});

