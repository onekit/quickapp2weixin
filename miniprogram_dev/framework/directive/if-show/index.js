import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "showVar":true,
        "value":1,
        "componentName":'指令 if 和指令 show'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "onClickShow"(){
      this.setData({showVar:!this.data.showVar});
    },
    "onClickCondition"(){
      this.setData({value:++this.data.value % 3});
    }
});

