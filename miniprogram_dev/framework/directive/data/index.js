import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'指令 data'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "onReady"(){
      const el = this.$element('elNode1');
      const elData = el.dataset.personName;
      prompt.showToast({
        "message":`输出data属性 : ${elData}`
});
    }
});

