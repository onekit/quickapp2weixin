import {Page,Component} from '../../../quickapp2weixin/index.js';
import {dataDirective} from '../../../common/js/data';
Page({
    "private":{
        "showCityList":1,
        "cities":dataDirective,
        "componentName":'组件 block'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "toggleCityList"(){
      this.setData({showCityList:(this.data.showCityList == 1)?0:1});
    }
});

