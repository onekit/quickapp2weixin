import {Page,Component} from '../../quickapp2weixin/index.js';
import {system_fetch} from '../../quickapp2weixin/index';const fetch = system_fetch;
import {system_prompt} from '../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'支持AsyncAwait'
});
    },
    "onReady"(){
      this.testAsync();
    },
    "testAsync"(){
      async function bar(){
      return '此时已支持async await';
    };
      async function foo(){
      const ret1 = await bar();
      prompt.showToast({
          "message":ret1
});
    };
      foo();
    }
});

