import {Page,Component} from '../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'Screen Orientation',
        "orientation":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Screen Orientation'
});
      this.setData({orientation:(this.$page.orientation == 'portrait')?'竖屏':'横屏'});
    }
});

