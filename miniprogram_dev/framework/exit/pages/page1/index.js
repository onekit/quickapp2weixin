import {Page,Component} from '../../../../quickapp2weixin/index.js';
import {system_router} from '../../../../quickapp2weixin/index';const router = system_router;
Page({
onekit_go(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.go0);this.go.apply(this,args);},
    "private":{
        "componentName":'onHide时，我会把自己关掉',
        "isgo":false
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "onShow"(){
      this.setData({isgo:false});
    },
    "onHide"(){
      console.log('第一页onHide触发');
      if(this.data.isgo == false)
      {
        console.log('第一页退出应用');
        this.data.$app.exit();
      }
else
      {
        this.setData({isgo:false});
        this.$page.finish();
      };
    },
    "onBackPress"(){
      console.log('第一页onbackpress');
      this.setData({isgo:true});
      return false;
    },
    "onDestroy"(){
      console.log('退出第一页:onDestroy');
    },
    "destoryPage"(){
      this.setData({isgo:true});
      console.log('退出第一页:finish');
      this.$page.finish();
    },
    "destoryApp"(){
      console.log('第一页退出应用:exit');
      this.data.$app.exit();
    },
    "go"(url){
      this.setData({isgo:true});
      router.push({
        "uri":url
});
    }
});

