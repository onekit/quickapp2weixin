import {Page,Component} from '../../../../quickapp2weixin/index.js';
import {system_router} from '../../../../quickapp2weixin/index';const router = system_router;
Page({
onekit_go(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.go0);this.go.apply(this,args);},
    "private":{
        "componentName":'退出页面与应用内页3',
        "isgo":false
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "onShow"(){
      this.setData({isgo:false});
    },
    "onHide"(){
      console.log('3页onHide触发',this.data.isgo);
      if(this.data.isgo == false)
      {
        console.log('第3页退出应用');
        this.data.$app.exit();
      }
else
      {
        this.setData({isgo:false});
      };
    },
    "onBackPress"(){
      console.log('第3页onbackpress');
      this.setData({isgo:true});
      return false;
    },
    "onDestroy"(){
      console.log('退出内页3:onDestroy');
    },
    "destoryPage"(){
      console.log('退出内页3:finish');
      this.setData({isgo:true});
      this.$page.finish();
    },
    "destoryApp"(){
      console.log('内页3退出应用:exit');
      this.data.$app.exit();
    },
    "go"(url){
      this.setData({isgo:true});
      router.push({
        "uri":url
});
    }
});

