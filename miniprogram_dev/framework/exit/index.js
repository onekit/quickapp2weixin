import {Page,Component} from '../../quickapp2weixin/index.js';
import {system_router} from '../../quickapp2weixin/index';const router = system_router;
Page({
onekit_go(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.go0);this.go.apply(this,args);},
    "private":{
        "componentName":'退出页面与应用首页',
        "isgo":false
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "onShow"(){
      this.setData({isgo:false});
    },
    "onHide"(){
      console.log('首页onHide触发:',this.data.isgo);
      if(this.data.isgo == false)
      {
        console.log('首页退出应用');
        this.data.$app.exit();
      }
else
      {
        this.setData({isgo:false});
      };
    },
    "onBackPress"(){
      console.log('首页onBackPress触发');
    },
    "onDestroy"(){
      console.log('退出首页:onDestroy');
    },
    "destoryPage"(){
      this.setData({isgo:true});
      console.log('退出首页:finish');
      this.$page.finish();
    },
    "destoryApp"(){
      console.log('首页退出应用:exit');
      this.data.$app.exit();
    },
    "go"(url){
      this.setData({isgo:true});
      router.push({
        "uri":url
});
    }
});

