import {Page,Component} from '../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'页面宽高',
        "beforeFilterMsg":'quickapp',
        "pageInfo":{
}
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'页面宽高'
});
      this.setData({[`pageInfo.windowWidth`]:this.$page.windowWidth});
      this.setData({[`pageInfo.windowHeight`]:this.$page.windowHeight});
      this.setData({[`pageInfo.statusBarHeight`]:this.$page.statusBarHeight});
    }
});

