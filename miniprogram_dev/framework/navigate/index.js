import {Page,Component} from '../../quickapp2weixin/index.js';
import {system_router} from '../../quickapp2weixin/index';const router = system_router;
Page({
    "private":{
        "path":'/home'
},
    "routerPush"(){
      router.push({
        "uri":this.data.path
});
    },
    "routerReplace"(){
      router.replace({
        "uri":this.data.path
});
    },
    "changePath"(e){
      this.setData({path:e.value});
    }
});

