import {Page,Component} from '../../quickapp2weixin/index.js';
import {system_configuration} from '../../quickapp2weixin/index';const configuration = system_configuration;
Page({
    "private":{
        "pageTitle":'多语言',
        "localeObject":{
},
        "messageTip":''
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.pageTitle
});
    },
    "onReady"(){
      this.setData({localeObject:configuration.getLocale()});
    },
    "onConfigurationChanged"(evt){
      console.info(`i18n：触发生命周期onConfigurationChange：${evt.type}`);
      this.setData({messageTip:`触发应用的生命周期：${new Date().toTimeString().split(' ')[0]}`});
      this.setData({localeObject:configuration.getLocale()});
    },
    "changeLocaleConfiguration"(){
      if(this.data.localeObject.language == 'zh')
      {
        console.info(`i18n：更新为英文的Locale`);
        configuration.setLocale({
            "language":'en',
            "countryOrRegion":'US'
});
      }
else
      {
        console.info(`i18n：更新为中文的Locale`);
        configuration.setLocale({
            "language":'zh',
            "countryOrRegion":'CN'
});
      };
    }
});

