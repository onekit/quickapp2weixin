import {Page,Component} from '../../../quickapp2weixin/index.js';
Component({
    "props":[
    ],
    "data"(){
      return {
        "msg":null,
        "eventDetail":null
};
    },
    "processMessage"(msg){
      const now = new Date().toISOString();
      this.setData({msg:`${now}: ${msg}`});
    },
    "events":{
        "customEventInVm2"(evt){
          const now = new Date().toISOString();
          this.setData({eventDetail:`${now}: ${evt.detail}`});
        }
}
});

