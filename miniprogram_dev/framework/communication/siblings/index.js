import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'兄弟组件通信'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "onReady"(){
      this.establishRef();
    },
    "establishRef"(){
      const siblingVm1 = this.$vm('sibling1');
      const siblingVm2 = this.$vm('sibling2');
      siblingVm1.parentVm = this;
      siblingVm1.nextVm = siblingVm2;
      siblingVm2.parentVm = this;
      siblingVm2.previousVm = siblingVm1;
    }
});

