import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Component({
    "props":[
      'prop1',
      'prop2Object'
    ],
    "data"(){
      return {
        "upperProp1":this.data.prop1
};
    },
    "onInit"(){
      console.info(`外部传递的数据：`,this.data.prop1,this.data.prop2Object);
      this.$watch('prop1','watchPropsChange');
      this.$watch('prop2Object.name','watchPropsChange');
      this.$on('evtType1',this.data.evtTypeHandler);
    },
    "watchPropsChange"(newV,oldV){
      prompt.showDialog({
        "title":'子组件监听数据变化',
        "message":`${oldV} -> ${newV}`,
        "buttons":[
          {
              "text":'确定',
              "color":'#0faeff'
}
        ]
});
      this.setData({upperProp1:(newV) && (newV.toUpperCase())});
    },
    "evtTypeHandler"(evt){
      prompt.showDialog({
        "title":'子组件事件响应',
        "message":`事件类型：${evt.type}\n事件参数：${JSON.stringify(evt.detail)}`,
        "buttons":[
          {
              "text":'确定',
              "color":'#0faeff'
}
        ]
});
    },
    "evtType2Emit"(){
      this.$dispatch('evtType2',{
        "params":'来自子组件的消息'
});
    },
    "evtType3Emit"(){
      this.$emit('evtType3',{
        "params":'来自子组件的消息'
});
    }
});

