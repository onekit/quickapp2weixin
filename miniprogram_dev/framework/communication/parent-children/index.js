import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "data1":'string',
        "data2":{
            "name":'object'
},
        "componentName":'父子组件通信'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
      this.$on('evtType2',this.data.evtTypeHandler);
    },
    "onReady"(){
      setTimeout(function(){
      this.setData({data1:'string2'});
      this.setData({[`data2.name`]:'object2'});
    }.bind(this),3000);
    },
    "evtTypeHandler"(evt){
      prompt.showDialog({
        "title":"父组件事件响应",
        "message":`事件类型：${evt.type}\n事件参数：${JSON.stringify(evt.detail)}`,
        "buttons":[
          {
              "text":'确定',
              "color":'#0faeff'
}
        ]
});
    },
    "evtType1Emit"(){
      this.$broadcast('evtType1',{
        "params":'来自父组件的消息'
});
    }
});

