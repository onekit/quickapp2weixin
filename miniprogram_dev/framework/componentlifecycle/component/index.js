import {Page,Component} from '../../../quickapp2weixin/index.js';
Component({
    "data":{
        "lifecycleList":[
        ],
        "lifecycleObject":{
            "onInit":{
                "count":0,
                "desc":`在组件数据驱动化完成后触发`
},
            "onReady":{
                "count":0,
                "desc":`在组件的模板加载完成后触发`
},
            "onDestroy":{
                "count":0,
                "desc":` 在组件销毁后触发`
}
}
},
    "onInit"(){
      Object.keys(this.data.lifecycleObject).forEach((item)=>{    const lifecycleList=this.data.lifecycleList;lifecycleList.push(item);this.setData({[`lifecycleList`]:lifecycleList})});
      console.info(`### onInit ###`);
          let lifecycleObject_onInit_count=this.data.lifecycleObject.onInit.count;++lifecycleObject_onInit_count;this.setData({[`lifecycleObject.onInit.count`]:lifecycleObject_onInit_count});
    },
    "onReady"(){
      console.info(`### onReady ###`);
          let lifecycleObject_onReady_count=this.data.lifecycleObject.onReady.count;++lifecycleObject_onReady_count;this.setData({[`lifecycleObject.onReady.count`]:lifecycleObject_onReady_count});
    },
    "onDestroy"(){
      console.info(`### onDestroy ###`);
          let lifecycleObject_onDestroy_count=this.data.lifecycleObject.onDestroy.count;++lifecycleObject_onDestroy_count;this.setData({[`lifecycleObject.onDestroy.count`]:lifecycleObject_onDestroy_count});
    }
});

