import {Page,Component} from '../../quickapp2weixin/index.js';
Page({
    "data":{
        "componentName":'Computed',
        "message":'Hello',
        "firstName":'Quick',
        "lastName":'App'
},
    "computed":{
        "reversedMessage"(){
          return this.data.message.split('').reverse().join('');
        },
        "fullName":{
            "get"(){
              return `${this.data.firstName} ${this.data.lastName}`;
            },
            "set"(value){
              const names = value.split(' ');
              this.setData({firstName:names[0]});
              this.setData({lastName:names[names.length - 1]});
            }
},
        "now"(){
          return Date.now();
        }
},
    "onReady"(){
      console.log(this.data.reversedMessage);
      this.setData({message:'Goodbye'});
      console.log(this.data.reversedMessage);
      console.log(this.data.fullName);
      this.setData({fullName:'John Doe'});
      console.log(this.data.firstName);
      console.log(this.data.lastName);
    }
});

