import {Page,Component} from '../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":'lifecycle',
        "lifecycleObject":{
            "onInit":{
                "count":0,
                "desc":`当页面完成初始化时调用，传递的第一个参数为对象，代表打开页面时外部传递进来的query参数，只触发一次`
},
            "onReady":{
                "count":0,
                "desc":`当页面完成创建可以显示时触发，只触发一次`
},
            "onShow":{
                "count":0,
                "desc":`当进入页面时触发`
},
            "onHide":{
                "count":0,
                "desc":`当页面跳转离开时触发`
},
            "onDestroy":{
                "count":0,
                "desc":`当页面跳转离开（不进入导航栈）时触发`
},
            "onBackPress":{
                "count":0,
                "desc":`当用户点击返回按钮时触发。返回true表示页面自己处理返回逻辑，返回false表示使用默认的返回逻辑，不返回值会作为false处理`,
                "params":''
},
            "onMenuPress":{
                "count":0,
                "desc":`当用户点击菜单按钮时触发`
}
},
        "lifecycleList":[
        ],
        "pageInfo":{
}
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Lifecycle'
});
      Object.keys(this.data.lifecycleObject).forEach((item)=>{    const lifecycleList=this.data.lifecycleList;lifecycleList.push(item);this.setData({[`lifecycleList`]:lifecycleList})});
      console.info(`### onInit ###`);
          let lifecycleObject_onInit_count=this.data.lifecycleObject.onInit.count;++lifecycleObject_onInit_count;this.setData({[`lifecycleObject.onInit.count`]:lifecycleObject_onInit_count});
      this.setData({pageInfo:this.$page});
    },
    "onReady"(){
      console.info(`### onReady ###`);
          let lifecycleObject_onReady_count=this.data.lifecycleObject.onReady.count;++lifecycleObject_onReady_count;this.setData({[`lifecycleObject.onReady.count`]:lifecycleObject_onReady_count});
    },
    "onShow"(){
      console.info(`### onShow ###`);
          let lifecycleObject_onShow_count=this.data.lifecycleObject.onShow.count;++lifecycleObject_onShow_count;this.setData({[`lifecycleObject.onShow.count`]:lifecycleObject_onShow_count});
    },
    "onHide"(){
      console.info(`### onHide ###`);
          let lifecycleObject_onHide_count=this.data.lifecycleObject.onHide.count;++lifecycleObject_onHide_count;this.setData({[`lifecycleObject.onHide.count`]:lifecycleObject_onHide_count});
    },
    "onDestroy"(){
      console.info(`### onDestroy ###`);
          let lifecycleObject_onDestroy_count=this.data.lifecycleObject.onDestroy.count;++lifecycleObject_onDestroy_count;this.setData({[`lifecycleObject.onDestroy.count`]:lifecycleObject_onDestroy_count});
    },
    "onBackPress"(params){
      console.info(`### onBackPress ###`);
          let lifecycleObject_onBackPress_count=this.data.lifecycleObject.onBackPress.count;++lifecycleObject_onBackPress_count;this.setData({[`lifecycleObject.onBackPress.count`]:lifecycleObject_onBackPress_count});
      this.data.lifecycleObject.onBackPress.params = JSON.stringify(params);
    },
    "onMenuPress"(){
      console.info(`### onMenuPress ###`);
          let lifecycleObject_onMenuPress_count=this.data.lifecycleObject.onMenuPress.count;++lifecycleObject_onMenuPress_count;this.setData({[`lifecycleObject.onMenuPress.count`]:lifecycleObject_onMenuPress_count});
    }
});

