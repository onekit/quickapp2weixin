import {Page,Component} from '../../quickapp2weixin/index.js';
let value;
Page({
    "private":{
        "componentName":'filter',
        "beforeFilterMsg":'quickapp',
        "front":'A'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":'Filter'
});
    },
    "capitalize"(value){
      if(!value)
      return '';
      value = value.toString();
      return value.charAt(0).toUpperCase() + value.slice(1);
    },
    "normalize"(value){
      if(!value)
      return '';
      value = value.toString();
      return value.charAt(0).toLowerCase() + value.slice(1);
    },
    "ModifyFrontAndBehind"(value,front,behind){
      return ((front + value)) + behind;
    }
});

