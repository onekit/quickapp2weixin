import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
onekit_onClickHandler2(e){const args=[];const dataset=e.currentTarget.dataset;args.push(dataset.onclickhandler20);args.push(dataset.onclickhandler21);this.onClickHandler2.apply(this,args);},
    "private":{
        "argName":'动态参数',
        "componentName":'原生组件事件'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "onClickHandler1"(evt){
      prompt.showToast({
        "message":`点击了按钮1`
});
      console.log(`触发事件：类型：${evt.type}, 详情： ${JSON.stringify(evt.detail)}`);
      if(evt.target)
      {
        console.log(`触发事件：节点：${evt.target.id}, ${evt.target.attr.disabled}`);
      };
    },
    "onClickHandler2"(arg1,arg2,evt){
      prompt.showToast({
        "message":`点击了按钮2`
});
      console.log(`触发事件：类型：${evt.type}, 参数： ${arg1}, ${arg2}`);
    },
    "emitElement"(){
      this.$emitElement('click',{
        "params":'参数内容'
},'elNode1');
    }
});

