import {Page,Component} from '../../../quickapp2weixin/index.js';
Page({
    "private":{
        "componentName":"事件冒泡"
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
    },
    "onParentClickHandler"(evt){
      console.info(`触发父组件事件：类型：${evt.type}`);
      if(evt.target)
      {
        console.info(`事件目标节点：${this.getNodeName(evt.target)}`);
      };
      if(evt.currentTarget)
      {
        console.info(`事件发生节点：${this.getNodeName(evt.currentTarget)}`);
      };
    },
    "onChildClickHandler"(evt){
      console.info(`触发子组件事件：类型：${evt.type}`);
      if(evt.target)
      {
        console.info(`事件目标节点：${this.getNodeName(evt.target)}`);
      };
      if(evt.currentTarget)
      {
        console.info(`事件发生节点：${this.getNodeName(evt.currentTarget)}`);
      };
    },
    "getNodeName"(node){
      const parentNode = this.$element('parentNode');
      const childNode1 = this.$element('childNode1');
      const childNode2 = this.$element('childNode2');
      let name = '';
          switch(node){
case parentNode:
        {
  name = 'parentNode';
  break;
};
case childNode1:
        {
  name = 'childNode1';
  break;
};
case childNode2:
        {
  name = 'childNode2';
  break;
};
    };
      return name;
    }
});

