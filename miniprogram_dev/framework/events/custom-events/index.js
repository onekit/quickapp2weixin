import {Page,Component} from '../../../quickapp2weixin/index.js';
import {system_prompt} from '../../../quickapp2weixin/index';const prompt = system_prompt;
Page({
    "private":{
        "componentName":'自定义事件'
},
    "onInit"(){
      this.$page.setTitleBar({
        "text":this.data.componentName
});
      this.$on('customEvtType1',this.data.customEvtType1Handler);
    },
    "emitEvent"(){
      this.$emit('customEvtType1',{
        "name":'自定义事件'
});
    },
    "customEvtType1Handler"(evt){
      prompt.showDialog({
        "title":`触发事件: ${evt.type}`,
        "message":`参数: ${evt.detail.name}`,
        "buttons":[
          {
              "text":'确定',
              "color":'#0faeff'
}
        ]
});
    },
    "addEventHandler"(){
      this.$on('customEvtType1',this.data.customEvtType1Handler);
      prompt.showToast({
        "message":'开始监听自定义事件'
});
    },
    "removeEventHandler"(){
      this.$off('customEvtType1');
      this.$off('customEvtType1',this.data.customEvtType1Handler);
      prompt.showToast({
        "message":'停止监听自定义事件'
});
    }
});

